%ydbguiUtils ; YottaDB Utils entry points; 05-07-2021
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
; --------------------------------------------
; --------------------------------------------
; OS
; --------------------------------------------
; --------------------------------------------
;
; ****************************************************************
; runShell
; ****************************************************************
runShell(command,return,shell,gldFile)
	; The shell parameter is used to use an alternative shell (like bash)
	new device,counter,string,currentdevice
	;
	set gldFile=$get(gldFile)
	set shell=$get(shell,"/bin/sh")
	set counter=0
	set currentdevice=$io
	set device="runshellcommmandpipe"_$job
	;
	if gldFile'="" set command="ydb_gbldir="_gldFile_" "_command
	open device:(shell=shell:command=command:readonly):5:"pipe"
	use device
	for  quit:$zeof=1  read string set return($increment(counter))=string
	close device if $get(return(counter))="" kill return(counter)
	use currentdevice
	quit $zclose
	;
	;
; ****************************************************************
; runIntShell
; ****************************************************************
runIntShell(command,sendCmd,return)
	new device,counter,string,currentdevice
	new $etrap
	set $etrap="goto runIntShellErr"
	;
	set counter=0
	set currentdevice=$io
	set device="runshellcommmandpipe"_$job
	;
	open device:(shell="/bin/sh":command=command)::"pipe"
	use device
	for  quit:$zeof  read string:.015 set return($increment(counter))=string quit:'$test
	write sendCmd,!
	for  quit:$zeof  read string:.015 set return($increment(counter))=string quit:'$test
	close device if $get(return(counter))="" kill return(counter)
	use currentdevice
runIntShellQuit
	quit $zclose
	;
runIntShellErr
	set $ecode=""
	;
	goto runIntShellQuit
	;
	;
; ****************************************************************
; getFileProps
; ****************************************************************
getFileProps(file)
	new props,ret,stat
	;
	set ret=$$statfile^%ydbposix(file,.stat)
	if ret'=0 set props=-1
	else  do
	. set props("accessRightsOctal")=stat("mode")
	. set props("ownerUser")=stat("uid")
	. set props("ownerGroup")=stat("gid")
	. set props("size")=stat("size")
	. set props("lastChanged")=stat("ctime")
	. set props("lastAccess")=stat("atime")
	. set props("lastModified")=stat("mtime")
	;
	quit *props
	;
	;
; ****************************************************************
; epochToDatetime
; ****************************************************************
epochToDatetime(epoch,withYday)
	new sec,min,hour,mday,mon,year,wday,yday,isdst,err
	;
	set ret=$&ydbposix.localtime(epoch,.sec,.min,.hour,.mday,.mon,.year,.wday,.yday,.isdst,.err)
	quit year_"-"_mon_"-"_mday_" "_$select($zlength(hour)=1:"0",1:"")_hour_":"_$select($zlength(min)=1:"0",1:"")_min_":"_$select($zlength(sec)=1:"0",1:"")_sec_$select($get(withYday):" "_yday,1:"")
	;
	;
; ****************************************************************
; tryCreateFile(filename)
;
; PARAMS:
; filename		string
; RETURNS:
; >0				OK
; 0					Could NOT create the file
; ****************************************************************
tryCreateFile(dir)
	new ret,io,file
	new $etrap
	;
	set ret=0
	set $etrap="goto tryCreateFileQuit"
	;
	; Create the file
	set file=dir_"/~tmp"
	open file:NEWVERSION
	close file:DELETE
	;
	set ret=1
	;
tryCreateFileQuit
	set $ecode=""
	quit ret
	;
	;
; ****************************************************************
; terminateProcess(pid)
;
; PARAMS:
; pid		number
; RETURNS:
; <0 				Shell error
; >0 <99996			Shell error
; 99997				Not enough rights
; 99998				Not a YDB process
; 99999				No such process
; 0					Ok
; ****************************************************************
terminateProcess(pid)
	new ret,shellResult
	;
	; And it is a yottadb process
	kill shellResult
	set ret=$$runShell^%ydbguiUtils("grep libyottadb.so /proc/"_pid_"/maps",.shellResult)
	; Not a YDB process
	if $data(shellResult)=0 set ret=99998 goto terminateProcessQuit
	; Process not running
	if $zfind($get(shellResult(1)),"No such") set ret=99999 goto terminateProcessQuit
	;
	; Terminate it
	set ret=$$runShell^%ydbguiUtils("$ydb_dist/mupip stop "_pid,.shellResult)
	if $zfind($get(shellResult(1)),"not permitted") set ret=99997
	;
terminateProcessQuit
	quit ret
	;
	;
; ****************************************************************
; serverMode
; ****************************************************************
serverMode ; GET /test/readwrite Tests readwrite flag
	set httprsp("serverMode")=$select(HTTPREADWRITE:"RW",1:"RO")
	;
	quit
	;
	;
; ****************************************************************
; getTmpDir
; ****************************************************************
getTmpDir()
	new tmpDir,ret,mode
	;
	set mode="S_IRWXU"
	set ret=$zsearch("$ydb_tmp")
	if ret'="" do  goto getTmpDirQuit
	. set tmpDir=ret_"/gui/"
	. set ret=$zsearch(tmpDir)
	. if ret="" do mkdir^%ydbposix(tmpDir,mode)
	;
	set ret=$zsearch("/tmp/")
	if ret="" do mkdir^%ydbposix("/tmp/",mode)
	set ret=$zsearch("/tmp/yottadb")
	if ret="" do mkdir^%ydbposix("/tmp/yottadb/",mode)
	set ret=$zsearch("/tmp/yottadb/gui")
	if ret="" do mkdir^%ydbposix("/tmp/yottadb/gui/",mode)
	set tmpDir="/tmp/yottadb/gui/"
	;
getTmpDirQuit
	quit tmpDir
	;
	;
; ****************************************************************
; getRandomFilename
; ****************************************************************
getRandomFilename()
	quit $zysuffix($zut)
	;
	;
; ****************************************************************
; ls
; ****************************************************************
ls(body)
	new command,ret,shellResult,res,resCnt,path,constDir
	;
	; get constant value
	set x=$&ydbposix.filemodeconst("S_IFDIR",.constDir)
	for  set file=$zsearch(body("path")_body("mask")) quit:file=""  do
	. kill stat
	. do statfile^%ydbposix(file,.stat)
	. ; check the file mode to verify it is a directory (mode \ constant MOD 2)
	. if stat("mode")\constDir#2,$zfind(body("type"),"D") do  quit
	. . set res("data",$increment(resCnt),"type")="dir"
	. . set file=$zpiece(file,"/",$zlength(file,"/"))
	. . set res("data",resCnt,"path")=file
	. if (stat("mode")\constDir#2)=0,$zfind(body("type"),"F") do
	. . set res("data",$increment(resCnt),"type")="file"
	. . set file=$zpiece(file,"/",$zlength(file,"/"))
	. . set res("data",resCnt,"path")=file
	;
	set res("result")="OK"
	;
lsQuit
	quit *res
	;
	;
; ****************************************************************
; pathExpand
; ****************************************************************
pathExpand(body)
	new command,shellResult,newPath,line,stat,constDir,x
	;
	set newPath=$zsearch(body("path"),-1)
	if newPath="" do  goto pathExpandQuit
	. set res("result")="ERROR"
	. set res("error","description")="Path / filename does not exist"
	. set res("error","code")=-1
	;
	set x=$&ydbposix.filemodeconst("S_IFDIR",.constDir)
	do statfile^%ydbposix(newPath,.stat)
	if body("dirsOnly")="true" do
	. if stat("mode")\constDir#2 do  quit
	. . set res("result")="OK"
	. . set res("data")=newPath
	. else  do
	. . set res("result")="ERROR"
	. . set res("error","description")="The target path is not a directory"
	. . set res("error","code")=-2
	else  do
	. if stat("mode")\constDir#2 do  quit
	. . set res("result")="ERROR"
	. . set res("error","description")="The target path is not a file"
	. . set res("error","code")=-3
	. else  do
	. . set res("result")="OK"
	. . set res("data")=newPath
	;
pathExpandQuit
	quit *res
	;
	;
; ****************************************************************
; getManifestFile(file)
;
; file				:full path specification of the manifest file
;
; Returns:
; *jdom				returns the json representation of the manifest file (jdom)
;					empty string if error reading or parsing the file
; ****************************************************************
getManifestFile(file)
	new dev,json,jdom,cnt,line,jsonCnt,jsonError
	new $etrap
	;
	set jdom=""
	set $etrap="goto getManifestFileQuit"
	;
	; read file
	set dev=file
	open dev:readonly
	use dev
	for  read line set json($increment(jsonCnt))=line quit:$zeof
	close dev
	;
	; convert the json to jdom
	do decode^%ydbwebjson("json","jdom","jsonError")
	if $data(jsonError) kill jdom set jdom=""
	;
getManifestFileQuit
	set $ecode=""
	quit *jdom
	;
	;
; ****************************************************************
; getConfigurationFile(file)
;
; file				:path specification of the manifest file
;
; Returns:
; *jdom				returns the json representation of the manifest file (jdom)
;					if error, jdom returns a code|description
;					1: path could not be found
;					2: error parsing json
;					3: error reading file
; ****************************************************************
getConfigurationFile(path)
	new dev,json,jdom,jsonCnt,line,jsonError
	new $etrap
	;
	set $etrap="goto getConfigurationFileError"
	;
	set path=$zsearch(path,-1)
	if path="" set jdom="1|path could not be found" goto getConfigurationFileQuit
	;
	; read file
	set dev=path
	open dev:readonly
	use dev
	for  read line set json($increment(jsonCnt))=line quit:$zeof
	close dev
	;
	; convert the json to jdom
	do decode^%ydbwebjson("json","jdom","jsonError")
	if $data(jsonError) do
	. kill jdom
	. merge jdom=jsonError
	. set jdom="2|Error parsing json"
	;
getConfigurationFileQuit
	quit *jdom
	;
getConfigurationFileError
	set jdom="3|"_$zstatus
	set $ecode=""
	;
	goto getConfigurationFileQuit
	;
	;
 ; --------------------------------------------
; --------------------------------------------
; STRINGS
; --------------------------------------------
; --------------------------------------------
strRemoveExtraSpaces(string)
	new cnt,newString,toggle,char
	;
	set toggle=0,newString=""
	for cnt=1:1:$zlength(string) do
	. set char=$zextract(string,cnt)
	. if char=" " do
	. . if toggle=0 set newString=newString_char,toggle=1
	. else  set newString=newString_char,toggle=0
	;
	quit newString
	;
	;
; This routine comes from the VistA Kernel
; IN is the string to be replaced
; SPEC is an array where each subscript is the string to search for and the value is the replacement value
; reutrns: the replaced string
strReplace(IN,SPEC) ;
 	Q:'$D(IN) "" Q:$D(SPEC)'>9 IN N A1,A2,A3,A4,A5,A6,A7,A8
 	S A1=$L(IN),A7=$J("",A1),A3="",A6=9999 F  S A3=$O(SPEC(A3)) Q:A3=""  S A6(A6)=A3,A6=A6-1
 	F A6=0:0 S A6=$O(A6(A6)) Q:A6'>0  S A3=A6(A6) D:$D(SPEC(A3))#2 RE1
 	S A8="" F A2=1:1:A1 D RE3
 	Q A8
 	;
RE1 S A4=$L(A3),A5=0 F  S A5=$F(IN,A3,A5) Q:A5<1  D RE2
 	Q
RE2 Q:$E(A7,A5-A4,A5-1)["X"  S A8(A5-A4)=SPEC(A3)
 	F A2=A5-A4:1:A5-1 S A7=$E(A7,1,A2-1)_"X"_$E(A7,A2+1,A1)
 	Q
RE3 I $E(A7,A2)=" " S A8=A8_$E(IN,A2) Q
 	S:$D(A8(A2)) A8=A8_A8(A2)
 	Q
