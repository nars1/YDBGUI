%ydbguiMaintenance ; YottaDB Maintenance; 01-07-2023
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
; ****************************************************************
; defrag
; ****************************************************************
defrag(body)
	new res,ret,command,shellResult,ix,regionList,globalFlag,region,report
	new field,global,rec,globalCnt,warningFlag
	;
	; we create the MUPIP string
	set command="$ydb_dist/mupip REORG"
	set command=command_" -FILL_FACTOR="_body("fillFactor")
	set command=command_" -INDEX_FILL_FACTOR="_body("indexFillFactor")
	if body("recover")'="false" set command=command_" -TRUNCATE="_body("recover")
	if body("resume")="true" set command=command_" -RESUME"
	; build the region list
	set (ix,regionList)=""
	for  set ix=$order(body("regions",ix)) quit:ix=""  set regionList=regionList_body("regions",ix)_","
	set regionList=$zextract(regionList,1,$zlength(regionList)-1)
	;
	set command=command_" -REGION "_regionList
	set res("command")=command
	;
	; execute the MUPIP REORG
	set ret=$$runShell^%ydbguiUtils(command,.shellResult)
	if ret'=0 do  goto defragQuit
	. ; if all regions have no globals
	. if ret=224 set res("result")="OK",res("data")="noGlobals" quit
	. ; errors
	. set res("result")="ERROR"
	. set res("error","description")="An error occured while executing REORG: "_ret
	. merge res("error","dump")=shellResult
	;
	;by global first
	set res("result")="OK",globalFlag="",warningFlag=0
	set ix="" for  set ix=$order(shellResult(ix)) quit:ix=""  do
	. set rec=shellResult(ix)
	. ;
	. if $zfind(rec,"%YDB-I-NEWJNLFILECREAT")!($zfind(rec,"%YDB-I-JNLSPACELOW"))!($zfind(rec,"%YDB-I-FILERENAME")) quit
	. if $zfind(rec,"Fill Factor") quit
	. if $ztranslate(rec," ","")="" set globalFlag="" quit
	. if $zfind(rec,"Global:"),globalFlag="" do  quit
	. . set globalFlag=$zpiece(rec," ",2)
	. . set region=$zpiece(rec," ",4),region=$zextract(region,1,$zlength(region)-1)
	. if $zfind(rec,"%YDB-I-MUTRUNCNOSPACE") do  quit
	. . set report($zpiece(rec," ",3),"totals","truncate")="noSpace"
	. if $zfind(rec,"YDB-MUPIP") quit
	. if $zfind(rec,"Truncated region:") do  quit
	. . set field=$zpiece(rec," ",8),field=$zextract(field,2,$zlength(field)-1)
	. . set report(region,"totals","truncate","totalBlocks","from")=field
	. . set field=$zpiece(rec," ",10),field=$zextract(field,2,$zlength(field)-2)
	. . set report(region,"totals","truncate","totalBlocks","to")=field
	. . ;
	. . set field=$zpiece(rec," ",15),field=$zextract(field,2,$zlength(field)-1)
	. . set report(region,"totals","truncate","freeBlocks","from")=field
	. . set field=$zpiece(rec," ",17),field=$zextract(field,2,$zlength(field)-2)
	. . set report(region,"totals","truncate","freeBlocks","to")=field
	. ;
	. set field=$zpiece(rec," ",2)
	. if field="cannot" set warningFlag=1 quit
	. set shellResult(ix)=$ztranslate(rec," ","")
	. set report(region,"byGlobal",globalFlag,field)=$ztranslate($zpiece(rec,":",2)," ","")
	;
	; and then the grand totals per region
	set report(region,"incompleteGlobalList")=$select(warningFlag:"true",1:"false")
	set region="" for  set region=$order(report(region)) quit:region=""  do
	. set globalCnt=0
	. set global=""  for  set global=$order(report(region,"byGlobal",global)) quit:global=""  do
	. . set globalCnt=globalCnt+1
	. . set field="" for  set field=$order(report(region,"byGlobal",global,field)) quit:field=""  do
	. . . set report(region,"totals",field)=+$get(report(region,"totals",field))+report(region,"byGlobal",global,field)
	. set report(region,"totals","globals")=globalCnt
	;
	merge res("data")=report
	;
defragQuit
	merge res("payloadRegions")=body("regions")
	quit *res
	;
	;
; ****************************************************************
; integ
; ****************************************************************
integ(body)
	new res,command,ix,line,regionFlag,summaryIx,detailsIx,detailsString
	new errors,errorList,errorFlag,errorData,errorIx,errorDataDumpIx
	;
	; we create the MUPIP string
	set command="$ydb_dist/mupip INTEG"
	set command=command_$select(body("type")="complete":"",1:" -FAST")
	set command=command_" -"_$select(body("reporting")="summary":"BRIEF",1:"FULL")
	set command=command_" -"_$select(body("keyRange")="false":"NO",1:"")_"KEYRANGES"
	set command=command_" -"_$select(body("stat")="false":"NO",1:"")_"STATS"
	set command=command_" -"_$select(body("mapErrors")=0:"NO",1:"")_"MAP"_$select(body("mapErrors")=0:"",1:"="_body("mapErrors"))
	set command=command_" -"_$select(body("keySizeErrors")=0:"NO",1:"")_"MAXKEYSIZE"_$select(body("keySizeErrors")=0:"",1:"="_body("keySizeErrors"))
	set command=command_" -"_$select(body("transactionErrors")=0:"NO",1:"")_"TRANSACTION"_$select(body("transactionErrors")=0:"",1:"="_body("transactionErrors"))
	;
	; build the region list
	set (ix,regionList)=""
	for  set ix=$order(body("regions",ix)) quit:ix=""  set regionList=regionList_body("regions",ix)_","
	set regionList=$zextract(regionList,1,$zlength(regionList)-1)
	;
	set command=command_" -REGION "_regionList
	set res("command")=command
	;
	; execute the MUPIP INTEG
	set ret=$$runShell^%ydbguiUtils(command,.shellResult)
	;
	; MUPIP returns 0 (all ok) 178 (integ errors), all others are real errors
	if ret'=0,ret'=178 do  goto integQuit
	. ; internal errors
	. set res("result")="ERROR"
	. set res("error","description")="An error occured while executing INTEG: "_ret
	. merge res("error","dump")=shellResult
	;
	set res("result")="OK"
	merge res("data","dump")=shellResult
	;
integQuit
	quit *res
	;
	;
; ****************************************************************
; backup
; ****************************************************************
backup(body)
	new res,ret,command,shellResult,ix,regionList,globalFlag,region,report
	new region,regionData,warnings,regions,deviceInfo,stat,lastChar
	new newPath,renameCnt
	;
	; build the region list
	set (ix,regionList)=""
	for  set ix=$order(body("regions",ix)) quit:ix=""  set regionList=regionList_body("regions",ix)_","
	set regionList=$zextract(regionList,1,$zlength(regionList)-1)
	;
	; adjust the path(s) first
	set lastChar=$zlength(body("targetPath"))
	if $extract(body("targetPath"),lastChar,lastChar)'="/" set body("targetPath")=body("targetPath")_"/"
	if $get(body("replTargetPath"))'="" do
	. set lastChar=$zlength(body("replTargetPath"))
	. if $extract(body("replTargetPath"),lastChar,lastChar)'="/" set body("replTargetPath")=body("replTargetPath")_"/"
	;
	;**************************
	; compute free space
	;**************************
	; get region data
	set ix="" for  set ix=$order(body("regions",ix)) quit:ix=""  do
	. set region=body("regions",ix)
	. kill regionData,warnings
	. do getRegionStruct^%ydbguiRegions(region,.regionData,.warnings)
	. merge regions(region)=regionData
	;
	; compute required space
	set reqSpace=0
	set region="" for  set region=$order(regions(region)) quit:region=""  do
	. set *stat=$$getFileProps^%ydbguiUtils($get(regions(region,"dbFile","data",1,"FILE_NAME")))
	. set reqSpace=reqSpace+stat("size")
	;
	set ret=$$runShell^%ydbguiUtils("df "_body("targetPath"),.deviceInfo)
	if ret'=0 do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The following error occurred while fetching device infos for: "_body("targetPath")_" "_ret
	. set res("error","code")=ret
	;
	set deviceInfo=$$strRemoveExtraSpaces^%ydbguiUtils(deviceInfo(2))
	if ($zpiece(deviceInfo," ",4)*1000-reqSpace)<0 do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="Not enough space on device mounted on: "_$zpiece(deviceInfo," ",6)_". Required space: "_reqSpace_" Current free space: "_($zpiece(deviceInfo," ",4)*1000)
	. set res("error","code")=10000
	;
	;**************************
	; rename if needed
	;**************************
	; loop in region
	if body("replace")'="true" set region="",renameCnt=0 for  set region=$order(regions(region)) quit:region=""  do  if $get(res("result"))="ERROR" goto backupQuit
	. set filename=$get(regions(region,"dbFile","data",1,"FILE_NAME"))
	. set filename=$zpiece(filename,"/",$zlength(filename,"/"))
	. ; perform the rename
	. set ret=$$renameFile(body("targetPath")_filename,.newPath)
	. if ret'=0 do  quit:$get(res("result"))="ERROR"
	. . set res("result")="ERROR"
	. . set res("error","description")="The following error occurred while moving the backup file: "_$select(ret=9999:"Access denied while moving the file",1:ret)
	. . set res("error","code")=ret
	. if newPath'="" do
	. . set renameCnt=renameCnt+0.1
	. . set res("data",renameCnt)="File: "_body("targetPath")_filename_" renamed into: "_newPath
	;
	; backup also processing replication ?
	if body("replace")'="true",body("includeReplicatedInstances")="true" do
	. set filename=$zsearch("$ydb_repl_instance",-1)
	. if filename="" set filename=$zsearch("$gtm_repl_instance",-1)
	. if filename'="" do
	. . set filename=$zparse(filename,"NAME")_$zparse(filename,"TYPE")
	. . if body("replUseSamePath")="true" set filename=body("targetPath")_filename
	. . else  set filename=body("replTargetPath")_filename
	. . ;
	. . set ret=$$renameFile(filename,.newPath)
	. . if ret'=0 do  quit:$get(res("result"))="ERROR"
	. . . set res("result")="ERROR"
	. . . set res("error","description")="The following error occurred while moving the repl file: "_$select(ret=9999:"Access denied while moving the file",1:ret)
	. . . set res("error","code")=ret
	. if newPath'="" do
	. . set renameCnt=renameCnt+0.1
	. . set res("data",renameCnt)="File: "_filename_" renamed into: "_newPath
	;
	;
	;**************************
	; perform backup
	;**************************
	; we create the MUPIP string
	set command="$ydb_dist/mupip BACKUP"
	if body("disableJournaling")="true" set command=command_" -BKUPDBJNL=DISABLE"
	if body("replace")="true" set command=command_" -REPLACE"
	if body("record")="true" set command=command_" -RECORD"
	set command=command_" -"_$select(body("createNewJournalFiles")="link":"NEWJNLFILES=PREVLINK",body("createNewJournalFiles")="nolink":"NEWJNLFILES=NOPREVLINK",1:"NONEWJNLFILES")
	if body("createNewJournalFiles")'="no" set command=command_","_$select(body("syncIo")="false":"NO",1:"")_"SYNC_IO"
	if body("includeReplicatedInstances")="true" set command=command_" -REPLINSTANCE="_$select(body("replUseSamePath")="true":body("targetPath"),1:body("replTargetPath"))
	set command=command_" "_regionList
	set command=command_" "_body("targetPath")
	set res("command")=command
	;
	; execute the MUPIP BACKUP
	kill shellResult
	set ret=$$runShell^%ydbguiUtils(command,.shellResult)
	;
	if ret'=0 do  goto integQuit
	. ; internal errors
	. set res("result")="ERROR"
	. set res("error","description")="An error occured while executing BACKUP: "_ret
	. merge res("error","dump")=shellResult
	;
	merge res("data")=shellResult
	;
	set res("result")="OK"
	;
backupQuit
	quit *res
	;
	;
	;*************************************************************
	; renameFile(fullPath)
	;
	; rename an existing file by appending a suffix
	;
	; Returns:	0			OK
	;			9999		Can not be expanded due to permissions
	;			<0			Error returned by the mv operation
	;*************************************************************
renameFile(fullPath,newPath)
	new err,file,timestamp,suffix,yday,time,level
	;
	set err=0
	set newPath=""
	set level=$zlevel
	;
	; look for existance in bu dir and expand the path
	set ret=$zsearch(fullPath,-1)
	if ret'="" do
	. ; ensure we have permissions
	. new $etrap
	. set $etrap="goto renameFileError"
	. open ret:readonly
	. close ret
	. ;
	. ; compute the suffix
	. set *stat=$$getFileProps^%ydbguiUtils(ret)
	. set timestamp=$$epochToDatetime^%ydbguiUtils(stat("lastModified"),1)
	. set yday=+$zpiece(timestamp," ",3)+1,time=$zpiece(timestamp," ",2)
	. set suffix="_20"_$extract($zpiece(timestamp,"-",1),2,3)_$select($zlength(yday)=1:"00",$zlength(yday)=2:"0",1:"")_yday_$zpiece(time,":",1)_$zpiece(time,":",2)_$zpiece(time,":",3)
	. ; and move the file
	. kill shellResult
	. set ret=$$runShell^%ydbguiUtils("mv "_fullPath_" "_fullPath_suffix,.shellResult)
	. if ret'=0 set err=ret
	. set newPath=fullPath_suffix
	;
renameFileQuit
	quit err
	;
renameFileError
	set err=9999
	zgoto level:renameFileQuit
