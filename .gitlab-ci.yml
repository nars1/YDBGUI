#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#       This source code contains the intellectual property     #
#       of its copyright holder(s), and is made available       #
#       under a license.  If you do not know the terms of       #
#       the license, please stop and do not read further.       #
#                                                               #
#################################################################

# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
  - build
  - test
  - scheduled

services:
  - docker:dind

sast:
  stage: test
  interruptible: true
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

commit-verify:
  image: ubuntu:22.04
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
  interruptible: true
  script:
    - apt-get update
    - apt-get install -yq git wget gnupg
    # Copy commit script to build directory and execute
    - wget https://gitlab.com/YottaDB/DB/YDB/-/raw/master/ci/commit_verify.sh
    - chmod +x commit_verify.sh
    - ./commit_verify.sh tools/ci/needs_copyright.sh https://gitlab.com/YottaDB/UI/YDBGUI/

build-test:
  stage: build
  # Run only on MR pipeline, not post merge
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"
  interruptible: true
  script:
    - mkdir -p docker-images
    - docker image build --progress=plain -t yottadbgui .
    - docker save yottadbgui > docker-images/yottadbgui.tar
  cache:
    key: "$CI_PIPELINE_ID"
    paths:
      - docker-images
    policy: push

ydb-gui-repl:
  image: docker:latest
  stage: test
  dependencies:
    - build-test
  # Run only on MR pipeline, not post merge
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"
  interruptible: true
  cache:
    key: "$CI_PIPELINE_ID"
    paths:
      - docker-images
    policy: pull
  script:
    - docker load < docker-images/yottadbgui.tar
    - docker tag yottadbgui ydbgui
    - $PWD/wwwroot/test/test_files/replication/main.sh

.test-shared:
  image: docker:latest
  stage: test
  dependencies:
    - build-test
  # Run only on MR pipeline, not post merge
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"
  interruptible: true
  cache:
    key: "$CI_PIPELINE_ID"
    paths:
      - docker-images
    policy: pull
  script:
    - docker load < docker-images/yottadbgui.tar
    - docker run --init --rm --env ydb_chset=${CHSET} -v $PWD/wwwroot:/YDBGUI/wwwroot:rw yottadbgui test -- ${TEST}

ydb-gui-test-readwrite:
  extends: .test-shared
  variables:
    CHSET: M
    TEST: readwrite

ydb-gui-test-readonly:
  extends: .test-shared
  variables:
    CHSET: M
    TEST: readonly

ydb-gui-test-login:
  extends: .test-shared
  variables:
    CHSET: M
    TEST: login

ydb-gui-test-octo-readwrite:
  extends: .test-shared
  variables:
    CHSET: M
    TEST: octo-readwrite

ydb-gui-test-octo-readonly:
  extends: .test-shared
  variables:
    CHSET: M
    TEST: octo-readonly

ydb-gui-test-readwrite-utf8:
  extends: .test-shared
  variables:
    CHSET: UTF-8
    TEST: readwrite

ydb-gui-test-login-utf8:
  extends: .test-shared
  variables:
    CHSET: UTF-8
    TEST: login

ydb-gui-test-octo-utf8:
  extends: .test-shared
  variables:
    CHSET: UTF-8
    TEST: octo-readwrite

ydb-gui-test-remote-config:
  extends: .test-shared
  variables:
    CHSET: UTF-8
    TEST: remote-config
