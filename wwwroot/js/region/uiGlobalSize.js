/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.globalSize.sort = {
    name: false,
    size: false,
    status: false
}
app.ui.globalSize.init = () => {
    $('#optGlobalSizeTypeScan').on('click', () => app.ui.globalSize.selectRadio('Scan'))
    $('#optGlobalSizeTypeArSample').on('click', () => app.ui.globalSize.selectRadio('ArSample'))
    $('#optGlobalSizeTypeImpSample').on('click', () => app.ui.globalSize.selectRadio('ImpSample'))

    $('#btnGlobalSizeOk').on('click', () => app.ui.globalSize.okButtonPressed())

    $('#btnGlobalSizeResultCopyToClipboard').on('click', () => app.ui.globalSize.copyToClipboardResult())

    $('#inpGlobalsTreeMap').on('change', () => app.ui.globalSize.treeMap.redraw())

    $('#modalGlobalSize').on('shown.bs.modal', () => {
        $('#btnGlobalSizeOk').focus()
    })

    // TEST
    app.ui.setupDialogForTest('modalGlobalSize')
    app.ui.setupDialogForTest('modalGlobalsTreeMap')
}

app.ui.globalSize.show = () => {
    app.ui.globalSize.selectRadio('ArSample')

    // color if zombie
    const type = $('#tblRegionViewGlobals > tbody').find('.selected >td:nth-child(2)').text()
    const color = type !== 'ok' ? 'var(--ydb-status-red)' : 'var(--ydb-status-green'

    $('#lblGlobalSizeGlobal')
        .text($('#tblRegionViewGlobals > tbody').find('.selected >td:nth-child(1)').text())
        .css('color', color)

    $('#tblRegionViewGlobals > tbody').find('.selected >td:nth-child(2)').text()


    $('#modalGlobalSize').modal({show: true, backdrop: 'static'})
}

app.ui.globalSize.selectRadio = type => {
    $('#optGlobalSizeType' + type).prop('checked', true)

    switch (type) {
        case 'Scan': {
            $('#lblGlobalSizeParam').text(' level')
            $('#inpGlobalSizeParam')
                .val(0)
                .attr('min', '-1')
                .attr('max', '10')

            break
        }

        case 'ArSample':
        case 'ImpSample': {
            $('#lblGlobalSizeParam').text(' samples')
            $('#inpGlobalSizeParam')
                .val(1000)
                .attr('min', '1')
                .attr('max', '10000')

            break
        }
    }
}

app.ui.globalSize.okButtonPressed = async () => {
    const region = app.ui.regionView.currentRegion
    const type = $('input[name=globalSizeType]:checked').prop('id').slice(17).toLowerCase()     // strip out the ID prefix
    const param = $('#inpGlobalSizeParam').val()
    const blockSize = app.system.regions[region].dbFile.flags.fsBlockSize

    try {
        $('#modalGlobalSize').modal('hide')

        app.ui.wait.show("Computing size...");

        const res = await app.REST._getGlobalSize(type, param, region)

        $('#modalGlobalSize').modal('hide')

        if (res.result === 'ERROR') {
            app.ui.wait.hide()

            app.ui.msgbox.show(app.REST.parseError(res), 'ERROR')

            return
        }

        // remove the first line
        res.data.dump.splice(0, 1)

        let result = []
        let lGlobal = {}
        let inRegionFlag = false

        res.data.dump.forEach(line => {
            // SCAN
            if (type === 'scan') {
                if (inRegionFlag === false && line.indexOf('Global') > -1) {
                    // new region boundary
                    lGlobal = {
                        name: '^' + line.replace(/  +/g, ' ').split(' ')[1],
                        data: [],
                        totalBlocks: 0
                    }

                    inRegionFlag = true

                    return
                }

                if (inRegionFlag === true) {
                    if (line === '') {
                        inRegionFlag = false

                        lGlobal.spaceInBytes = lGlobal.totalBlocks * blockSize

                        result.push(lGlobal)

                        return
                    }

                    lGlobal.data.push(line)

                    const piece3 = line.replace(/  +/g, ' ').split(' ')[2]
                    if (parseInt(piece3) > -1) {
                        lGlobal.totalBlocks += parseInt(piece3)
                    }
                }

            } else {
                // other
                if (inRegionFlag === false && line.indexOf('Global') > -1) {
                    // new region boundary
                    lGlobal = {
                        name: '^' + line.replace(/  +/g, ' ').split(' ')[1],
                        data: []
                    }

                    inRegionFlag = true

                    return
                }

                if (inRegionFlag === true) {
                    if (line === '') return

                    lGlobal.data.push(line)

                    if (line.indexOf('Total') > -1) {
                        inRegionFlag = false

                        // compute space
                        lGlobal.totalBlocks = line.replace(/  +/g, ' ').split(' ')[1]
                        lGlobal.spaceInBytes = lGlobal.totalBlocks * blockSize

                        result.push(lGlobal)
                    }
                }
            }
        })

        // adjust the last record of the SCAN method
        if (type === 'scan') {
            lGlobal.spaceInBytes = lGlobal.totalBlocks * blockSize

            result.push(lGlobal)
        }

        // execute the mapping
        app.ui.regionView.globals.map(global => {
            const newData = result.find(glb => global.name === glb.name)

            if (newData) {
                global.totalBlocks = newData.totalBlocks
                global.spaceInBytes = newData.spaceInBytes
            }
        })

        // add sort events
        $('#regionViewGlobalsHeaderName')
            .off('click')
            .on('click', () => app.ui.globalSize.sortBy('name'))
            .css('cursor', 'pointer')
            .html('Name&nbsp;&nbsp;<i class="bi-sort-alpha-down"></i>')
        $('#regionViewGlobalsHeaderStatus')
            .off('click')
            .on('click', () => app.ui.globalSize.sortBy('status'))
            .css('cursor', 'pointer')
            .html('Status&nbsp;&nbsp;<i class="bi-sort-alpha-down-alt"></i>')
        $('#regionViewGlobalsHeaderSizeBlocks')
            .off('click')
            .on('click', () => app.ui.globalSize.sortBy('size1'))
            .css('cursor', 'pointer')
            .html('Size (blocks)&nbsp;&nbsp;<i class="bi-sort-numeric-down-alt"></i>')
        $('#regionViewGlobalsHeaderSizeBytes')
            .off('click')
            .on('click', () => app.ui.globalSize.sortBy('size2'))
            .css('cursor', 'pointer')
            .html('Size (bytes)&nbsp;&nbsp;<i class="bi-sort-numeric-down-alt"></i>')

        // reset and perform sorting
        app.ui.globalSize.sort.name = false
        app.ui.globalSize.sortBy('name')

        if (app.ui.regionView.globals.length > 1) {
            app.ui.button.enable($('#btnRegionViewGlobalsTreeMap'))
        } else {
            app.ui.button.disable($('#btnRegionViewGlobalsTreeMap'))
        }

        app.ui.wait.hide()

    } catch (e) {
        app.ui.wait.hide()

        app.ui.msgbox.show(app.REST.parseError(e), 'ERROR')
    }
}

app.ui.globalSize.redraw = () => {
    const tblRegionViewGlobals = $('#tblRegionViewGlobals > tbody')
    let rows = ''

    app.ui.regionView.globals.forEach(global => {
        rows += '<tr style="color: ' + (global.status === 'ok' ? 'var(--ydb-status-green)' : 'var(--ydb-status-red)') + ';">'

        rows += '<td>' + global.name + '</td>'
        rows += '<td>' + global.status + '</td>'
        rows += '<td>' + app.ui.formatThousands(global.totalBlocks) + '</td>'
        rows += '<td>' + app.ui.formatBytes(global.spaceInBytes) + '</td>'

        rows += '</tr>'
    })

    tblRegionViewGlobals
        .empty()
        .append(rows)

    app.ui.regeneratePopups()
}

app.ui.globalSize.sortBy = field => {
    $('#regionViewGlobalsHeaderName').css('color', 'var(--ydb-purple')
    $('#regionViewGlobalsHeaderStatus').css('color', 'var(--ydb-purple')
    $('#regionViewGlobalsHeaderSizeBlocks').css('color', 'var(--ydb-purple')
    $('#regionViewGlobalsHeaderSizeBytes').css('color', 'var(--ydb-purple')

    switch (field) {
        case 'name': {
            $('#regionViewGlobalsHeaderName').css('color', 'var(--ydb-orange')

            break
        }
        case 'status': {
            $('#regionViewGlobalsHeaderStatus').css('color', 'var(--ydb-orange')

            break
        }
        case 'size1': {
            $('#regionViewGlobalsHeaderSizeBlocks').css('color', 'var(--ydb-orange')

            break
        }
        case 'size2': {
            $('#regionViewGlobalsHeaderSizeBytes').css('color', 'var(--ydb-orange')

            break
        }
    }

    if (field.indexOf('size') > -1) field = 'size';

    app.ui.globalSize.sort[field] = !app.ui.globalSize.sort[field]

    app.ui.regionView.globals.sort((el1, el2) => {
        switch (field) {
            case 'name': {
                if (el1.name > el2.name) return app.ui.globalSize.sort.name === true ? 1 : -1
                if (el1.name < el2.name) return app.ui.globalSize.sort.name === true ? -1 : 1
                return 0

                break
            }
            case 'size': {
                if (el1.spaceInBytes > el2.spaceInBytes) return app.ui.globalSize.sort.size === true ? 1 : -1
                if (el1.spaceInBytes < el2.spaceInBytes) return app.ui.globalSize.sort.size === true ? -1 : 1
                return 0

                break
            }

            case 'status': {
                if (el1.status > el2.status) return app.ui.globalSize.sort.status === true ? 1 : -1
                if (el1.status < el2.status) return app.ui.globalSize.sort.status === true ? -1 : 1
                return 0
            }
        }
    })

    app.ui.globalSize.redraw()
}

// *****************************************
// tree map
// *****************************************
app.ui.globalSize.treeMap = {
    show: function () {
        $('#inpGlobalsTreeMap')
            .attr('min', 2)
            .attr('max', app.ui.regionView.globals.length)
            .val(app.ui.regionView.globals.length > 10 ? 10 : app.ui.regionView.globals.length)

        app.ui.globalSize.treeMap.chart = new ApexCharts(document.querySelector("#chartGlobalsTreeMap"), {
            chart: {
                height: 550,
                type: 'treemap'
            },
            title: {
                text: 'Region: ' + app.ui.regionView.currentRegion
            },
            dataLabels: {
                enabled: true,
                style: {
                    fontSize: '13px',
                },
                formatter: function (text, op) {
                    return [text, app.ui.formatBytes(op.value)]
                },
                offsetY: -4
            },
            series: [
                {
                    data: [{x: 10, y: 10}]
                }
            ],
            plotOptions: {
                treemap: {
                    enableShades: false,
                }
            },
            colors: [
                '#64a555'
            ],
            tooltip: {
                y: {
                    formatter: function (value, {series, seriesIndex, dataPointIndex, w}) {
                        return app.ui.formatBytes(value)
                    }
                }
            }
        });
        app.ui.globalSize.treeMap.chart.render();

        this.redraw()

        $('#modalGlobalsTreeMap').modal({show: true, backdrop: 'static'})
    },

    redraw: function () {
        // sort the data
        const sortedGlobals = app.ui.regionView.globals.sort((el1, el2) => {
            if (el1.spaceInBytes > el2.spaceInBytes) return -1
            if (el1.spaceInBytes < el2.spaceInBytes) return 1
            return 0
        })

        const plotData = []
        let counter = 0

        sortedGlobals.forEach(global => {
            counter++
            if (counter > $('#inpGlobalsTreeMap').val()) return

            plotData.push({
                x: global.name,
                y: global.spaceInBytes
            })
        })

        app.ui.globalSize.treeMap.chart.updateSeries([
            {
                data: plotData
            }
        ])
    },

    chart: {},
}
