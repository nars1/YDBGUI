/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.octoViewer.runPressed = async type => {
    let sql = app.ui.octoViewer.getSql(type).sql

    if (!sql || sql === '') return

    // append ; if needed
    if (sql.slice(-1).toString() !== ';') sql += ';'
    sql = sql.trim() //.replace(/\n/gm, '')

    // replace tabs with 1 space to match Octo error indicator ^^^^^
    sql = sql.replace(/\t/g, ' ')

    // extract all queries
    app.ui.octoViewer.queries[type] = {
        fullSql: sql,
        splitSql: sql.split(';'),
        aSql: [],
        result: []
    }

    // clean out the array and remove empty ones
    app.ui.octoViewer.queries[type].splitSql.forEach(lSql => {
        if (lSql !== '') app.ui.octoViewer.queries[type].aSql.push(lSql)
    })
    delete app.ui.octoViewer.queries[type].splitSql

    // disable RUN button
    $('#btnOctoRun' + type)
        .attr('disabled', true)
        .addClass('disabled')

    let aborted = false;

    app.ui.wait.show("Executing query", () => {
        aborted = true

    }, false);

    for (const lSql of app.ui.octoViewer.queries[type].aSql) {
        try {
            const res = await app.REST._executeOctoQuery(lSql, parseInt($('#inpOctoTimeout' + type).val()) * 1000)

            // abort should abort ALL subsequent queries
            if (aborted === true) continue;

            // error should be flagged and stop all subsequent queries
            if (res.result === 'ERROR') {
                app.ui.msgbox.show(app.REST.parseError(res), 'WARNING')

                continue;
            }

            // parse output and choose viewer
            let cliInResponse = false
            let emptyFlag = false

            if (res.data === undefined) {
                // *************************************
                // empty response
                // *************************************
                $('#tabOctoText' + type).tab('show')
                app.ui.octoViewer.queries[type].result.push({
                    data: undefined,
                    type: 'cli'
                })

            } else {
                let rowCounter = 0
                let tableStructCounter = 0
                let lResult = {
                    data: [],
                    type: ''
                }
                let inQuery = false

                res.data.forEach((row, ix) => {
                    row = row.toString()

                    // single row CLI
                    if (
                        (row === 'CREATE TABLE' ||
                            row === 'CREATE FUNCTION' ||
                            row.indexOf('INSERT') > -1 ||
                            row.indexOf('UPDATE') > -1 ||
                            row === 'DROP TABLE' ||
                            row === 'DROP VIEW' ||
                            row === 'CREATE VIEW' ||
                            row.indexOf('DELETE') > -1 ||
                            (row.indexOf('[ERROR]') > -1 && (res.data[ix + 1] === undefined || res.data[ix + 1].indexOf('LINE') !== 0)))
                        && (inQuery === false && row.indexOf('|') === -1)
                    ) {
                        app.ui.octoViewer.queries[type].result.push({
                            data: [row],
                            type: 'cli'
                        })
                        cliInResponse = true

                        return
                    }

                    // table and view struct start
                    if ((row.indexOf('Table') === 0 || row.indexOf('View') === 0) && inQuery === false) {
                        tableStructCounter = 1
                        lResult.data.push(row)
                        lResult.type = 'cli'
                        cliInResponse = true
                        return

                    }

                    // table / view struct continue and end
                    if (tableStructCounter > 0) {
                        lResult.data.push(row)

                        rowCounter++

                        if (row.indexOf('Global: ^') > -1 || row.toUpperCase().indexOf('CREATE VIEW') > -1) {
                            tableStructCounter = 0
                            app.ui.octoViewer.queries[type].result.push(lResult)

                            lResult = {
                                data: [],
                                type: ''
                            }
                        }

                        return
                    }

                    // CLI ?
                    // determine the NEXT query entry. If empty, NEXT will be 0
                    const nextQryIndex = app.ui.octoViewer.queries[type].result.length
                    let skip = false

                    // now we check if the command is a SELECT {num}
                    if (app.ui.octoViewer.queries[type].aSql[nextQryIndex] !== undefined && app.ui.octoViewer.queries[type].aSql[nextQryIndex].trim().toLowerCase().indexOf('select') === 0) skip = true

                    if (inQuery === false && ((row.indexOf('[ERROR]') > -1 || row.indexOf('LINE') > -1 || (row.indexOf('???') > -1 && skip === false)) && rowCounter === 0) && row.indexOf('|') === -1) {
                        rowCounter = 1
                        lResult.data.push(row)
                        lResult.type = 'cli'
                        cliInResponse = true
                        return
                    }

                    // inside CLI response
                    if (rowCounter > 0) {
                        lResult.data.push(row)

                        rowCounter++

                        // check if we have all THREE lines of the error response
                        if (rowCounter === 3) {
                            rowCounter = 0
                            app.ui.octoViewer.queries[type].result.push(lResult)

                            lResult = {
                                data: [],
                                type: ''
                            }
                        }

                        return
                    }

                    // if we are here, it is a query result
                    if (inQuery === false) {
                        inQuery = true
                        lResult.data.push(row)

                        return
                    }

                    // terminator found   // row.indexOf('row)') > -1 ||
                    if (row.indexOf('rows)') > -1 || row.indexOf('row)') > -1) {
                        lResult.data.push(row)
                        app.ui.octoViewer.queries[type].result.push(lResult)
                        lResult = {
                            data: [],
                            type: ''
                        }
                        inQuery = false

                        return
                    }

                    lResult.data.push(row)
                    lResult.type = 'table'
                })

            }
        } catch (err) {
            // skip this test and flag it as error

            // TEST closure
            app.ui.setTestClosure('divOctoSqlArea' + type)

            app.ui.wait.hide()

            app.ui.octoViewer.resetButtons(type)

            app.ui.msgbox.show(app.REST.parseError(err), 'WARNING')

            app.ui.octoViewer.cm[type].focus()
        }
    }

    // now load the data into the viewers

    // clear all tabs highlight
    $('#tabOctoResultTabs' + type).children().each((ix, el) => {
        $(el)
            .css('text-decoration', 'none')
            .html($(el).html().replaceAll(app.ui.octoViewer.bullet, ''))
    })

    if (Array.isArray(app.ui.octoViewer.queries[type].result)) {
        app.ui.octoViewer.queries[type].result.forEach((result, ix) => {
            if (result === null) return

            if (result.type === 'cli') {
                app.ui.octoViewer.text(result, type)

                return
            }

            const sql = app.ui.octoViewer.queries[type].aSql[ix]
            app.ui.octoViewer.table(result, sql, type)
        })
    }

    // TEST closure
    // by executing this function, we inform the test that the query execution has been completed
    app.ui.setTestClosure('divOctoSqlArea' + type)

    app.ui.wait.hide()

    app.ui.octoViewer.cm[type].focus()

    app.ui.octoViewer.resetButtons(type)
}

