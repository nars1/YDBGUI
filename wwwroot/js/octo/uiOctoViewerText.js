/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.octoViewer.text = (res, type) => {
    if (res.data === undefined) {
        res.data = []
        res.data.push('<span style="color: green;">Empty response </span>')

    } else {
        // detect syntax errors
        if (res.data[0].indexOf('LINE') > -1 || res.data[0].indexOf('[ERROR]') > -1) {
            res.data.forEach((line, lineIx) => {
                if (typeof line === 'string') {
                    const firstOccurrence = line.indexOf('^')
                    let lastOccurrence = 0

                    if (firstOccurrence > -1) {
                        // syntax error found, get the start / end position
                        for (let ix = firstOccurrence; ix <= line.length; ix++) {
                            if (line.charAt(ix) !== '^') {
                                lastOccurrence = ix

                                break
                            }
                        }

                        // color the previous line on the error location
                        const prevLine = res.data[lineIx - 1]

                        if (prevLine === undefined) return

                        let newLastLine = prevLine.substring(0, firstOccurrence)

                        newLastLine += '<span style="color: red;">' + prevLine.substring(firstOccurrence, lastOccurrence) + '</span>'

                        res.data[lineIx - 1] = newLastLine
                    }
                }
            })
        }
    }

    let text = '<hr><pre>' + res.data.join('<br>') + '</pre>'

    $('#tabOctoText' + type)
        .html(app.ui.octoViewer.bullet + '&nbsp;&nbsp;<span name="octo-sub-tab">CLI</span>')
        .tab('show')

    $('#divOctoCli' + type)
        .append(text)

    $('#divOctoText' + type).animate({scrollTop: $('#divOctoCli' + type).outerHeight()}, 250)
}
