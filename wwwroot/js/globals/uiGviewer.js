/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

let gViewer = class {
    constructor() {
        this.tabId = '';
        this.path = {};

        this.status = {
            parenOpen: false,
            autoComplete: {},
            errorSet: 0
        };

        this.viewer = {
            buffer: {
                original: {},               // The original payload
                head: 0,                    // The offset to use when doing a reverse traverse operation
                tail: {},                   // The path of the last record
                data: [],                   // The data buffer
                bookmarks: [],
                dataCount: 0,               // The number of records in the buffer
                baseOffset: 0,              // The offset to the first record, if head got clipped
                tailNoMoreRecords: false,   // True if no more records can be fetched
                headNoMoreRecords: true,    // True if no more records can be fetched
                scrollLast: 0,              // We use this to avoid extra fetches while scrolling up from the bottom and within triggering range
                scrolling: false,           // true if we are scrolling (to avoid re-entrance from async calls)
                scrollMode: 'head',         // head if M is going down, tail if going up
                headScrolling: false,       // true if we are scrolling the head (to avoid re-entrance from async calls)
                path: '',                   // the original path
                defaults: {}
            },
            appearance: {
                theme: 'light'
            },
            table: {
                bookmarkColor: '',
                headers: {
                    fontSize: '12px'
                },
                bookmarks: {
                    fore: '',
                    back: '',
                },
                rows: {
                    fontSize: '12px',
                    left: {
                        hideDuplicates: false,
                        level: 9999,
                    },
                    right: {
                        showPieces: 'pill',
                        pieceChar: '^',
                    }
                },
                scrollLocked: false
            },
            settings: {},
            theme: {},
            parserPayload: {}               // The payload after parsing (so, before to be adjusted for the REST call), used to redraw the path bar
        }
    }
};

app.ui.gViewer.instance = {};

app.ui.gViewer.lastIdGviewer = 0;

app.ui.gViewer.addNew = () => {
    // we need to look, at first, for fatal errors in the database
    if (app.ui.checkSystemErrors() === true) return

    app.ui.gViewer.lastIdGviewer++;
    const type = '-G-' + app.ui.gViewer.lastIdGviewer;

    app.ui.tabs.select(type);

    return type
};

app.ui.gViewer.createInstance = type => {
    const newViewer = $('#tabGviewer').clone();
    const newTabId = 'tab' + type + 'Div';

    $('#tabsDivsContainer').append(newViewer);

    $('#tabsDivsContainer > #tabGviewer').prop('id', newTabId);
    const newTabDiv = $('#' + newTabId);

    newTabDiv
        .attr('aria-labelledby', 'tab' + type)
        .css('display', 'block');

    // regenerate id's
    newTabDiv.find('#spanGviewerPathPreview').attr('id', 'spanGviewerPathPreview' + type);
    newTabDiv.find('#inpGviewerPath').attr('id', 'inpGviewerPath' + type);
    newTabDiv.find('#btnGviewerPathSubmit').attr('id', 'btnGviewerPathSubmit' + type);
    newTabDiv.find('#btnGviewerPermalink').attr('id', 'btnGviewerPermalink' + type);
    newTabDiv.find('#lblGviewerPath').attr('id', 'lblGviewerPath' + type);
    newTabDiv.find('#altGviewerNoRecordsFound').attr('id', 'altGviewerNoRecordsFound' + type);

    newTabDiv.find('#btnGviewerZoomOut').attr('id', 'btnGviewerZoomOut' + type);
    newTabDiv.find('#btnGviewerZoomIn').attr('id', 'btnGviewerZoomIn' + type);
    newTabDiv.find('#btnGviewerTheme').attr('id', 'btnGviewerTheme' + type);
    newTabDiv.find('#btnGviewerSettings').attr('id', 'btnGviewerSettings' + type);

    newTabDiv.find('#divGviewerTableScroll').attr('id', 'divGviewerTableScroll' + type);
    newTabDiv.find('#tblGviewerTable').attr('id', 'tblGviewerTable' + type);

    newTabDiv.find('#menuGviewerBookmarkColorRed').attr('id', 'menuGviewerBookmarkColorRed' + type);
    newTabDiv.find('#menuGviewerBookmarkColorGreen').attr('id', 'menuGviewerBookmarkColorGreen' + type);
    newTabDiv.find('#menuGviewerBookmarkColorBlue').attr('id', 'menuGviewerBookmarkColorBlue' + type);
    newTabDiv.find('#menuGviewerBookmarkColorCyan').attr('id', 'menuGviewerBookmarkColorCyan' + type);
    newTabDiv.find('#menuGviewerBookmarkColorMagenta').attr('id', 'menuGviewerBookmarkColorMagenta' + type);
    newTabDiv.find('#menuGviewerBookmarkColorYellow').attr('id', 'menuGviewerBookmarkColorYellow' + type);

    newTabDiv.find('#lblGviewerBookmarkColor').attr('id', 'lblGviewerBookmarkColor' + type);

    // instance the class
    app.ui.gViewer.instance[type] = new gViewer;
    app.ui.gViewer.instance[type].tabId = type;

    // init the table
    app.ui.gViewer.table.instanceInit(type);


    // path bar handlers
    const inpGviewerPath = $('#inpGviewerPath' + type);
    inpGviewerPath
        .val('^')
        .on('keypress', e => { return app.ui.gViewer.parser.keyPress(type, e) })
        .on('keyup', e => { return app.ui.gViewer.parser.keyUp(type, e) })
        .on('focus', () => { app.ui.gViewer.parser.focus(type) });

    $('#spanGviewerPathPreview' + type).on('click', () => { app.ui.gViewer.parser.resetPreview(type) });
    $('#btnGviewerPathSubmit' + type).on('click', () => app.ui.gViewer.parser.parse(type, inpGviewerPath.val()));
    $('#btnGviewerPermalink' + type).on('click', () => app.ui.permalinks.show());

    // toolbar handlers
    $('#btnGviewerZoomOut' + type).on('click', () => app.ui.gViewer.table.zoom(type, '-'));
    $('#btnGviewerZoomIn' + type).on('click', () => app.ui.gViewer.table.zoom(type, '+'));

    $('#btnGviewerTheme' + type).on('click', () => app.ui.gViewer.table.theme(type));

    $('#btnGviewerSettings' + type).on('click', () => app.ui.gViewer.settings.show(type));

    $('#menuGviewerBookmarkColorRed' + type).on('click', () => app.ui.gViewer.updateBookmarkColor(type, 'red'));
    $('#menuGviewerBookmarkColorGreen' + type).on('click', () => app.ui.gViewer.updateBookmarkColor(type, 'lightgreen'));
    $('#menuGviewerBookmarkColorBlue' + type).on('click', () => app.ui.gViewer.updateBookmarkColor(type, 'blue'));
    $('#menuGviewerBookmarkColorCyan' + type).on('click', () => app.ui.gViewer.updateBookmarkColor(type, 'cyan'));
    $('#menuGviewerBookmarkColorMagenta' + type).on('click', () => app.ui.gViewer.updateBookmarkColor(type, 'magenta'));
    $('#menuGviewerBookmarkColorYellow' + type).on('click', () => app.ui.gViewer.updateBookmarkColor(type, 'gold'));

    $('#lblGviewerBookmarkColor' + type).css('background', app.userSettings.globalViewer.themes[app.ui.gViewer.instance[type].viewer.appearance.theme].table.bookmarkColor);

    // handler to trap CTRL-END
    $('#tblGviewerTable' + type).on('keydown', e => app.ui.gViewer.table.keyPressed(e, type))

    // this will hide the json viewer
    $('#tab' + type + 'Div').on('click', () => $('#divGviewerJsonViewer').css('display', 'none'))

    inpGviewerPath.focus()

    // TEST
    $('#' + newTabId).css('animation-name', 'shown')
};

app.ui.gViewer.addLastUsed = path => {
    const type = app.ui.gViewer.addNew();

    $('#inpGviewerPath' + type).val(path);

    app.ui.gViewer.parser.autoComplete.init(type);
    app.ui.gViewer.parser.parse(type, path)
};

app.ui.gViewer.updateBookmarkColor = (type, color) => {
    $('#lblGviewerBookmarkColor' + type).css('background', color);

    app.ui.gViewer.instance[type].viewer.table.bookmarkColor = color;
};
