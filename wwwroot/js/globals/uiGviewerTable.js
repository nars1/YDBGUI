/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
app.ui.gViewer.table = {};

app.ui.gViewer.table.scrollLocked = false

app.ui.gViewer.table.keyPressed = async (e, tabId) => {
    if (e.originalEvent.ctrlKey === true && e.originalEvent.key === 'End') {
        const instance = app.ui.gViewer.instance[tabId];

        instance.viewer.table.scrollLocked = true
        instance.viewer.buffer.scrollMode = 'tail'

        // fetch data
        const res = await app.ui.gViewer.data.endFetch(instance);
        if (res === -1) return;        // error encountered (msg already displayed)

        // populate table using default values if recordCount > 0
        if (res > 0) {
            app.ui.gViewer.table.populate(tabId);

            // set the scroll value at the end
            await $('#divGviewerTableScroll' + tabId).scrollTop(100000);
            setTimeout(() => {
                instance.viewer.table.scrollLocked = false
            }, 250)

        } else {
            $('#tblGviewerTable' + tabId + ' >tbody').empty()
            instance.viewer.table.scrollLocked = false
        }

        return false
    }

    if (e.originalEvent.ctrlKey === true && e.originalEvent.key === 'Home') {
        const instance = app.ui.gViewer.instance[tabId];

        instance.viewer.table.scrollLocked = true

        await app.ui.gViewer.table.processPath(tabId)

        setTimeout(() => {
            instance.viewer.table.scrollLocked = false
        }, 250)

        return false
    }
}

app.ui.gViewer.table.instanceInit = tabId => {
    const instance = app.ui.gViewer.instance[tabId];

    // load default values
    instance.viewer.table.headers.fontSize = app.userSettings.globalViewer.themes[app.userSettings.globalViewer.defaultTheme].table.headers.fontSize;
    instance.viewer.table.rows.fontSize = app.userSettings.globalViewer.themes[app.userSettings.globalViewer.defaultTheme].table.content.fontSize;
    instance.viewer.table.rows.right.pieceChar = app.userSettings.globalViewer.themes[app.userSettings.globalViewer.defaultTheme].table.content.values.pieceChar.char;
    instance.viewer.table.rows.right.showPieces = app.userSettings.globalViewer.themes[app.userSettings.globalViewer.defaultTheme].table.content.values.showPieces;
    instance.viewer.table.bookmarkColor = app.userSettings.globalViewer.themes[app.userSettings.globalViewer.defaultTheme].table.bookmarkColor;

    instance.viewer.themes = JSON.parse(JSON.stringify(app.userSettings.globalViewer.themes));

    instance.viewer.buffer.defaults = app.userSettings.globalViewer.options.table;

    instance.viewer.appearance.theme = app.userSettings.globalViewer.defaultTheme;
    if (instance.viewer.appearance.theme !== 'light') {
        $('#btnGviewerTheme' + tabId)
            .attr('aria-pressed', false)
            .addClass('active');
    }
    // render headers  using default values
    app.ui.gViewer.table.initTable(tabId);

    // set default values on toolbar
    let $toolbarLabel;

    switch (instance.viewer.table.rows.right.showPieces) {
        case 'no': {
            $toolbarLabel = $('#lblGviewerToolbarPiecesNo' + tabId);

            break
        }
        case 'yes': {
            $toolbarLabel = $('#lblGviewerToolbarPiecesYes' + tabId);

            break
        }
        case 'pills': {
            $toolbarLabel = $('#lblGviewerToolbarPiecesPills' + tabId);
        }
    }

    $toolbarLabel.addClass('active');

    // mount handlers
    $('#divGviewerTableScroll' + tabId).on('scroll', (e) => app.ui.gViewer.data.scroll(tabId));

    $('#lblGviewerToolbarPiecesNo' + tabId).on('click', () => app.ui.gViewer.table.changeRightView(tabId, 'no'));
    $('#lblGviewerToolbarPiecesYes' + tabId).on('click', () => app.ui.gViewer.table.changeRightView(tabId, 'yes'));
    $('#lblGviewerToolbarPiecesPills' + tabId).on('click', () => app.ui.gViewer.table.changeRightView(tabId, 'pills'));
};

app.ui.gViewer.table.initTable = tabId => {
    const instance = app.ui.gViewer.instance[tabId];

    app.ui.gViewer.table.initHeader(tabId);

    $('#tblGviewerTable' + tabId + ' >tbody').css('font-size', instance.viewer.table.rows.fontSize + 'px');
};

app.ui.gViewer.table.initHeader = tabId => {
    const instance = app.ui.gViewer.instance[tabId];
    const headers = instance.viewer.themes[app.userSettings.globalViewer.defaultTheme].table.headers;

    let header = '';

    header += '<tr style="font-size: ' + headers.fontSize + '; font-weight: 600;">';

    header += '<th class="gviewer-table-th" scope="col" style="width: 60px; color: ' + headers.bookmark.fore + '; background: ' + headers.bookmark.back + '; vertical-align: middle;"></th>';

    header += '<th name="resizable" class="gviewer-table-th" scope="col" style="width: 40%; color: ' + headers.fore + '; background: ' + headers.back + '; vertical-align: middle;">Nodes</th>';

    header += '<th name="resizable" class="gviewer-table-th" scope="col" style="color: ' + headers.fore + '; background: ' + headers.back + '; vertical-align: middle;"><span style="padding-top: 5px; display: inline-grid;">Data</span>' +
        '<div class="btn-group" role="group" style="float: right; position: sticky;">' +
        '<button type="button" id="btnGviewerPieceChar' + tabId + '" class="btn btn-outline-info btn-sm dropdown-toggle gviewer-table-toolbar" data-toggle="dropdown" aria-expanded="false" title="Select a piece character separator">' +
        'Piece char:&nbsp;&nbsp;' + instance.viewer.table.rows.right.pieceChar + '&nbsp;&nbsp;' +
        '</button>' +
        '<div class="dropdown-menu" style="z-index: 10000000">' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'^\')" class="dropdown-item" href="#">^</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'!\')" class="dropdown-item" href="#">!</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'@\')" class="dropdown-item" href="#">@</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'#\')" class="dropdown-item" href="#">#</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'$\')" class="dropdown-item" href="#">$</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'%\')" class="dropdown-item" href="#">%</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'&\')" class="dropdown-item" href="#">&</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'\\\\\')" class="dropdown-item" href="#">&bsol;</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'|\')" class="dropdown-item" href="#">|</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'.\')" class="dropdown-item" href="#">.</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\',\')" class="dropdown-item" href="#">,</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\':\')" class="dropdown-item" href="#">:</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\';\')" class="dropdown-item" href="#">;</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'/\')" class="dropdown-item" href="#">/</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'*\')" class="dropdown-item" href="#">*</a>' +
        '<a onclick="app.ui.gViewer.table.changePieceChar(\'' + tabId + '\',\'~\')" class="dropdown-item" href="#">~</a>' +
        '</div>' +
        '</div>' +

        '<div class="btn-group btn-group-toggle" data-toggle="buttons" style="float: right;">' +
        '<label class="btn btn-outline-info btn-sm gviewer-table-toolbar" id="lblGviewerToolbarPiecesNo' + tabId + '"  title="Display the node value without highlights"><input type="radio" id=btnGviewerToolbarPiecesNo' + tabId + '">Pn^Pn</label> ' +
        '<label class="btn btn-outline-info btn-sm gviewer-table-toolbar" id="lblGviewerToolbarPiecesYes' + tabId + '" title="Display the node value highlighting the selected piece character"><input type="radio" id=btnGviewerToolbarPiecesYes' + tabId + '">Pn' + '<span style="background: #ffd4d1; color: blue;" >^</span>Pn</label> ' +
        '<label class="btn btn-outline-info btn-sm gviewer-table-toolbar" id="lblGviewerToolbarPiecesPills' + tabId + '"  title="Display the node value splitting the pieces and prefixing them with the piece number using the selected piece char"><input type="radio" id=btnGviewerToolbarPiecesPills' + tabId + '"><span style="border-radius: 7px; background: lightblue; color: red">&nbsp;&nbsp;Pn&nbsp;&nbsp;</span></label>' +
        '<span style="width:10px;"></span>' +
        '</div' +
        '</th>';

    header += '</tr>';

    $('#tblGviewerTable' + tabId + ' >thead')
        .empty()
        .append(header);

    const table = document.getElementById('tblGviewerTable' + tabId);
    const cols = table.querySelectorAll('th');

    // Mount the column resizer
    [].forEach.call(cols, function (col) {
        if (col.getAttribute('name') === 'resizable') {
            // Create a resizer element
            const resizer = document.createElement('div');
            resizer.classList.add('resizer')

            // Set the height
            resizer.style.height = `${table.offsetHeight}px`;

            // Add a resizer element to the column
            col.appendChild(resizer);

            // Will be implemented in the next section
            createResizableColumn(col, resizer);
        }
    });
};

app.ui.gViewer.table.processPath = async (tabId) => {
    const instance = app.ui.gViewer.instance[tabId];

    instance.viewer.buffer.scrollMode = 'head'

    // fetch data
    const res = await app.ui.gViewer.data.initialFetch(instance);
    if (res === -1) return;        // error encountered (msg already displayed)

    // populate table using default values if recordCount > 0
    if (res > 0) {
        app.ui.gViewer.table.populate(tabId);

        // reset the scroll value
        $('#divGviewerTableScroll' + tabId).scrollTop(0);

    } else {
        $('#tblGviewerTable' + tabId + ' >tbody').empty()
    }
};

// **********************************************
// TABLE POPULATION
// **********************************************

app.ui.gViewer.table.populate = (tabId, mode = 'new', data = 'all', offset = 0) => {
    // mode describes what we do here:
    // new              empty the table and replace it with data
    // append           will append the data specified in 'data' to the existing table
    // insert           inserts the records at the beginning of the table

    // data             describes the data source of the operation:
    // 'all'            will redraw all the data in the buffer
    // 'n1-n2'          will fetch data from n1 up to n2

    const instance = app.ui.gViewer.instance[tabId];
    const $tbody = $('#tblGviewerTable' + tabId + ' >tbody');
    let rows = '';

    // clear table if needed
    if (mode === 'new') $tbody.empty();

    // what data
    if (data === 'all') {
        instance.viewer.buffer.data.forEach((rowData, ix) => {
            rows += app.ui.gViewer.table.renderRow(tabId, rowData, ix + Math.abs(offset))
        })

    } else {
        const range = data.split('-');

        for (let ix = parseInt(range[0]); ix < parseInt(range[1]); ix++) {
            const rowData = instance.viewer.buffer.data[ix];

            if (!rowData) continue

            rows += app.ui.gViewer.table.renderRow(tabId, rowData, ix + Math.abs(offset))
        }
    }

    if (mode === 'insert') {
        const data = $tbody.find('tr');

        $tbody
            .empty()
            .append(rows)
            .append(data);

    } else {
        $tbody.append(rows);
    }

    $tbody.find('tr > td:first-child')
        .unbind()
        .on('click', (e) => app.ui.gViewer.table.bookmarkClicked(instance, e));
};

app.ui.gViewer.table.behead = (tabId, recordCount) => {
    console.log('deleting head:', recordCount)
    const tblGviewerTable = $('#tblGviewerTable' + tabId + ' >  tbody');
    const divGviewerTableScroll = $('#divGviewerTableScroll' + tabId);

    const $cloneTable = tblGviewerTable.clone(true);

    $cloneTable.find('tr').slice(0, recordCount).remove();
    tblGviewerTable.replaceWith($cloneTable);

    divGviewerTableScroll.scrollTop(divGviewerTableScroll.scrollTop() - 800);
};

app.ui.gViewer.table.betail = (tabId, recordCount) => {
    console.log('deleting tail:', recordCount)
    const tblGviewerTable = $('#tblGviewerTable' + tabId + ' > tbody');
    const divGviewerTableScroll = $('#divGviewerTableScroll' + tabId);

    let data = tblGviewerTable.find('tr');
    data.splice(-recordCount, recordCount);

    tblGviewerTable
        .empty()
        .append(data);

    divGviewerTableScroll.scrollTop(divGviewerTableScroll.scrollTop() + 5500);
};

app.ui.gViewer.table.refresh = (tabId, location = 'row', range = 'all') => {
    // location described what we are updating
    // row              all rows
    // colBookmarks     bookmarks column
    // colLeft          the left column
    // colRight         the right column
    // range            describes which rows will be affected by the operation:
    // 'all'            all
    // 'n1-n2'          will affect rows from n1 up to n2

    const instance = app.ui.gViewer.instance[tabId];

    if (range === 'all') {
        if (location === 'colBookmarks') {
            const tblGviewerTable = $('#tblGviewerTable' + tabId + ' > tbody');
            const clone = tblGviewerTable.clone(true);

            clone.find('tr').each((ix, row) => {
                row.cells[0].innerHTML = app.ui.gViewer.table.renderBookmarks(tabId, ix)
            });

            tblGviewerTable.replaceWith(clone);
        }

        if (location === 'colLeft') {
            const tblGviewerTable = $('#tblGviewerTable' + tabId + ' > tbody');
            const clone = tblGviewerTable.clone(true);

            clone.find('tr').each((ix, row) => {
                row.cells[1].innerHTML = app.ui.gViewer.table.renderLeft(instance, instance.viewer.buffer.data[ix])
            });

            tblGviewerTable.replaceWith(clone);
        }

        if (location === 'colRight') {
            const tblGviewerTable = $('#tblGviewerTable' + tabId + ' > tbody');
            const clone = tblGviewerTable.clone(true);

            clone.find('tr').each((ix, row) => {
                row.cells[2].innerHTML = app.ui.gViewer.table.renderRight(instance, instance.viewer.buffer.data[ix])
            });

            tblGviewerTable.replaceWith(clone);
        }

        if (location === 'row') {
            const tblGviewerTable = $('#tblGviewerTable' + tabId + ' > tbody');
            const clone = tblGviewerTable.clone(true);

            clone.find('tr').each((ix, row) => {
                row.innerHTML = app.ui.gViewer.table.renderRow(tabId, instance.viewer.buffer.data[ix], ix)
            });

            tblGviewerTable.replaceWith(clone);
        }
    }
};

// **********************************************
// tbody operations
// **********************************************

app.ui.gViewer.table.renderRow = (tabId, rowData, ix) => {
    const instance = app.ui.gViewer.instance[tabId];

    let row = '<tr style="overflow: hidden" id="rowTableGviewer' + tabId + '-' + (ix) + '">';
    row += app.ui.gViewer.table.renderBookmarks(tabId, ix);

    row += app.ui.gViewer.table.renderLeft(instance, rowData);

    row += app.ui.gViewer.table.renderRight(instance, rowData);

    row += '</tr>';

    return row
};

app.ui.gViewer.table.renderBookmarks = (tabId, ix) => {
    const instance = app.ui.gViewer.instance[tabId];
    const settings = instance.viewer.themes[instance.viewer.appearance.theme].table.content;

    return '<td id="gviewer' + tabId + '-row-' + ix + '" class="global-bookmark inconsolata' + (instance.viewer.buffer.scrollMode === 'head' ? ' hand' : '') + '" style="color: ' + settings.bookmarks.fore + '; background: ' + (instance.viewer.buffer.bookmarks[ix] === undefined || instance.viewer.buffer.bookmarks[ix] === '' ? settings.bookmarks.back : instance.viewer.buffer.bookmarks[ix]) + ';">' + (instance.viewer.buffer.scrollMode === 'tail' ? '' : (ix + 1)) + '</td>';
};

app.ui.gViewer.table.renderLeft = (instance, rowData) => {
    const settings = instance.viewer.themes[instance.viewer.appearance.theme].table.content;

    let col = '<td>';

    // global
    col += '<span style="color: ' + settings.globalName + ';">' + rowData.global + '</span>';

    if (Array.isArray(rowData.subscripts)) {
        // paren
        col += '<span style="color: ' + settings.parens + ';">(</span>';

        // subscripts
        let commaLength = 0;
        rowData.subscripts.forEach(subscript => {
            if (typeof (subscript) === 'string' && app.ui.gViewer.table.detectHorolog(subscript) === true) {
                const date = app.ui.gViewer.table.parseHorolog(subscript)

                col += '<span style="color: ' + (typeof (subscript) === 'number' ? settings.numbers : settings.strings) + ';" title="' + date + '" class="global-horolog-viewer">' +
                    (typeof (subscript) === 'number' ? '' : '"') + subscript + (typeof (subscript) === 'number' ? '' : '"') +
                    '</span>';

            } else {
                col += '<span style="color: ' + (typeof (subscript) === 'number' ? settings.numbers : settings.strings) + ';">' +
                    (typeof (subscript) === 'number' ? '' : '"') + subscript + (typeof (subscript) === 'number' ? '' : '"') +
                    '</span>';
            }

            const comma = '<span style="color: ' + settings.comma + ';">,</span>';
            commaLength = comma.length;
            col += comma
        });

        // remove last comma
        col = col.slice(0, -commaLength);

        // paren
        col += '<span style="color: ' + settings.parens + ';">)</span>';
    }

    col += '</td>';

    return col
};

app.ui.gViewer.table.renderRight = (instance, rowData) => {
    const settings = instance.viewer.themes[instance.viewer.appearance.theme].table.content.values;
    const instSettings = instance.viewer.table.rows.right;
    const value = rowData.value;
    const isNumber = typeof (value) === 'number';

    let col = '<td>';

    switch (instSettings.showPieces) {
        case 'no': {
            // it is a valid JSON ?
            let json = app.ui.gViewer.table.detectJson(value)

            if (json !== null) {
                col += '<span onclick="app.ui.gViewer.table.displayJSON(event, \'' + json + '\')" style="color: ' + settings.piecePills.fore + '; background: ' + settings.piecePills.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + '; cursor: pointer">JSON' + '</span><span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">&nbsp;' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';

            } else {
                if (app.ui.gViewer.table.detectHorolog(value) === true) {
                    const date = app.ui.gViewer.table.parseHorolog(value)

                    col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + ';"class="global-horolog-viewer" title="' + date + '">' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';

                } else {
                    col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';
                }
            }

            break
        }

        case 'yes': {
            if (isNumber === true) {
                col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">' + value + '</span>';

            } else {
                const valuePieces = value.split(instSettings.pieceChar);
                if (valuePieces.length === 1) {
                    // it is a valid JSON ?
                    let json = app.ui.gViewer.table.detectJson(value)

                    if (json !== null) {
                        col += '<span onclick="app.ui.gViewer.table.displayJSON(event, \'' + json + '\')" style="color: ' + settings.pieceChar.fore + '; background: ' + settings.pieceChar.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + '; cursor: pointer">JSON' + '</span><span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">&nbsp;' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';

                    } else {
                        if (app.ui.gViewer.table.detectHorolog(value) === true) {
                            const date = app.ui.gViewer.table.parseHorolog(value)

                            col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + ';"class="global-horolog-viewer" title="' + date + '">' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';

                        } else {
                            col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';
                        }
                    }
                } else {
                    col += '"';

                    valuePieces.forEach((piece, ix) => {
                        // value
                        // it is a valid JSON ?
                        let json = app.ui.gViewer.table.detectJson(piece)

                        if (json !== null) {
                            col += '<span onclick="app.ui.gViewer.table.displayJSON(event, \'' + json + '\')" style="color: ' + settings.pieceChar.fore + '; background: ' + settings.pieceChar.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + '; cursor: pointer"> JSON' + '</span><span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">&nbsp;' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';

                        } else {
                            if (app.ui.gViewer.table.detectHorolog(piece) === true) {
                                const date = app.ui.gViewer.table.parseHorolog(piece)

                                col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + ';" class="global-horolog-viewer"  title="' + date + '">' + piece + '</span>';

                            } else {
                                col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">' + piece + '</span>';
                            }

                            // piece
                            if (ix < valuePieces.length - 1) {
                                col += '<span style="color: ' + settings.pieceChar.fore + '; background: ' + settings.pieceChar.back + '">' + instSettings.pieceChar + '</span>';
                            }
                        }
                    });

                    col += '"';
                }
            }

            break
        }

        case 'pills': {
            if (isNumber === true) {
                col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">' + value + '</span>';

            } else {
                const valuePieces = value.split(instSettings.pieceChar);
                if (valuePieces.length === 1) {
                    if (value === '') {
                        // pill only
                        col += '<span style="color: ' + settings.piecePills.fore + '; background: ' + settings.piecePills.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + ';">&lt;EMPTY STRING&gt;</span>';

                    } else {
                        // it is a valid JSON ?
                        let json = app.ui.gViewer.table.detectJson(value)

                        if (json !== null) {
                            // pill w json
                            col += '<span onclick="app.ui.gViewer.table.displayJSON(event, \'' + json + '\')" style="color: ' + settings.piecePills.fore + '; background: ' + settings.piecePills.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + '; cursor: pointer">P1 JSON' + '</span>';

                        } else {
                            // pill
                            col += '<span style="color: ' + settings.piecePills.fore + '; background: ' + settings.piecePills.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + ';">P1' + '</span>';

                        }

                        // data
                        if (app.ui.gViewer.table.detectHorolog(value) === true) {
                            const date = app.ui.gViewer.table.parseHorolog(value)

                            col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + ';" class="global-horolog-viewer"  title="' + date + '">&nbsp;' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';

                        } else {
                            col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">&nbsp;' + (isNumber ? '' : "\"") + value + (isNumber ? '' : "\"") + '</span>';
                        }
                    }

                } else {
                    valuePieces.forEach((piece, ix) => {
                        // it is a valid JSON ?
                        let json = app.ui.gViewer.table.detectJson(piece)

                        if (json !== null) {
                            // pill w json
                            col += '<span onclick="app.ui.gViewer.table.displayJSON(event, \'' + json + '\')" style="color: ' + settings.piecePills.fore + '; background: ' + settings.piecePills.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + '; cursor: pointer">P' + (ix + 1) + ' JSON' + '</span>';

                        } else {
                            // pill
                            col += '<span style="color: ' + settings.piecePills.fore + '; background: ' + settings.piecePills.back + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.borderRadius + ';">P' + (ix + 1) + '</span>';
                        }

                        // value
                        if (app.ui.gViewer.table.detectHorolog(piece) === true) {
                            const date = app.ui.gViewer.table.parseHorolog(piece)

                            col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + ';" class="global-horolog-viewer" title="' + date + '">&nbsp;' + piece + '&nbsp;</span>';

                        } else {
                            col += '<span style="color: ' + settings.data.fore + '; background: ' + settings.data.back + '">&nbsp;' + piece + '&nbsp;</span>';
                        }
                    })
                }
            }
        }
    }

    if (rowData.hasMore === true) {
        col += '<span style="padding-right: 5px; padding-left: 10px;"><button class="btn btn-outline-success btn-sm" style="height: 20px; padding: 0 5px ;" type="button" onclick="app.ui.gViewer.table.globalValue.show(\'' + rowData.path.replace(/"/g, "~") + '\')">…</button></span>'
    }

    col += '</td>';

    return col
};

// **********************************************
// table operations
// **********************************************

app.ui.gViewer.table.setFocus = tabId => {
    $('#tblGviewerTable' + tabId).focus()
}

app.ui.gViewer.table.zoom = (tabId, mode) => {
    const instance = app.ui.gViewer.instance[tabId];
    let fontSize = instance.viewer.table.rows.fontSize;

    $('body').addClass('wait');

    if (mode === '-') {
        if (fontSize > 6) {
            fontSize--

        } else {
            $('html').removeClass('wait');

            return
        }

    } else {
        if (fontSize < 35) {
            fontSize++

        } else {
            $('html').removeClass('wait');

            return
        }
    }

    instance.viewer.table.rows.fontSize = fontSize;

    const $tbody = $('#tblGviewerTable' + tabId + ' > tbody');
    const clone = $tbody.clone(true);
    clone.css('font-size', fontSize + 'px');
    $tbody.replaceWith(clone);

    $('body').removeClass('wait');
};

app.ui.gViewer.table.changeRightView = (tabId, type) => {
    const instance = app.ui.gViewer.instance[tabId];

    instance.viewer.table.rows.right.showPieces = type;

    $('body').addClass('wait');

    app.ui.gViewer.table.refresh(tabId, 'colRight');

    $('body').removeClass('wait');
};

app.ui.gViewer.table.changePieceChar = (tabId, char) => {
    const instance = app.ui.gViewer.instance[tabId];

    $('body').addClass('wait');

    $('#btnGviewerPieceChar' + tabId).html('Piece char:&nbsp;&nbsp;' + char + '&nbsp;&nbsp;');
    instance.viewer.table.rows.right.pieceChar = char;

    app.ui.gViewer.table.refresh(tabId, 'colRight');

    $('body').removeClass('wait');
};

app.ui.gViewer.table.bookmarkClicked = (instance, e) => {
    if (instance.viewer.buffer.scrollMode !== 'head') return

    const ix = e.target.id.split('-')[4];

    if (isNaN(ix)) return

    if (e.target.style.backgroundColor === instance.viewer.table.bookmarkColor) {
        e.target.style.backgroundColor = app.userSettings.globalViewer.themes[instance.viewer.appearance.theme].table.content.bookmarks.back;
        instance.viewer.buffer.bookmarks[ix] = '';

    } else {
        e.target.style.backgroundColor = instance.viewer.table.bookmarkColor;
        instance.viewer.buffer.bookmarks[ix] = e.target.style.backgroundColor;

    }
};

app.ui.gViewer.table.theme = tabId => {
    const instance = app.ui.gViewer.instance[tabId];
    const tblGviewerTable = $('#tblGviewerTable' + tabId);

    const lightFlag = $('#btnGviewerTheme' + tabId).attr('aria-pressed');
    instance.viewer.appearance.theme = lightFlag === 'true' ? 'light' : 'dark';


    if (lightFlag === 'true') {
        instance.viewer.appearance.theme = 'light';
        tblGviewerTable.removeClass('table-dark');

    } else {
        instance.viewer.appearance.theme = 'dark';
        tblGviewerTable.addClass('table-dark');

    }

    $('body').addClass('wait');

    app.ui.gViewer.table.populate(tabId);

    $('body').removeClass('wait');
};

// **********************************************
// utils
// **********************************************

const createResizableColumn = function (col, resizer) {
    // Track the current position of mouse
    let x = 0;
    let w = 0;

    const mouseDownHandler = function (e) {
        // Get the current mouse position
        x = e.clientX;

        // Calculate the current width of column
        const styles = window.getComputedStyle(col);
        w = parseInt(styles.width, 10);

        // Attach listeners for document's events
        document.addEventListener('mousemove', mouseMoveHandler);
        document.addEventListener('mouseup', mouseUpHandler);

        resizer.classList.add('resizing');
    };

    const mouseMoveHandler = function (e) {
        // Determine how far the mouse has been moved
        const dx = e.clientX - x;

        // Update the width of column
        col.style.width = `${w + dx - 2}px`;
    };

    // When user releases the mouse, remove the existing event listeners
    const mouseUpHandler = function () {
        document.removeEventListener('mousemove', mouseMoveHandler);
        document.removeEventListener('mouseup', mouseUpHandler);

        resizer.classList.remove('resizing');
    };

    resizer.addEventListener('mousedown', mouseDownHandler);

    resizer.classList.remove('resizing');
};

app.ui.gViewer.table.globalValue = {};

app.ui.gViewer.table.globalValue.show = async path => {
    path = path.replace(/~/g, "\"");
    try {
        const res = await app.REST._getData(path);

        if (res.result === 'ERROR') {
            app.ui.msgbox.show('The following error occurred while fetching the data:\n' + res.error.description, 'ERROR');

            return
        }

        $('#h6GlobalNodeValue').text(path);
        $('#txtGlobalNodeValue').text(res.data);

        $('#modalGlobalNodeValue').modal({show: true, backdrop: 'static'});

    } catch (err) {
        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');

    }
};

app.ui.gViewer.table.detectHorolog = data => {
    if (typeof data !== 'string') return false

    const splitted = data.split(',')

    // if first piece contains chars !== numbers, is NOT a $h / $zh
    if (splitted[0] === '' || /^\d+$/.test(splitted[0]) === false || splitted.length === 1) return false

    // now we check the other 1 / 3 pieces
    let isNumber = true
    for (let ix = 1; ix < splitted.length; ix++) {
        if (splitted[ix] === '' || /^[\d ()+-]+$/.test(splitted[ix]) === false) isNumber = false
    }

    return isNumber
}

app.ui.gViewer.table.parseHorolog = mumpsHorolog => {
    const MUMPS_EPOCH_OFFSET = 883612800000; // milliseconds between 12/31/1840 and 1/1/1970
    const splitted = mumpsHorolog.split(',')

    // convert to milliseconds
    const FULL_DAY = 86400000
    const mumpsHorologMillis = splitted[0] * FULL_DAY + (splitted[1] * 1000);
    // convert to js date
    let jsDate = new Date(MUMPS_EPOCH_OFFSET + mumpsHorologMillis)
    // adjust the year
    jsDate.setFullYear(jsDate.getFullYear() - 157)

    // and format it
    jsDate = jsDate.toString().slice(0, 24)

    if (splitted.length > 2) {
        // microseconds
        jsDate += '.' + splitted[2]

        const offset = parseInt(splitted[3])
        if (offset !== 0) {
            const hours = offset / 3600
            jsDate += ' GMT' + (hours < 0 ? '' : '+') + hours
        }
    }

    return jsDate
}

app.ui.gViewer.table.detectJson = data => {
    if (typeof data !== 'string' ||
        (typeof data === 'string' &&
            (data === '' ||
                (data.charAt(0) !== '{'
                    && data.charAt(0) !== '['
                )
            )
        ))
        return null

    try {
        const obj = JSON.parse(data)

        let json = JSON.stringify(obj, null, 4)
        json = json.replace(/"/g, "<dblquote>")
        json = json.replace(/'/g, "<quote>")
        json = json.replace(/\n/g, "<br>")

        return json

    } catch (e) {
        return null
    }
}

app.ui.gViewer.table.displayJSON = (e, json) => {
    e.stopPropagation()

    json = json.replace(/<dblquote>/g, "\"")
    json = json.replace(/<quote>/g, "\'")
    json = '<pre>' + json + '</pre>'

    $('#divGviewerJsonViewer')
        .css('display', 'block')
        .css('left', e.clientX + 'px')
        .css('top', e.clientY + 'px')
        .resizable()

    $('.gviewer-json-viewer-content')
        .html(json)
}
