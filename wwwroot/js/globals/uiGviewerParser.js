/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.gViewer.parser = {};

app.ui.gViewer.parser.autoComplete = {};

app.ui.gViewer.parser.payload = {};

//******************************************
// Keyboard handlers
//******************************************
app.ui.gViewer.parser.keyUp = (type, e) => {
    // skip cursor down
    if (e.originalEvent.which === 16 || e.originalEvent.which === 40 || e.originalEvent.which === 38 || e.originalEvent.which === 13) return;

    // This is used to clear the error when no standard chars are pressed (backspace, cursor, etc)
    const instance = app.ui.gViewer.instance[type];

    if (instance.errorSet === 1) {
        instance.errorSet++;

    } else if (instance.errorSet === 2) {
        $('#inpGviewerPath' + type).removeClass('is-invalid');
        $('#lblGviewerPath' + type).text('');

        instance.errorSet = 0;
    }

    // AUTO COMPLETE
    let path = $('#inpGviewerPath' + type).val();
    instance.status.parenOpen = path.indexOf('(') > -1;


    if (path.charAt(0) === '^' && path.charAt(1) === '^') {
        path = path.substring(1);
        $('#inpGviewerPath' + type).val(path)
    }

    if (path.charAt(0) === '^' && instance.status.parenOpen === false) {
        app.ui.gViewer.parser.autoComplete.populate(type, path, 'SC')
    }
};

app.ui.gViewer.parser.keyPress = (type, e) => {
    const inpGviewerPath = $('#inpGviewerPath' + type);
    const path = inpGviewerPath.val();
    const charCode = e.which || e.originalEvent.which
    const instance = app.ui.gViewer.instance[type];
    instance.status.parenOpen = path.indexOf('(') > -1;

    // ENTER, submit
    if (charCode === 13) {
        // eventually close the autocomplete
        if (inpGviewerPath.catcomplete !== undefined) {
            try {
                $('#inpGviewerPath' + type).catcomplete('close');
            } catch (e) {
            }
        }

        // perform full validation
        app.ui.gViewer.parser.parse(type, path);

        return false
    }
};

app.ui.gViewer.parser.focus = type => {
    // clear the warning
    $('#altGviewerNoRecordsFound' + type).css('display', 'none');
};

//******************************************
// Autocomplete
//******************************************

app.ui.gViewer.parser.autoComplete.init = type => {
    // init the autocomplete
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _create: function () {
            this._super();
            this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
        },
        _renderMenu: function (ul, items) {
            let that = this,
                currentCategory = "";
            $.each(items, function (index, item) {
                let li;
                if (item.category !== currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
                if (item.category) {
                    li.attr("aria-label", item.category + " : " + item.label);
                }
            });
        }
    });

    $('#inpGviewerPath' + type).on('catcompleteselect', (event, ui) => {
        const path = ui.item.value;
        if (ui.item.category === 'Previously used') {
            // perform full validation
            app.ui.gViewer.parser.parse(type, path);
        }
    });
};

app.ui.gViewer.parser.autoComplete.populate = async (type, path, location) => {
    // type: index
    // path: the path
    // location: 'SC' or 'C' for Server & Client or Client only

    let data = [];

    if (location.indexOf('S') > -1) {
        let res;

        try {
            res = await app.REST._globalsFind(path.slice(1), path.length < app.userSettings.globalViewer.options.pathBar.fetchAt ? app.userSettings.globalViewer.options.pathBar.fetchAllSize : 0);
        } catch (err) {
            app.ui.msgbox.show(app.REST.parseError(err), 'ERROR');
            return
        }

        if (Array.isArray(res.data)) {
            res.data.forEach(serverPath => {
                data.push(
                    {
                        label: serverPath.charAt(0) === '^' ? serverPath : '^' + serverPath,
                        category: 'Found in server'
                    }
                )
            })
        }
    }
    app.userSettings.globalViewer.lastUsed.forEach(clientPath => {
        if (clientPath.indexOf(path) > -1) {
            data.push(
                {
                    label: clientPath,
                    category: 'Previously used'
                }
            )
        }
    });

    app.ui.gViewer.parser.autoComplete.setAndSearch(type, data)
};

app.ui.gViewer.parser.autoComplete.setAndSearch = (type, data) => {

    // init the autocomplete
    app.ui.gViewer.parser.autoComplete.init(type);

    $('#inpGviewerPath' + type)
        .catcomplete({
            delay: 0,
            source: data
        })
        .catcomplete('search')
};

//******************************************
// Parser
//******************************************

app.ui.gViewer.parser.parse = async (type, path) => {
    const instance = app.ui.gViewer.instance[type];

    try {
        $('#inpGviewerPath' + type).catcomplete('close');

    } catch (e) {
    }

    // execute parse
    const ret = await app.ui.gViewer.parser.validateNamespace(path, type);

    // if error, display alert and select error char
    if (ret.pos > -1) {
        $('#inpGviewerPath' + type)
            .addClass('is-invalid');
        const pathInput = document.getElementById('inpGviewerPath' + type);
        pathInput.setSelectionRange(ret.pos - 1, ret.pos);

        $('#lblGviewerPath' + type).text(ret.description);
        app.ui.gViewer.instance[type].errorSet = 1;

        $('#btnGviewerPermalink' + type).attr('disabled', true)

        return
    }

    // if ok:
    app.ui.gViewer.parser.payload = ret.payload;
    instance.viewer.parserPayload = JSON.parse(JSON.stringify(ret.payload));

    $('#btnGviewerPermalink' + type).attr('disabled', false)

    // create <span> with colors
    app.ui.gViewer.parser.createPreview(type, ret.payload);

    // change tab caption
    const adjustedPath = path.length > app.userSettings.globalViewer.options.tab.maxLengthCaption ? (path.slice(0, app.userSettings.globalViewer.options.tab.maxLengthCaption) + '...') : path;
    $('#tab' + type)
        .text(adjustedPath)
        .attr('title', path)
        .html('<span class="tab-pill">&nbsp;G&nbsp;</span>&nbsp;' + adjustedPath + '&nbsp;&nbsp;&nbsp;<span onclick="app.ui.tabs.close(\'tab' + type + '\')" class="tab-close-button close" title="">x</span>');

    // save as last used path in storage
    const found = app.userSettings.globalViewer.lastUsed.find(el => {
        return el === path
    });

    if (found === undefined) {
        app.userSettings.globalViewer.lastUsed.push(path);

        // check limits
        if (app.userSettings.globalViewer.lastUsed.length > app.userSettings.globalViewer.options.lastUsedMaxItems) {
            app.userSettings.globalViewer.lastUsed.shift();
        }

        app.ui.storage.save('gViewer', app.userSettings.globalViewer);
    }

    app.ui.menu.rebuildGlobalViewerLastUsed();

    // rename all S and N as V and the null:null as commas
    app.ui.gViewer.parser.payload.subscripts.forEach(subscript => {
        if (subscript.type === 'S' || subscript.type === 'N') {
            subscript.type = 'V'
        }

        if (subscript.type === ':' && subscript.value === '' && subscript.valueEnd === '') {
            subscript.type = ','
        }

    });

    // adjust the subs if only global is returned
    if (app.ui.gViewer.parser.payload.subscripts.length === 0) {
        app.ui.gViewer.parser.payload.subscripts.push({type: '*'})
    }

    // fetch data !!!
    instance.viewer.buffer.original = app.ui.gViewer.parser.payload;
    instance.viewer.buffer.path = path
    app.ui.gViewer.table.processPath(type);

    $('#tblGviewerTable' + type).focus();
};

app.ui.gViewer.parser.validateNamespace = async (name, type) => {
    // **************************
    // Local routines
    // **************************
    this.parseSubscript = (subscripts, offset) => {
        let sub = {};
        let errorDescription = '';

        const char = subscripts.charAt(offset);

        if (char === ')') {
            if (offset !== subscripts.length - 1) {
                offset++;
                errorDescription = 'The ) char must be either the last char or inside a string'


            } else {
                offset++;
                sub = {type: '^'};
            }

        } else if (char === ',') {
            // it is an empty comma
            if (subscripts.charAt(offset - 1) === ',') {
                sub = {type: ','};
            } else if (subscripts.charAt(offset + 1) === '*') {
                sub = {type: ','}

            }

            offset++


        } else if (char === '*') {
            if (subscripts.charAt(offset + 1) !== ')') {
                offset++;
                errorDescription = 'Only the ) char is allowed after a * char'
            } else {
                offset++;
                sub = {type: '*'}
            }

        } else {
            // State machine to parse params
            let status = {
                start: true,
                firstParam: {
                    quoteOpen: false,
                    isNumber: false,
                    numberEcount: 0,
                    numberDotCount: 0,
                    value: '',
                    complete: false,
                    firstQuotePos: 0
                },
                secondParam: {
                    quoteOpen: false,
                    isNumber: false,
                    numberEcount: 0,
                    numberDotCount: 0,
                    value: '',
                    complete: false,
                    firstQuotePos: 0
                },
                type: ''
            };

            for (let iy = offset; iy < subscripts.length; iy++) {
                const char = subscripts.charAt(iy);
                const nextChar = subscripts.charAt(iy + 1);

                const node = status.firstParam.complete === true ? 'secondParam' : 'firstParam';

                // colon only
                if (status.start === true && (char === ':' && (nextChar === ',' || nextChar === ')'))) {
                    sub = {
                        type: ':',
                        value: '',
                        valueEnd: ''
                    };
                    offset += 2;

                    break
                }

                //2nd colon
                if (char === ':' && status.firstParam.complete === true) {
                    errorDescription = 'You can not have more than 2 parameters in the range';
                    offset++;

                    break
                }

                // colon as first, continue with second param
                if (status.start === true && (char === ':' && nextChar !== ',')) {
                    sub = {type: ':', value: ''};
                    status.firstParam.complete = true;
                    offset++;

                    continue
                }

                // start of first param, string or number
                if (status.start === true) {
                    if (char === '-' || char === '.' || (char.charCodeAt(0) > 47 && char.charCodeAt(0) < 58)) {
                        status[node].isNumber = true;
                        status[node].value = char;
                        if (char === '.') {
                            status[node].numberDotCount = 1
                        }
                        status.start = false;
                        offset++;

                        continue

                    } else if (char === '"') {
                        // quote found, string starting
                        status.start = false;
                        status[node].quoteOpen = true;
                        status[node].firstQuotePos = offset;
                        offset++;

                        continue

                    } else if (char === ')') {
                        if (offset === subscripts.length) {
                            offset++;
                            errorDescription = 'The ) char must be either the last char or inside a string'

                        } else {
                            offset++;

                        }

                        continue

                    } else if (char === ',') {
                        if (status.firstParam.complete === true) {
                            sub = {
                                type: ':',
                                value: status.firstParam.value,
                                valueEnd: ''
                            };
                        }

                        break

                    } else {
                        // error, invalid char
                        errorDescription = 'Invalid character';
                        offset++;

                        break
                    }
                }

                // --------------------
                // numeric
                // --------------------
                // first we check if a token terminator is found ( : or ,)
                if (status[node].isNumber === true) {
                    if (char === ',' || char === ')') {

                        const lastChar = status[node].value.slice(-1);
                        // ensure no E e (exponent) or . (fraction) are token terminator
                        if (lastChar === 'e' || lastChar === 'E' || lastChar === '.') {
                            errorDescription = 'The char ' + lastChar + ' can not be the last character';

                            break
                        }

                        // comma or paren found, finalize and return
                        sub.type = node === 'firstParam' ? 'N' : ':';
                        if (sub.type === ':') {
                            sub.type2 = 'N'
                        }

                        if (node === 'firstParam') {
                            sub.value = status[node].value

                        } else {
                            sub.valueEnd = status[node].value
                        }

                        offset++;
                        status[node].complete = true;

                        break
                    }

                    if (char === ':') {
                        // finalize and goto param 2
                        sub.type = 'N';
                        sub.type1 = 'N';
                        sub.value = status[node].value;
                        offset += 1;
                        status[node].complete = true;
                        status.start = true;

                        continue
                    }

                    if ((char.charCodeAt(0) > 47 && char.charCodeAt(0) < 58)) {
                        // numeric value, add to token
                        status[node].value += char;
                        offset++;

                        continue

                    } else if ((char === '.' && status[node].numberDotCount === 1) || ((char === 'e' || char === 'E') && status[node].numberEcount === 1)) {
                        errorDescription = 'The char ' + char + ' can not appear more than once';
                        offset++;

                        break

                    } else if (char === '.' && status[node].numberDotCount === 0) {
                        status[node].value += char;
                        status[node].numberDotCount = 1;
                        offset++;

                        continue

                    } else if ((char === 'e' || char === 'E') && status[node].numberEcount === 0) {
                        status[node].value += char;
                        status[node].numberEcount = 1;
                        offset++;

                        continue

                    } else {
                        // error: invalid character
                        errorDescription = 'Invalid character';
                        offset++;

                        break
                    }
                }

                // --------------------
                // string
                // --------------------
                if (char === '"') {
                    // handle quotes (THE MUST BE DOUBLE!!!)
                    if (nextChar === '"') {
                        // valid quote
                        status[node].value += '""';
                        offset += 2;
                        iy++;

                    } else if (nextChar === ',' || nextChar === ')') {
                        // end of string token
                        sub.type = node === 'firstParam' ? 'S' : ':';
                        if (sub.type === ':') {
                            sub.type2 = 'S'
                        }

                        offset += 2;
                        status[node].complete = true;

                        if (node === 'firstParam') {
                            sub.value = status[node].value

                        } else {
                            sub.valueEnd = status[node].value
                        }

                        break

                    } else if (nextChar === ':') {
                        // finalize and goto param 2
                        sub.type = 'S';
                        sub.type1 = 'S';
                        sub.value = status[node].value;
                        offset += 2;
                        iy++;
                        status[node].complete = true;
                        status.start = true;


                    } else {
                        // error: not escaped quote
                        errorDescription = 'Quote inside string is not escaped';
                        offset++;

                        break
                    }
                } else {
                    status[node].value += char;
                    //status.firstParam.quoteOpen = false;
                    offset++;
                }

            }

            const node = status.firstParam.complete === true ? 'secondParam' : 'firstParam';
            if (status[node].quoteOpen === true && errorDescription === '' && sub.type !== ':') {
                // error: unterminated string
                errorDescription = 'The string is not terminated';
                offset = status[node].firstQuotePos + 1;
            }
        }

        return {
            offset: offset,
            sub: sub,
            description: errorDescription
        }
    };

    // **************************
    // Start of code
    // **************************

    let payload = {                       // We will build this up while parsing, so we can submit it
        global: '',
        subscripts: [],
        text: name
    };

    // -------------------------
    // GLOBAL
    // -------------------------
    let withPercent = false;

    if (name.charAt(0) !== '^') {
        name = '^' + name;
        $('#inpGviewerPath' + type).val(name)
    }

    if (name.length === 1) {
        // No global name
        return {
            description: 'The global name is missing',
            pos: 1
        }
    }

    name = name.substring(1);

    // if no global name
    if (name.charAt(0) === '(') {
        // No up caret
        return {
            description: 'No naked reference allowed here',
            pos: 1
        }
    }

    // First determine if the name has a % and if more than 1
    const percentCount = app.ui.countChar(name.split('(')[0], '%');
    if (percentCount === 1) {
        // is the second character ?
        if (name.split('(')[0].indexOf('%') !== 0) {
            // Wrong position, abort
            return {
                description: 'The % char can only be the first character',
                pos: name.indexOf('%') + 1
            }

        } else {
            withPercent = true
        }
    } else if (percentCount > 1) {
        // Error, return the position of second %
        return {
            description: 'You can not have more than one % char',
            pos: app.ui.nthIndex(name, '%', 2) + 2
        }
    }

    // Now we check the validity of the global name
    let global = name.split('(')[0];

    // remove % if present
    if (withPercent) global = global.substring(1);

    // ensure first char is alpha
    if (global.substring(0, 1).replace(/[a-z]/gi, '').length > 0) {
        // error, return first char pos
        return {
            description: 'The first char of the global name must be a upper or lower case letter',
            pos: withPercent ? 3 : 2
        }
    }

    // ensure the rest of string is alphanumeric
    if (global.replace(/[a-z0-9]/gi, '').length > 0) {
        // error, return 1
        return {
            description: 'The global name can contain only alphanumeric characters',
            pos: 2
        }
    }

    // and length <= 31 chars
    if (global.length > (withPercent ? 30 : 31)) {
        return {
            description: 'The global name can have a maximum length of 31 characters',
            pos: 31
        }
    }

    const globalRest = withPercent === true ? '_' + global : global;
    global = withPercent === true ? '%' + global : global;

    // validate global existence in server
    const res = await app.REST._dollarData('^' + globalRest);

    if (res.data === undefined || res.data === 0) {
        return {
            description: 'The global doesn\'t exist on the server',
            pos: 2
        }
    }

    payload.global = '^' + global;

    // -------------------------
    // Subscripts
    // -------------------------
    const subscripts = name.split('(')[1];

    if (subscripts !== undefined) {

        if (subscripts.charAt(subscripts.length - 1) !== ')') {
            // no closing paren
            return {
                description: 'The paren is not closed',
                pos: name.indexOf('(') + 2
            }
        }

        if (subscripts.charAt(0) === ')') {
            // closing paren, but no subscripts
            return {
                description: 'The paren is closed, but no subscripts are present',
                pos: name.length
            }
        }

        if (subscripts !== '') {
            let offset = 0;
            let nameOffset = name.indexOf('(') + 2;             // offset of the ( char. We can use offset (based 0) to compute the error position
                                                                // and return the sum of the two for a position in the overall string
            let lastError = '';

            if (subscripts.charAt(0) === ',') {
                payload.subscripts.push({type: ','});
                offset++
            }

            while (offset < subscripts.length - 1) {
                const ret = parseSubscript(subscripts, offset);

                offset = ret.offset;

                if (ret.description === '') {
                    payload.subscripts.push(ret.sub)

                } else {
                    lastError = ret.description;

                    break;
                }
            }

            if (lastError !== '') {
                return {
                    pos: nameOffset + offset,
                    description: lastError
                }
            }
        }
    }

    // adjust last empty comma, if present
    if (payload.text.slice(-2) === ',)') {
        payload.subscripts.push({type: ','})
    }

    // remove empty objects in subscripts
    payload.subscripts = payload.subscripts.filter(el => el.type !== undefined);

    app.ui.gViewer.parser.payload = payload;

    // success
    return {
        payload: payload,
        pos: -1
    };
};

app.ui.gViewer.parser.createPreview = (type, payload) => {
    const $preview = $('#spanGviewerPathPreview' + type);
    const instance = app.ui.gViewer.instance[type];
    const titleBar = instance.viewer.themes[instance.viewer.appearance.theme].titleBar;


    $preview.empty();

    // global
    let span = '<span style="color: ' + titleBar.globalName + ';">' + payload.global + '</span>';
    if (Array.isArray(payload.subscripts) && payload.subscripts.length > 0) {

        // open paren
        span += '<span style="color: ' + titleBar.parens + ';">(</span>';

        // subscripts
        payload.subscripts.forEach(sub => {
            switch (sub.type) {
                case 'N': {
                    span += '<span style="color: ' + titleBar.numbers + ';">' + sub.value + '</span>';

                    span += '<span style="color: ' + titleBar.comma + ';">,</span>';

                    break
                }

                case 'S': {
                    span += '<span style="color: ' + titleBar.quotes + ';">"</span>';
                    span += '<span style="color: ' + titleBar.strings + ';">' + sub.value + '</span>';
                    span += '<span style="color: ' + titleBar.quotes + ';">"</span>';

                    span += '<span style="color: ' + titleBar.comma + ';">,</span>';

                    break
                }

                case ',': {
                    span += '<span style="color: ' + titleBar.comma + ';">,</span>';

                    break
                }

                case '*': {
                    span += '<span style="color: ' + titleBar.star + ';">*</span>';

                    span += '<span style="color: ' + titleBar.comma + ';">,</span>';

                    break
                }

                case ':': {
                    // left side
                    if (sub.type1 === 'S') {
                        // string
                        span += '<span style="color: ' + titleBar.quotes + ';">"</span>';
                        span += '<span style="color: ' + titleBar.strings + ';">' + sub.value + '</span>';
                        span += '<span style="color: ' + titleBar.quotes + ';">"</span>';

                    } else {
                        // number
                        span += '<span style="color: ' + titleBar.numbers + ';">' + sub.value + '</span>';
                    }

                    // colon
                    span += '<span style="color: ' + titleBar.colon + ';">:</span>';

                    // right side
                    if (sub.type2 === 'S') {
                        // string
                        span += '<span style="color: ' + titleBar.quotes + ';">"</span>';
                        span += '<span style="color: ' + titleBar.strings + ';">' + sub.valueEnd + '</span>';
                        span += '<span style="color: ' + titleBar.quotes + ';">"</span>';

                    } else {
                        // number
                        span += '<span style="color: ' + titleBar.numbers + ';">' + sub.valueEnd + '</span>';
                    }

                    span += '<span style="color: ' + titleBar.comma + ';">,</span>';
                }
            }
        });
    }

    // show the preview
    $preview
        .append(span)
        .css('display', 'block');

    if (Array.isArray(payload.subscripts) && payload.subscripts.length > 0) {
        // remove last comma
        $('#spanGviewerPathPreview' + type + ' > :last-child').remove();

        // close paren
        $preview.append('<span style="color: ' + titleBar.parens + ';">)</span>');
    }

    $('#inpGviewerPath' + type).css('display', 'none');
};

app.ui.gViewer.parser.resetPreview = type => {
    $('#spanGviewerPathPreview' + type).css('display', 'none');

    $('#inpGviewerPath' + type)
        .css('display', 'block')
        .focus();
};
