/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.integ.init = () => {
    $('#btnIntegOk').on('click', () => app.ui.integ.okPressed());

    $('#optIntegComplete').on('click', () => app.ui.integ.completePressed())
    $('#optIntegFast').on('click', () => app.ui.integ.fastPressed())

    $('#btnIntegResultCopyToClipboard').on('click', () => app.ui.integ.copyToClipboardResult())
    app.ui.setupDialogForTest('modalInteg')
    app.ui.setupDialogForTest('modalIntegResult')
};

app.ui.integ.show = () => {
    let regions = [];

    // create region list
    Object.keys(app.system.regions).forEach((region) => {
        const regionData = {};
        regionData.id = region;
        regionData.text = region;
        regionData.icon = false;
        regionData.state = {selected: false};

        regions.push(regionData)
    });

    // populate the region list
    const tree = $("#treeIntegRegions");

    tree.jstree("destroy");
    tree.jstree({
        "checkbox": {
            "keep_selected_style": false
        },
        "plugins":
            ["checkbox"],
        'core': {
            'data': regions,
            'themes': {
                'name': 'proton',
            }
        },
    });
    tree.on("changed.jstree", (e, data) => {
        app.ui.integ.validateOk();
    });

    // init default values
    $('#optIntegComplete').prop('checked', true)
    $('#optIntegSummary')
        .prop('checked', true)
        .prop('disabled', false)
    $('#optIntegDetailed').prop('disabled', false)

    $('#swIntegAdvancedOptions').prop('checked', false)
    $('#collapseIntegAdvanced').collapse('hide')
    $('#swIntegKeyRanges').prop('checked', true)
    $('#inpIntegMap').val(10)
    $('#inpIntegTransaction').val(10)
    $('#inpIntegKeySize').val(10)

    $('#swIntegRunBackground').prop('checked', false)

    $('#btnIntegOk')
        .addClass('disabled')
        .prop("disabled", true)

    // display the dialog
    $('#modalInteg').modal({show: true, backdrop: 'static'})
};

app.ui.integ.validateOk = () => {
    const tree = $("#treeIntegRegions");
    const btn = $('#btnIntegOk');

    if (tree.jstree("get_checked").length !== 0) {
        btn.removeClass('disabled');
        btn.prop("disabled", false)
    } else {
        btn.addClass('disabled');
        btn.prop("disabled", true)
    }
}

app.ui.integ.completePressed = () => {
    $('#optIntegSummary')
        .prop('disabled', false)
    $('#optIntegDetailed').prop('disabled', false)

}

app.ui.integ.fastPressed = () => {
    $('#optIntegSummary')
        .prop('disabled', true)
        .prop('checked', true)
    $('#optIntegDetailed').prop('disabled', true)
}

app.ui.integ.okPressed = () => {
    const runBackground = $('#swIntegRunBackground').is(':checked');
    const bgndText = runBackground ? 'You have chosen to run the Integrity check in the background. When it will complete it will alert you with the result.<br>' : '';

    app.ui.inputbox.show(bgndText + 'Do you want to proceed with the Integrity check operation ?', 'Defrag / Compact', async (opt) => {
        if (opt === 'YES') {
            const payload = {};
            const selRegions = [];

            const selElem = $("#treeIntegRegions").jstree("get_checked", true);
            selElem.forEach(el => {
                selRegions.push(el.id)
            });
            payload.regions = selRegions;

            payload.type = $('#optIntegComplete').prop('checked') === true ? 'complete' : 'fast'
            payload.reporting = $('#optIntegSummary').prop('checked') === true ? 'summary' : 'full'

            payload.keyRange = $('#swIntegKeyRanges').prop('checked') === true

            payload.mapErrors = $('#inpIntegMap').val()
            payload.keySizeErrors = $('#inpIntegKeySize').val()
            payload.transactionErrors = $('#inpIntegTransaction').val()

            $('#modalInteg').modal('hide');

            if (runBackground === false) {
                app.ui.wait.show("Checking the integrity of the selected regions...");

            } else {
                app.ui.toaster.startInterval('Integ')
            }

            try {
                const res = await app.REST._integ(payload)
                if (runBackground) {
                    if (res.result !== undefined && res.result === 'OK') {
                        app.ui.toaster.createToast('integ', 'Integrity check', 'Click to display the report...', 'success', res);

                    } else {
                        app.ui.toaster.createToast('integ', 'Integrity check', 'Click to display the report...', 'fatal', res);
                    }

                } else {
                    app.ui.wait.hide();

                    // display the report
                    app.ui.integ.displayResult(res)
                }

            } catch (err) {
                app.ui.wait.hide();

                app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')
            }
        }
    })
}

app.ui.integ.displayResult = result => {
    $('#divIntegResult')
        .empty()
        .append('<pre>' + result.data.dump.join('<br>'))

    // display the dialog
    $('#modalIntegResult').modal({show: true, backdrop: 'static'})
}

app.ui.integ.copyToClipboardResult = () => {
    const text = $("#divIntegResult");
    let clipboard = $("#divIntegResult").html().replace(/<br>/g, '\n').replace(/<pre>/g, '').replace(/<\/pre>/g, '')

    app.ui.copyToClipboard(clipboard, $('#btnIntegResultCopyToClipboard'))
}
