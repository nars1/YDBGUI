/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.fileSelect.init = () => {
    $('#selFileSelectList')
        .on('dblclick', () => app.ui.fileSelect.listDblClick())
    $('#btnFileSelectOk').on('click', () => app.ui.fileSelect.okPressed())

    app.ui.setupDialogForTest('modalFileSelect')
}

app.ui.fileSelect.path = ''
app.ui.fileSelect.pathFinal = false
app.ui.fileSelect.callback = null;
app.ui.fileSelect.mask = '*'

app.ui.fileSelect.show = (path, mask = '*', callback) => {
    app.ui.fileSelect.mask = mask

    app.ui.fileSelect.callback = callback

    app.ui.fileSelect.path = path
    app.ui.fileSelect.pathFinal = false
    app.ui.fileSelect.populate(path)

    $('#btnFileSelectOk')
        .addClass('disabled')
        .prop('disabled', true)

    $('#modalFileSelect').modal({show: true, backdrop: 'static'})
}

app.ui.fileSelect.populate = async () => {
    let dirList;

    $('#txtFileSelectPath').text(app.ui.fileSelect.path)

    try {
        const dir = await app.REST._ls(app.ui.fileSelect.path, 'FD')

        if (dir.result === 'OK') {
            if (app.ui.fileSelect.path !== '/' && app.ui.fileSelect.path !== '') dirList = '<option>..</option>'
            if (dir.data === undefined) {
                app.ui.fileSelect.pathFinal = true

            } else {
                dir.data.forEach(el => {
                    if (el.type === 'dir' || app.ui.fileSelect.mask === '*') {
                        dirList += '<option>' + el.path + '</option>'
                    } else if (el.type === 'file' && el.path.indexOf(app.ui.fileSelect.mask) > -1) {
                        dirList += '<option class="file-select-so"><span style="color: green;">' + el.path + '</span></option>'

                    }
                });

                app.ui.fileSelect.pathFinal = false
            }

            $('#selFileSelectList')
                .empty()
                .append(dirList)

        } else {
            app.ui.fileSelect.pathFinal = true
        }
    } catch (err) {
        app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')

    }
}
app.ui.fileSelect.listDblClick = () => {
    let triggerOkFlag = false
    const el = $('#selFileSelectList').children("option:selected")

    if (el[0] && el[0].innerHTML.indexOf('color: green') > -1) {
        $('#btnFileSelectOk')
            .removeClass('disabled')
            .prop('disabled', false)
        triggerOkFlag = true

    } else {
        $('#btnFileSelectOk')
            .addClass('disabled')
            .prop('disabled', true)

    }

    if (el.text() === '..') {
        app.ui.fileSelect.path = app.ui.fileSelect.path.substring(0, app.ui.fileSelect.path.lastIndexOf('/', app.ui.fileSelect.path.length - 2) + 1);
    } else {
        if (app.ui.fileSelect.pathFinal === true) {
            app.ui.fileSelect.path = app.ui.fileSelect.path.substring(0, app.ui.fileSelect.path.lastIndexOf('/', app.ui.fileSelect.path.length - 2) + 1);
            app.ui.fileSelect.path += el.text()

        } else {
            app.ui.fileSelect.path += (app.ui.fileSelect.path.slice(-1) === '/' ? '' : '/') + el.text()
        }
    }

    if (triggerOkFlag === true) {
        app.ui.fileSelect.okPressed()

        return
    }

    app.ui.fileSelect.populate()
}

app.ui.fileSelect.okPressed = () => {
    $('#modalFileSelect').modal('hide')

    app.ui.fileSelect.callback(app.ui.fileSelect.path)
}
