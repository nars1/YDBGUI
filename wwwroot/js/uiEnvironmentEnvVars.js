/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.envEnvVars = {
    init: function () {
        $('#btnEnvEnvVarsOk').on('click', () => this.buttonOkPressed())

        app.ui.setupDialogForTest('modalEnvEnvVars')
    },

    show: function () {
        let envVars = ''
        const divEnvEnvVarsList = $('#divEnvEnvVarsList')

        divEnvEnvVarsList.empty()

        for (let envVar in app.ui.envSettings.gldFileSelectResponse.envVars) {

            const idEnvVar = envVar.replace('$', '_')

            envVars += '<div style="display: flex;">'
            envVars += '<span>' + envVar.slice(1) + '&nbsp;=&nbsp;</span>'
            envVars += '<input type="text" class="form-control form-control-sm" id="txtEnvVarList' + idEnvVar + '" value="' + app.ui.envSettings.gldFileSelectResponse.envVars[envVar] + '" onchange="app.ui.envEnvVars.textChanged()" onkeyup="app.ui.envEnvVars.textChanged()">'
            envVars += '</div><br>'
        }

        divEnvEnvVarsList.append(envVars)

        $('#modalEnvEnvVars').modal({show: true, backdrop: 'static'})
    },

    textChanged: function () {
        let emptyText = false

        for (let envVar in app.ui.envSettings.gldFileSelectResponse.envVars) {
            envVar = envVar.replace('$', '_')

            if ($('#txtEnvVarList' + envVar).val() === '') emptyText = true
        }

        if (emptyText === true) {
            app.ui.button.disable($('#btnEnvEnvVarsOk'))
        } else {
            app.ui.button.enable($('#btnEnvEnvVarsOk'))
        }
    },

    buttonOkPressed: function () {
        app.ui.envSettings.currentEnvVars = []
        let envVarString = ''

        for (const envVar in app.ui.envSettings.gldFileSelectResponse.envVars) {
            const idName = '#txtEnvVarList' + envVar.replace('$', '_')

            app.ui.envSettings.gldFileSelectResponse.envVars[envVar] = $(idName).val()
            envVarString += envVar.slice(1) + '=' + $(idName).val() + '; '
        }

        envVarString = envVarString.slice(0, -2)

        app.ui.envSettings.currentEnvVars = envVarString
        // update text on main screen
        $('#lblEnvSettingsEnvVars').val(envVarString)
        $('#valEnvSettingsEnvVarsInvalid').text('The environment variables are set.')

        $('#modalEnvEnvVars').modal('hide')

        app.ui.envSettings.buttonValidatePressed()
    },
}
