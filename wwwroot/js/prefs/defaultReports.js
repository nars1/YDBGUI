/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

demoStatsReports = [{
    "version": 2.1, "fileType": "ydb-stats-report", "name": "demo", "description": "A demo report, capturing data using the PEEKBYNAME data source and displaying it in graph and table view.", "data": [{
        "type": "peekByName", "sampleRate": 1, "data": {
            "enabled": true, "samples": ["node_local.gvstats_rec.n_set", "node_local.gvstats_rec.n_kill", "node_local.gvstats_rec.n_get", "node_local.gvstats_rec.n_order", "node_local.gvstats_rec.n_lock_success", "node_local.gvstats_rec.n_lock_fail"], "processes": {"type": "agg"}, "regions": ["DEFAULT"], "file": "", "views": [{
                "id": "8e6b3fbc-7973-4ae1-8ebd-4f6550adea2a",
                "type": "graph",
                "mapping": [{"sample": "node_local.gvstats_rec.n_set", "abs": false, "delta": true, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_kill", "abs": false, "delta": true, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_get", "abs": false, "delta": true, "min": false, "max": false, "avg": false}, {
                    "sample": "node_local.gvstats_rec.n_order",
                    "abs": false,
                    "delta": true,
                    "min": false,
                    "max": false,
                    "avg": false
                }, {"sample": "node_local.gvstats_rec.n_lock_success", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_lock_fail", "abs": false, "delta": false, "min": false, "max": false, "avg": false}],
                "highlights": [],
                "options": {},
                "graphMapping": [{
                    "type": "*-node_local.gvstats_rec.n_set-delta",
                    "settings": {
                        "default": "line",
                        "line": {"stroke": {"type": "smooth", "smoothTension": 0.2, "color": "#ff822e", "width": 2, "dashArray": 0}, "marking": {"type": "none", "markers": {"size": 3, "color": "#ff0000", "shape": "square"}}},
                        "area": {"stroke": {"type": "smooth", "width": 2, "dashArray": 0, "color": "#ff822e"}, "fill": {"type": "gradient", "gradient": {"opacityFrom": 0.9, "opacityTo": 0.2, "type": "vertical", "shade": "dark"}}, "marking": {"type": "none", "markers": {"size": 0, "color": "#ff0000", "shape": "circle"}, "dataLabels": {"font": {"size": 9, "family": "Inconsolata", "weight": "normal", "color": "#2ed5ff"}}}},
                        "yAxis": {"show": true, "type": "linear", "min": 1, "max": 100, "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "n_set Δ", "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}
                    }
                }, {
                    "type": "*-node_local.gvstats_rec.n_kill-delta",
                    "settings": {
                        "default": "line",
                        "line": {"stroke": {"type": "smooth", "smoothTension": 0.2, "color": "#2effc0", "width": 2, "dashArray": 0}, "marking": {"type": "none", "markers": {"size": 3, "color": "#ff0000", "shape": "square"}}},
                        "area": {"stroke": {"type": "smooth", "width": 2, "dashArray": 0, "color": "#2effc0"}, "fill": {"type": "gradient", "gradient": {"opacityFrom": 0.9, "opacityTo": 0.2, "type": "vertical", "shade": "dark"}}, "marking": {"type": "none", "markers": {"size": 0, "color": "#ff0000", "shape": "circle"}, "dataLabels": {"font": {"size": 9, "family": "Inconsolata", "weight": "normal", "color": "#2ed5ff"}}}},
                        "yAxis": {"show": true, "type": "linear", "min": 1, "max": 100, "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "n_kill Δ", "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}
                    }
                }, {
                    "type": "*-node_local.gvstats_rec.n_get-delta",
                    "settings": {
                        "default": "line",
                        "line": {"stroke": {"type": "smooth", "smoothTension": 0.2, "color": "#ff2e93", "width": 2, "dashArray": 0}, "marking": {"type": "none", "markers": {"size": 3, "color": "#ff0000", "shape": "square"}}},
                        "area": {"stroke": {"type": "smooth", "width": 2, "dashArray": 0, "color": "#ff2e93"}, "fill": {"type": "gradient", "gradient": {"opacityFrom": 0.9, "opacityTo": 0.2, "type": "vertical", "shade": "dark"}}, "marking": {"type": "none", "markers": {"size": 0, "color": "#ff0000", "shape": "circle"}, "dataLabels": {"font": {"size": 9, "family": "Inconsolata", "weight": "normal", "color": "#2ed5ff"}}}},
                        "yAxis": {"show": true, "type": "linear", "min": 1, "max": 100, "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "n_get Δ", "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}
                    }
                }, {
                    "type": "*-node_local.gvstats_rec.n_order-delta",
                    "settings": {
                        "default": "line",
                        "line": {"stroke": {"type": "smooth", "smoothTension": 0.2, "color": "#962eff", "width": 2, "dashArray": 0}, "marking": {"type": "none", "markers": {"size": 3, "color": "#ff0000", "shape": "square"}}},
                        "area": {"stroke": {"type": "smooth", "width": 2, "dashArray": 0, "color": "#962eff"}, "fill": {"type": "gradient", "gradient": {"opacityFrom": 0.9, "opacityTo": 0.2, "type": "vertical", "shade": "dark"}}, "marking": {"type": "none", "markers": {"size": 0, "color": "#ff0000", "shape": "circle"}, "dataLabels": {"font": {"size": 9, "family": "Inconsolata", "weight": "normal", "color": "#2ed5ff"}}}},
                        "yAxis": {"show": true, "type": "linear", "min": 1, "max": 100, "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "n_order Δ", "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}
                    }
                }],
                "defaults": {
                    "globals": {"background": "#ffffff", "color": "#373d3f", "fontFamily": "FiraGO", "theme": "dark"},
                    "globalsDark": {"background": "#000000", "color": "#ffffff", "gridRowOpacity": 0.1},
                    "title": {"display": true, "text": "DEFAULT", "align": "center", "position": "top", "style": {"size": 15, "fontFamily": "Inconsolata", "weight": "bold", "color": "#ff7f27"}},
                    "subTitle": {"display": false, "text": "Aggregate", "align": "center", "position": "top", "style": {"size": 13, "fontFamily": "Inconsolata", "weight": "normal", "color": "#ff7f27"}},
                    "legend": {"display": true, "position": "bottom", "hAlign": "center", "font": {"size": 9, "fontFamily": "Inconsolata", "weight": "normal", "color": "#ff7f27"}},
                    "grid": {"row": {"color": "#f3f3f3"}, "column": {"color": "#f3f3f3"}, "xAxisLines": true, "yAxisLines": true},
                    "xaxis": {"labels": {"show": true, "colors": "#808080", "fontSize": 11, "fontFamily": "Inconsolata", "fontWeight": "bold"}},
                    "tooltips": {"enabled": true},
                    "yAxis": {"common": false, "defaultsCommon": {"show": true, "type": "linear", "min": 1, "max": 100, "color": "#ff7f27", "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}, "rotate": 0}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "", "style": {"colors": "#ff7f27", "fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}}
                },
                "renderIx": 0,
                "region": "DEFAULT",
                "processes": "Aggregate",
                "infoSet": false,
                "graphId": 0
            }, {
                "id": "580120b6-f786-410f-8cb8-4fc9f759c3d9",
                "type": "graph",
                "mapping": [{"sample": "node_local.gvstats_rec.n_set", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_kill", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_get", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {
                    "sample": "node_local.gvstats_rec.n_order",
                    "abs": false,
                    "delta": false,
                    "min": false,
                    "max": false,
                    "avg": false
                }, {"sample": "node_local.gvstats_rec.n_lock_success", "abs": false, "delta": true, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_lock_fail", "abs": false, "delta": true, "min": false, "max": false, "avg": false}],
                "highlights": [],
                "options": {},
                "graphMapping": [{
                    "type": "*-node_local.gvstats_rec.n_lock_success-delta",
                    "settings": {
                        "default": "line",
                        "line": {"stroke": {"type": "smooth", "smoothTension": 0.2, "color": "#ff822e", "width": 2, "dashArray": 0}, "marking": {"type": "none", "markers": {"size": 3, "color": "#ff0000", "shape": "square"}}},
                        "area": {"stroke": {"type": "smooth", "width": 2, "dashArray": 0, "color": "#ff822e"}, "fill": {"type": "gradient", "gradient": {"opacityFrom": 0.9, "opacityTo": 0.2, "type": "vertical", "shade": "dark"}}, "marking": {"type": "none", "markers": {"size": 0, "color": "#ff0000", "shape": "circle"}, "dataLabels": {"font": {"size": 9, "family": "Inconsolata", "weight": "normal", "color": "#2ed5ff"}}}},
                        "yAxis": {"show": true, "type": "linear", "min": 1, "max": 100, "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "n_lock_success Δ", "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}
                    }
                }, {
                    "type": "*-node_local.gvstats_rec.n_lock_fail-delta",
                    "settings": {
                        "default": "line",
                        "line": {"stroke": {"type": "smooth", "smoothTension": 0.2, "color": "#2effc0", "width": 2, "dashArray": 0}, "marking": {"type": "none", "markers": {"size": 3, "color": "#ff0000", "shape": "square"}}},
                        "area": {"stroke": {"type": "smooth", "width": 2, "dashArray": 0, "color": "#2effc0"}, "fill": {"type": "gradient", "gradient": {"opacityFrom": 0.9, "opacityTo": 0.2, "type": "vertical", "shade": "dark"}}, "marking": {"type": "none", "markers": {"size": 0, "color": "#ff0000", "shape": "circle"}, "dataLabels": {"font": {"size": 9, "family": "Inconsolata", "weight": "normal", "color": "#2ed5ff"}}}},
                        "yAxis": {"show": true, "type": "linear", "min": 1, "max": 100, "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "n_lock_fail Δ", "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}
                    }
                }],
                "defaults": {
                    "globals": {"background": "#ffffff", "color": "#373d3f", "fontFamily": "FiraGO", "theme": "dark"},
                    "globalsDark": {"background": "#000000", "color": "#ffffff", "gridRowOpacity": 0.1},
                    "title": {"display": true, "text": "DEFAULT", "align": "center", "position": "top", "style": {"size": 15, "fontFamily": "Inconsolata", "weight": "bold", "color": "#ff7f27"}},
                    "subTitle": {"display": false, "text": "Aggregate", "align": "center", "position": "top", "style": {"size": 13, "fontFamily": "Inconsolata", "weight": "normal", "color": "#ff7f27"}},
                    "legend": {"display": true, "position": "bottom", "hAlign": "center", "font": {"size": 9, "fontFamily": "Inconsolata", "weight": "normal", "color": "#ff7f27"}},
                    "grid": {"row": {"color": "#f3f3f3"}, "column": {"color": "#f3f3f3"}, "xAxisLines": true, "yAxisLines": true},
                    "xaxis": {"labels": {"show": true, "colors": "#808080", "fontSize": 11, "fontFamily": "Inconsolata", "fontWeight": "bold"}},
                    "tooltips": {"enabled": true},
                    "yAxis": {"common": false, "defaultsCommon": {"show": true, "type": "linear", "min": 1, "max": 100, "color": "#ff7f27", "useManualSetting": false, "labels": {"show": true, "style": {"fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}, "rotate": 0}, "axisBorder": {"show": true}, "title": {"align": "center", "text": "", "style": {"colors": "#ff7f27", "fontSize": 12, "fontFamily": "Inconsolata", "fontWeight": "normal"}}}}
                },
                "renderIx": 1,
                "region": "DEFAULT",
                "processes": "Aggregate",
                "infoSet": false,
                "graphId": 1
            }, {
                "id": "2aaf7ad2-e37c-4e8c-8827-d06891feb6e2",
                "type": "reportLine",
                "mapping": [{"sample": "node_local.gvstats_rec.n_set", "abs": true, "delta": true, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_kill", "abs": true, "delta": true, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_get", "abs": true, "delta": true, "min": false, "max": false, "avg": false}, {
                    "sample": "node_local.gvstats_rec.n_order",
                    "abs": true,
                    "delta": true,
                    "min": false,
                    "max": false,
                    "avg": false
                }, {"sample": "node_local.gvstats_rec.n_lock_success", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_lock_fail", "abs": false, "delta": false, "min": false, "max": false, "avg": false}],
                "highlights": [{"sample": "node_local.gvstats_rec.n_set", "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]}, {
                    "sample": "node_local.gvstats_rec.n_kill",
                    "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]
                }, {"sample": "node_local.gvstats_rec.n_get", "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]}, {
                    "sample": "node_local.gvstats_rec.n_order",
                    "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]
                }, {"sample": "node_local.gvstats_rec.n_lock_success", "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]}, {
                    "sample": "node_local.gvstats_rec.n_lock_fail",
                    "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]
                }],
                "options": {"orientation": "h"},
                "defaults": {
                    "header": {"border": {"color": "gray", "thickness": "1"}},
                    "region": {"background": "#5b3333", "border": {"color": "lightgray", "thickness": "0"}, "font": {"size": 24, "color": "#ff7f27", "fontFamily": "Inconsolata", "weight": "bold", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "center", "text": "GLOBALS"},
                    "process": {"background": "#000000", "border": {"color": "silver", "thickness": "1"}, "font": {"size": 15, "color": "#eab83b", "fontFamily": "Inconsolata", "weight": "normal", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "left", "show": false},
                    "sample": {"background": "#000000", "border": {"color": "silver", "thickness": "1"}, "font": {"size": 17, "color": "#ff7f27", "fontFamily": "Inconsolata", "weight": "bold", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "center"},
                    "sampleMap": {"background": "#000000", "border": {"color": "silver", "thickness": "1"}, "font": {"size": 17, "color": "#ffffff", "fontFamily": "Inconsolata", "weight": "normal", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "center"},
                    "value": {"background": "#000000", "border": {"color": "gray", "thickness": "1"}, "font": {"size": 15, "color": "#64a555", "fontFamily": "Inconsolata", "weight": "bold", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "dotted", "thickness": 2}}, "align": "center", "formatting": {"thousands": true}},
                    "noValue": {"background": "#000000", "border": {"color": "gray", "thickness": "1"}},
                    "highlighters": {"low": "#64a555", "mid": "#eab83b", "high": "#ce3a3a", "topProcess": "#64a555"}
                },
                "renderIx": 2,
                "region": "DEFAULT",
                "processes": "Aggregate",
                "infoSet": false
            }, {
                "id": "00a7739a-6075-4fcb-8650-1c9c45abace0",
                "type": "reportLine",
                "mapping": [{"sample": "node_local.gvstats_rec.n_set", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_kill", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_get", "abs": false, "delta": false, "min": false, "max": false, "avg": false}, {
                    "sample": "node_local.gvstats_rec.n_order",
                    "abs": false,
                    "delta": false,
                    "min": false,
                    "max": false,
                    "avg": false
                }, {"sample": "node_local.gvstats_rec.n_lock_success", "abs": true, "delta": true, "min": false, "max": false, "avg": false}, {"sample": "node_local.gvstats_rec.n_lock_fail", "abs": true, "delta": true, "min": false, "max": false, "avg": false}],
                "highlights": [{"sample": "node_local.gvstats_rec.n_set", "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]}, {
                    "sample": "node_local.gvstats_rec.n_kill",
                    "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]
                }, {"sample": "node_local.gvstats_rec.n_get", "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]}, {
                    "sample": "node_local.gvstats_rec.n_order",
                    "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]
                }, {"sample": "node_local.gvstats_rec.n_lock_success", "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]}, {
                    "sample": "node_local.gvstats_rec.n_lock_fail",
                    "data": [{"sampleType": "abs", "type": "normal"}, {"sampleType": "delta", "type": "normal"}, {"sampleType": "min", "type": "normal"}, {"sampleType": "max", "type": "normal"}, {"sampleType": "avg", "type": "normal"}]
                }],
                "options": {"orientation": "h"},
                "defaults": {
                    "header": {"border": {"color": "gray", "thickness": "1"}},
                    "region": {"background": "#5b3333", "border": {"color": "lightgray", "thickness": "0"}, "font": {"size": 24, "color": "#ff7f27", "fontFamily": "Inconsolata", "weight": "bold", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "center", "text": "LOCKS"},
                    "process": {"background": "#000000", "border": {"color": "silver", "thickness": "1"}, "font": {"size": 15, "color": "#eab83b", "fontFamily": "Inconsolata", "weight": "normal", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "left", "show": false},
                    "sample": {"background": "#000000", "border": {"color": "silver", "thickness": "1"}, "font": {"size": 17, "color": "#ff7f27", "fontFamily": "Inconsolata", "weight": "bold", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "center"},
                    "sampleMap": {"background": "#000000", "border": {"color": "silver", "thickness": "1"}, "font": {"size": 17, "color": "#ffffff", "fontFamily": "Inconsolata", "weight": "normal", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "wavy", "thickness": 1}}, "align": "center"},
                    "value": {"background": "#000000", "border": {"color": "gray", "thickness": "1"}, "font": {"size": 15, "color": "#64a555", "fontFamily": "Inconsolata", "weight": "bold", "style": "normal", "decoration": {"type": "normal", "color": "red", "style": "dotted", "thickness": 2}}, "align": "center", "formatting": {"thousands": true}},
                    "noValue": {"background": "#000000", "border": {"color": "gray", "thickness": "1"}},
                    "highlighters": {"low": "#64a555", "mid": "#eab83b", "high": "#ce3a3a", "topProcess": "#64a555"}
                },
                "renderIx": 3,
                "region": "DEFAULT",
                "processes": "Aggregate",
                "infoSet": false
            }]
        }, "id": "ba2a7763-ea7d-4c97-83d1-60b6dd4501f4", "ix": 0
    }], "sparkChart": {"graphsConfig": {"orientation": "v", "height": 300}, "theme": "dark"}
}]
