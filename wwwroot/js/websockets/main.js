/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the web sockets initialization, termination and remote servers start and stop.
// It additionally routes incoming data to the proper handlers


// Note: before to call the init() you need to initialize the messages modules ( like app.statistics.init() )
// to register the events
// Example: app.statistics.init()

app.websockets = {
    init: (host, port, withPromise = false) => {
        return new Promise(function (resolve, reject) {
            try {
                this.connected = false
                const protocol = window.location.protocol === 'https:' ? 'wss://' : 'ws://'

                console.log('Connecting sockets using: ' + protocol + host + ':' + app.websockets.wsPort)

                app.websockets.socket = this.socket = new WebSocket(protocol + host + ':' + app.websockets.wsPort);

                setTimeout(() => {
                    if (this.socket.readyState === 3) {

                        console.log('Web sockets: Couldn\'t connect to server')
                        if (withPromise === true) reject('Couldn\'t connect to server')

                        return
                    }

                    // Listen for messages
                    this.socket.addEventListener('message', function (event) {
                        const msg = JSON.parse(event.data)

                        // exception for keep-alive
                        if (msg.opCode === 'keepAlive') {
                            app.statistics.keepAlive(msg)

                            return
                        }

                        // exception for streaming data
                        if (msg.opCode === 'timer') {
                            app.statistics.statsData(msg)

                            return
                        }

                        // loop through the registered opCodes / events
                        app.websockets.opCodes.forEach(event => {
                            if (msg.opCode === event) {
                                this.dispatchEvent(new CustomEvent(event, {detail: msg}))
                            }
                        })
                    })

                    // event handler: when connection is open
                    this.socket.addEventListener('open', () => {
                        console.log('Connected to server...')
                    })

                    // event handler: when connection closes
                    this.socket.addEventListener('close', function (event) {
                        if (app.websockets.terminating === false) {
                            app.ui.msgbox.show('Got unexpectedly disconnected from the Web sockets server.', 'ERROR')

                            app.ui.tabs.close('tab-S')

                            app.websockets.connected = false

                            app.ui.stats.resetTab()
                        }
                    })

                    // socket initialized, we can resolve the Promise
                    app.websockets.connected = this.connected = true

                    console.log('Socket client has been initialized')

                    if (withPromise === true) resolve(this.socket)
                }, 300)

            } catch (err) {
                console.log('Error connecting to websocket server: ' + err.message)

                reject(err)
            }
        })
    },

    terminate: () => {
        return new Promise(function (resolve, reject) {
            try {
                console.log('Closing web sockets...')
                this.socket.close();

                this.connected = false

                resolve()

            } catch (err) {
                console.log('An error occurred while closing the websocket connection: ' + err.message)

                reject(err)
            }
        })
    },

    startServer: async function () {
        try {
            const res = await app.REST._wsStart()

            // Error, fatal
            if (res.result === 'ERROR') {
                app.ui.msgbox.show(res.error.description, 'ERROR')

                return false
            }
            // Warning, already running. Are we in the same browser session ?
            else if (res.result === 'WARNING') {
                return app.websockets.serverPid !== 0 ? true : -1
            }

            if (res.data && res.data.PID) app.websockets.serverPid = res.data.PID
            if (res.data && res.data.wsPort) app.websockets.wsPort = res.data.wsPort

            return true

        } catch (err) {
            app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')

            return false
        }
    },

    mping: function () {
        app.statistics.exec.mping()
    },

    stopServer: async function () {
        try {
            const res = await app.REST._terminateProcess(this.serverPid)

        } catch (err) {
            console.log('An error occurred while closing the websocket connection: ' + err.message)

        }
    },

    socket: {},
    connected: false,
    opCodes: [],
    serverPid: 0,
    terminating: false,
    wsPort: 0,
};

