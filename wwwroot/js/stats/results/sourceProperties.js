/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

app.ui.stats.results.sourceProperties = {
    init: function () {
        $('#btnStatsResultsSourcePropertiesHelp').on('click', () => this.helpClicked())

        app.ui.setupDialogForTest('modalStatsResultSourceProperties')
    },

    show: function () {
        const source = app.ui.stats.results.tabsData[app.ui.stats.results.tabIx].source

        $('#txtStatsResultsSourcePropertiesType').text(source.type.toUpperCase())
        $('#txtStatsResultsSourcePropertiesSampleRate').text(source.sampleRate + ' sec' + (parseInt(source.sampleRate) > 1 ? 's' : ''))

        $('#txtStatsResultsSourcePropertiesRegions').html(source.data.regions.join('<br>'))

        if (source.type === 'ygbl') {

            let processesText
            switch (source.data.processes.type) {
                case 'agg': {
                    processesText = 'All processes'

                    break
                }
                case 'pids': {
                    processesText = 'PIDs: ' + source.data.processes.pids.join(', ')

                    break
                }
                case 'top': {
                    processesText = 'TOP ' + source.data.processes.count + ' of ' + source.data.processes.sampleCount

                    break
                }
            }
            $('#txtStatsResultsSourcePropertiesProcesses').text(processesText)
        }

        $('#divStatsResultsSourcePropertiesRegions').css('display', 'flex')
        $('#divStatsResultsSourcePropertiesProcesses').css('display', source.type === 'ygbl' ? 'flex' : 'none')
        $('#hrStatsResultsSourcePropertiesYgbl').css('display', source.type === 'ygbl' ? 'block' : 'none')

        $('#txtStatsResultsSourcePropertiesSamples').text(source.data.samples.join(', '))

        $('#modalStatsResultSourceProperties')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    helpClicked: function () {
        app.ui.help.show('stats/source-props')
    },


}
