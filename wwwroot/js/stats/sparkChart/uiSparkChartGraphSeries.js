/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the sparkChart Graph individual series settings

app.ui.stats.sparkChart.graph.series = {
    init: function () {
        $('#btnStatsSparkChartGraphSeriesLineSettings').on('click', () => this.lineSettingsPressed())
        $('#btnStatsSparkChartGraphSeriesYaxisSettings').on('click', () => this.yAxisSettingsPressed())
        $('#btnStatsSparkChartGraphSeriesAreaSettings').on('click', () => this.areaSettingsPressed())

        $('#optStatsSparkChartGraphSeriesLine').on('click', () => this.linePressed())
        $('#optStatsSparkChartGraphSeriesArea').on('click', () => this.areaPressed())

        $('#btnStatsSparkChartGraphSeriesOk').on('click', () => this.okPressed())
        $('#btnStatsSparkChartGraphSeriesHelp').on('click', () => app.ui.help.show('stats/spark-chart/graph-series'))

        app.ui.setupDialogForTest('modalStatsSparkChartGraphSeries')
    },

    show: function (ix) {
        this.ix = ix
        const mapping = this.mapping = JSON.parse(JSON.stringify(app.ui.stats.sparkChart.graph.view.graphMapping[ix]))

        $('#hideme').css('display', 'none')

        if (mapping.settings.default === 'line') {
            $('#optStatsSparkChartGraphSeriesLine').prop('checked', true)

        } else {
            $('#optStatsSparkChartGraphSeriesArea').prop('checked', true)
        }

        this.graphSettings.data.datasets[0].label = app.ui.stats.utils.graph.convertSeriesName(mapping.type, app.ui.stats.sparkChart.graph.source)
        $('#lblStatsSparkChartGraphSeriesTitle').text('Series properties: ' + this.graphSettings.data.datasets[0].label)

        this.graphSettings.data.datasets[0].data = app.ui.stats.sparkChart.graph.graph.series.createDummySeries(new Date())

        if (this.chart !== null) this.chart.destroy()

        this.chart = new Chart(document.getElementById("graphStatsSparkChartGraphSeries"), this.graphSettings);

        this.chartUpdate()

        app.ui.button.set($('#btnStatsSparkChartGraphSeriesYaxisSettings'), app.ui.stats.sparkChart.graph.view.defaults.yAxis.common === true ? 'disabled' : 'enabled')

        $('#modalStatsSparkChartGraphSeries')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    chartUpdate: function () {
        // update series type
        //this.graph.options.series[0].type = this.mapping.settings.default

        const series = this.graphSettings.data.datasets[0]
        const newSeries = {
            borderColor: this.mapping.settings[this.mapping.settings.default].stroke.color,
            backgroundColor: this.mapping.settings[this.mapping.settings.default].stroke.color,
            borderWidth: this.mapping.settings[this.mapping.settings.default].stroke.width,
            tension: this.mapping.settings[this.mapping.settings.default].stroke.type === 'smooth' ? this.mapping.settings[this.mapping.settings.default].stroke.smoothTension : 0,
            stepped: this.mapping.settings[this.mapping.settings.default].stroke.type === 'stepline',
            borderDash: [this.mapping.settings[this.mapping.settings.default].stroke.dashArray, this.mapping.settings[this.mapping.settings.default].stroke.dashArray],
            pointRadius: this.mapping.settings[this.mapping.settings.default].marking.type === 'markers' ? this.mapping.settings[this.mapping.settings.default].marking.markers.size : 0,
            pointBackgroundColor: this.mapping.settings[this.mapping.settings.default].marking.type === 'markers' ? this.mapping.settings[this.mapping.settings.default].marking.markers.color : this.mapping.settings[this.mapping.settings.default].stroke.color,
            pointBorderColor: this.mapping.settings[this.mapping.settings.default].marking.type === 'markers' ? this.mapping.settings[this.mapping.settings.default].marking.markers.color : this.mapping.settings[this.mapping.settings.default].stroke.color,
            pointStyle: this.mapping.settings[this.mapping.settings.default].marking.markers.shape,
            yAxisID: 'y'
        }

        Object.assign(series, newSeries)


        const y = this.graphSettings.options.scales.y = {}

        y.display = this.mapping.settings.yAxis.show === true ? 'auto' : false
        y.type = this.mapping.settings.yAxis.type
        y.min = this.mapping.settings.yAxis.useManualSetting === true ? this.mapping.settings.yAxis.min : undefined
        y.max = this.mapping.settings.yAxis.useManualSetting === true ? this.mapping.settings.yAxis.max : undefined
        y.position = 'left'
        y.title = {
            display: true,
            text: this.mapping.settings.yAxis.title.text, //this.graphSettings.data.datasets[0].label,
            color: this.graphSettings.data.datasets[0].borderColor,
            align: this.mapping.settings.yAxis.title.align,
            font: {
                size: this.mapping.settings.yAxis.title.style.fontSize,
                family: this.mapping.settings.yAxis.title.style.fontFamily,
                weight: this.mapping.settings.yAxis.title.style.fontWeight,
            }
        }
        y.ticks = {
            textStrokeColor: this.graphSettings.data.datasets[0].borderColor,
            textStrokeWidth: 1,
            color: this.graphSettings.data.datasets[0].borderColor,
            display: this.mapping.settings.yAxis.labels.show,
            font: {
                size: this.mapping.settings.yAxis.labels.style.fontSize,
                family: this.mapping.settings.yAxis.labels.style.fontFamily,
                weight: this.mapping.settings.yAxis.labels.style.fontWeight,
            }
        }
        y.border = {
            color: this.mapping.settings.yAxis.axisBorder.show === true ? this.graphSettings.data.datasets[0].borderColor : undefined
        }


        this.chart.update()
    },

    linePressed: function () {
        $('#optStatsSparkChartGraphSeriesLine').prop('checked', true)
        this.mapping.settings.default = 'line'

        this.chartUpdate()
    },

    areaPressed: function () {
        $('#optStatsSparkChartGraphSeriesArea').prop('checked', true)
        this.mapping.settings.default = 'area'

        this.chartUpdate()
    },

    lineSettingsPressed: function () {
        this.linePressed()

        app.ui.prefs.show({
            title: 'Series line settings',
            manifest: app.ui.stats.manifest.graph.series.line,
            target: this.mapping.settings.line,
            name: 'statsSparkChartGraphSeriesLine'
        })

        this.chartUpdate()
    },

    yAxisSettingsPressed: function () {
        this.linePressed()

        app.ui.prefs.show({
            title: 'Y axis settings',
            manifest: app.ui.stats.manifest.graph.chart.yAxisSeries,
            target: this.mapping.settings.yAxis,
            name: 'statsSparkChartGraphSeriesYaxis'
        })

        this.chartUpdate()
    },

    areaSettingsPressed: function () {
        this.areaPressed()

        app.ui.prefs.show({
            title: 'Series area settings',
            manifest: app.ui.stats.manifest.graph.series.area,
            target: this.mapping.settings.area,
            name: 'statsSparkChartGraphSeriesArea'
        })

        this.chartUpdate()
    },

    okPressed: function () {
        app.ui.stats.sparkChart.graph.view.graphMapping[this.ix] = this.mapping

        app.ui.stats.sparkChart.graph.graph.update()

        $('#modalStatsSparkChartGraphSeries').modal('hide')
    },

    graphSettings: {
        type: 'line',
        responsive: true,
        maintainAspectRatio: false,
        data: {
            datasets: [
                {
                    label: '',
                    data: []
                }
            ]
        },
        options: {
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'seconds',
                        displayFormats: {
                            seconds: 'HH:mm:ss'
                        }
                    },
                    ticks: {
                        color: '',
                        /*
                        font: {
                            size: defaults.xaxis.labels.fontSize,
                            weight: defaults.xaxis.labels.fontWeight,
                            family: defaults.xaxis.labels.fontFamily,
                        },

                         */
                    },
                    /*
                    grid: {
                        color: defaults.grid.column.color,
                        display: defaults.grid.xAxisLines,
                        drawTicks: true,
                    }
                     */
                }
            }
        }
    },
    chart: null,
    mapping: {},
    ix: -1
}
