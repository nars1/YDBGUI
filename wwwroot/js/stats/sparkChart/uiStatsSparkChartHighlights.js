/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// This file manages the spark chart report line highlighters

app.ui.stats.sparkChart.highlighters = {
    init: function () {
        $('#btnStatsSparkChartHighlightsOk').on('click', () => this.okPressed())
        $('#btnStatsSparkChartHighlightsHelp').on('click', () => app.ui.help.show('stats/spark-chart/highlights'))

        $('#inpStatsSparkChartHighlightsRangeRed').on('change', () => this.rangeChanged('red'))
        $('#inpStatsSparkChartHighlightsRangeYellow').on('change', () => this.rangeChanged('yellow'))

        $('#inpStatsSparkChartHighlightsRangeRed').on('focus', () => this.rangeGotFocus())
        $('#inpStatsSparkChartHighlightsRangeYellow').on('focus', () => this.rangeGotFocus())

        app.ui.setupDialogForTest('modalStatsSparkChartHighlights')
    },

    show: function (source, view, highlight) {
        this.highlight = highlight
        this.view = view

        // load highlight values
        switch (highlight.type) {
            case 'normal': {
                $('#optStatsSparkChartHighlightsRegular').prop('checked', 'checked')

                break
            }
            case 'range': {
                $('#optStatsSparkChartHighlightsRange').prop('checked', 'checked')

                this.loadRangeValues(highlight.range.red, highlight.range.yellow)

                break
            }
            case 'max': {
                $('#optStatsSparkChartHighlightsMax').prop('checked', 'checked')

                break
            }
        }

        // enable / disable the third option
        $('#optStatsSparkChartHighlightsMax').attr('disabled', source.data.processes.type === 'agg')

        // apply correct range colors
        $('#spanStatsSparkChartHighlightsHigh').css('background', view.defaults.highlighters.high)
        $('#spanStatsSparkChartHighlightsMid').css('background', view.defaults.highlighters.mid)
        $('#spanStatsSparkChartHighlightsLow').css('background', view.defaults.highlighters.low)

        // display dialog
        $('#modalStatsSparkChartHighlights')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    rangeChanged: function (type) {
        const inpStatsSparkChartHighlightsRangeRed = $('#inpStatsSparkChartHighlightsRangeRed')
        const inpStatsSparkChartHighlightsRangeYellow = $('#inpStatsSparkChartHighlightsRangeYellow')
        const lblStatsSparkChartHighlightsRangeGreen = $('#lblStatsSparkChartHighlightsRangeGreen')

        switch (type) {
            case 'red': {
                inpStatsSparkChartHighlightsRangeYellow.attr('max', parseInt(inpStatsSparkChartHighlightsRangeRed.val()) - 1)

                break
            }
            case 'yellow': {
                inpStatsSparkChartHighlightsRangeRed.attr('min', parseInt(inpStatsSparkChartHighlightsRangeYellow.val()) + 1)
                lblStatsSparkChartHighlightsRangeGreen.text(app.ui.formatThousands(inpStatsSparkChartHighlightsRangeYellow.val()))
            }
        }
    },

    rangeGotFocus: function () {
        $('#optStatsSparkChartHighlightsRange').prop('checked', 'checked')
    },

    loadRangeValues: function (red, yellow) {
        const inpStatsSparkChartHighlightsRangeRed = $('#inpStatsSparkChartHighlightsRangeRed')
        const inpStatsSparkChartHighlightsRangeYellow = $('#inpStatsSparkChartHighlightsRangeYellow')
        const lblStatsSparkChartHighlightsRangeGreen = $('#lblStatsSparkChartHighlightsRangeGreen')

        inpStatsSparkChartHighlightsRangeRed.val(red)
        inpStatsSparkChartHighlightsRangeYellow.val(yellow)
        lblStatsSparkChartHighlightsRangeGreen.text(app.ui.formatThousands(yellow))
        inpStatsSparkChartHighlightsRangeRed.attr('min', parseInt(inpStatsSparkChartHighlightsRangeYellow.val()) + 1)
        inpStatsSparkChartHighlightsRangeYellow.attr('max', parseInt(inpStatsSparkChartHighlightsRangeRed.val()) - 1)
    },

    okPressed: function () {
        this.highlight.type = $('input[name=optStatsSparkChartHighlights]:checked').val()

        if (this.highlight.type === 'range') {
            const inpStatsSparkChartHighlightsRangeRed = parseInt($('#inpStatsSparkChartHighlightsRangeRed').val())
            const inpStatsSparkChartHighlightsRangeYellow = parseInt($('#inpStatsSparkChartHighlightsRangeYellow').val())

            if (isNaN(parseInt(inpStatsSparkChartHighlightsRangeRed))) {
                app.ui.msgbox.show('You must enter the first Greater than value', app.ui.msgbox.INPUT_ERROR)

                return
            }

            if (isNaN(parseInt(inpStatsSparkChartHighlightsRangeYellow))) {
                app.ui.msgbox.show('You must enter the second Greater than value', app.ui.msgbox.INPUT_ERROR)

                return
            }

            if (inpStatsSparkChartHighlightsRangeYellow > inpStatsSparkChartHighlightsRangeRed) {
                app.ui.msgbox.show('The second Greater than value is greater than the first one.', app.ui.msgbox.INPUT_ERROR)

                return
            }

            this.highlight.range = {
                red: inpStatsSparkChartHighlightsRangeRed,
                yellow: inpStatsSparkChartHighlightsRangeYellow
            }
        }

        // refresh view
        app.ui.stats.sparkChart.reportLine.preview()

        $('#modalStatsSparkChartHighlights').modal('hide')
    },

    view: [],
    highlight: {},
    rangeDefaults: {
        red: 50,
        yellow: 25
    }
}
