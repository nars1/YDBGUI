/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// This file contains the code to manage the creation / edit of YGBLSTATS entry

app.ui.stats.sources.ygblstats = {
    init: function () {
        $('#optStatsYgblstatsSelectorSamples').on('change', () => this.samplesChange())
        $('#inpStatsYgblstatsSelectorRateSecs').on('change', () => this.sampleRateUpdated())

        $('#statsYgblstatsSelectorProcessesAgg').on('click', () => this.processesUpdated('agg'))
        $('#statsYgblstatsSelectorProcessesTop').on('click', () => this.processesUpdated('top'))
        $('#statsYgblstatsSelectorProcessesPids').on('click', () => this.processesUpdated('pids'))

        $('#btnStatsYgblstatsSelectorOk').on('click', () => this.okPressed())
        $('#btnStatsYgblstatsSelectorHelp').on('click', () => app.ui.help.show('stats/ygblstats-manage'))

        $('#btnStatsYgblRefreshPids').on('click', () => this.refreshPids())

        app.ui.setupDialogForTest('modalstatsYgblstatsSelector')
    },

    show: async function (editMode = false) {
        const optModalstatsYgblstatsSelectorSamples = $('#optStatsYgblstatsSelectorSamples')
        const optStatsYgblstatsSelectorRegions = $('#optStatsYgblstatsSelectorRegions')
        let options = ''

        // *************************
        // populate the dialog controls
        // *************************

        // populate the <select> with the samples data
        optModalstatsYgblstatsSelectorSamples.empty()

        this.samples.forEach(group => {
            if (group.children.find(child => child.ygbl === true)) {
                options += '<optgroup label="' + group.name + '">'

                group.children.forEach(sample => {
                    if (sample.ygbl === true) {
                        options += '<option style="color: green;" value="' + sample.value + '" title="' + sample.caption + '">'
                        options += sample.value + ':&nbsp;' + (sample.value.length === 4 ? '' : '&nbsp;') + sample.caption
                        options += '</option>'
                    }
                })

                options += '</optgroup>'
            }
        })

        optModalstatsYgblstatsSelectorSamples.append(options)

        // regions list
        optStatsYgblstatsSelectorRegions.empty()
        options = '<option value="*">*</option>'

        for (const region in app.system.regions) {
            options += '<option value="' + region + '">' + region + '</option>'
        }

        optStatsYgblstatsSelectorRegions.append(options)

        // *************************
        // set defaults
        // *************************
        if (editMode === false) {
            this.editModeIx = -1

            $('#optStatsYgblstatsSelectorRegions [value="*"]').attr('selected', 'selected')

            $('#modalstatsYgblstatsSelectorTitle').text('YGBLSTAT: Adding entry')

            // create new
            $('#inpStatsYgblstatsSelectorRateSecs').val(1)

            $('#numStatsYgblstatsSelectorProcessesTop').val('')
            $('#optStatsYgblstatsSelectorTopSample')
                .empty()
                .val('')

            this.processesUpdated('agg')
            $('#statsYgblstatsSelectorProcessesAgg').prop('checked', true)

            await this.refreshPids()

        } else {
            // edit mode
            const source = app.ui.stats.sources.entry.data[editMode]
            this.editModeIx = editMode

            $('#modalstatsYgblstatsSelectorTitle').text('YGBLSTAT: Editing entry')

            // samples
            source.data.samples.forEach(sample => {
                $('#optStatsYgblstatsSelectorSamples option[value="' + sample + '"]').attr('selected', 'selected')
            })
            this.samplesChange()

            // regions
            source.data.regions.forEach(region => {
                $('#optStatsYgblstatsSelectorRegions option[value="' + region + '"]').attr('selected', 'selected')
            })

            await this.refreshPids()

            // processes
            switch (source.data.processes.type) {
                case 'agg':
                    $('#statsYgblstatsSelectorProcessesAgg').prop('checked', true);
                    break
                case 'top':
                    $('#statsYgblstatsSelectorProcessesTop').prop('checked', true);
                    break
                case 'pids':
                    $('#statsYgblstatsSelectorProcessesPids').prop('checked', true);
                    break
            }
            this.processesUpdated(source.data.processes.type)

            if (source.data.processes.type === 'top') {
                $('#numStatsYgblstatsSelectorProcessesTop').val(source.data.processes.count)
                $('#optStatsYgblstatsSelectorTopSample').val(source.data.processes.sampleCount)

            } else if (source.data.processes.type === 'pids') {
                source.data.processes.pids.forEach(pid => {
                    $('#selStatsYgblstatsSelectorPids option[value="' + pid + '"]').attr('selected', 'selected')
                })

            } else {
                $('#numStatsYgblstatsSelectorProcessesTop').val('')
                $('#optStatsYgblstatsSelectorTopSample').val('')
            }

            // sample rate
            $('#inpStatsYgblstatsSelectorRateSecs').val(source.sampleRate)
        }

        this.sampleRateChanged = false

        // display the dialog
        $('#modalstatsYgblstatsSelector')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    processesUpdated: function (type) {
        const divStatsYgblProcessesAll = $('#divStatsYgblProcessesAll')
        const divStatsYgblProcessesTop = $('#divStatsYgblProcessesTop')
        const divStatsYgblProcessesPids = $('#divStatsYgblProcessesPids')

        switch (type) {
            case 'agg': {
                divStatsYgblProcessesAll.css('display', 'block')
                divStatsYgblProcessesTop.css('display', 'none')
                divStatsYgblProcessesPids.css('display', 'none')

                break
            }
            case 'top': {
                divStatsYgblProcessesAll.css('display', 'none')
                divStatsYgblProcessesTop.css('display', 'block')
                divStatsYgblProcessesPids.css('display', 'none')

                break
            }
            case 'pids': {
                divStatsYgblProcessesAll.css('display', 'none')
                divStatsYgblProcessesTop.css('display', 'none')
                divStatsYgblProcessesPids.css('display', 'block')

                break
            }
        }
    },

    refreshPids: async function () {
        const selStatsYgblstatsSelectorPids = $('#selStatsYgblstatsSelectorPids')

        selStatsYgblstatsSelectorPids.empty()

        try {
            const regions = $('#optStatsYgblstatsSelectorRegions').val()
            const res = await app.REST._ygblEnumPids(regions)

            if (res.data) {
                res.data.processes.forEach(process => {
                    const option = '<option value="' + process + '">' + process + '</option>'

                    selStatsYgblstatsSelectorPids.append(option)
                })

            } else {
                const option = '<option value="" disabled style="font-style: italic; color: silver;">No processes found...</option>'

                selStatsYgblstatsSelectorPids.append(option)
            }
        } catch (err) {
            console.log(err)
        }
    },

    sampleRateUpdated: function () {
        this.sampleRateChanged = true
    },

    samplesChange: function () {
        const selection = $('#optStatsYgblstatsSelectorSamples').val()
        const optStatsYgblstatsSelectorTopSample = $('#optStatsYgblstatsSelectorTopSample')
        let options = '<option value=""></option>'

        optStatsYgblstatsSelectorTopSample.empty()

        selection.forEach(sample => {
            options += '<option value="' + sample + '">' + sample + '</option>'
        })

        optStatsYgblstatsSelectorTopSample.append(options)
    },

    okPressed: function () {
        const optStatsYgblstatsSelectorTopSample = $('#optStatsYgblstatsSelectorTopSample').val()
        const numStatsYgblstatsSelectorProcessesTop = parseInt($('#numStatsYgblstatsSelectorProcessesTop').val())
        const selStatsYgblstatsSelectorPids = $('#selStatsYgblstatsSelectorPids').val()

        const inpStatsYgblstatsSelectorRateSecs = parseFloat($('#inpStatsYgblstatsSelectorRateSecs').val())

        // processes top
        if ($("input[name='nameStatsYgblstatsSelectorProcesses']:checked").val() === 'top') {
            // check for valid # of processes and sample name
            if (isNaN(numStatsYgblstatsSelectorProcessesTop) || numStatsYgblstatsSelectorProcessesTop < 1) {
                app.ui.msgbox.show('You must enter a valid number of processes.', app.ui.msgbox.INPUT_ERROR)

                return
            }

            if (optStatsYgblstatsSelectorTopSample === null || optStatsYgblstatsSelectorTopSample === '') {
                app.ui.msgbox.show('You must select the sample to be used in the Top.', app.ui.msgbox.INPUT_ERROR)

                return
            }
        }

        // processes pids
        if ($("input[name='nameStatsYgblstatsSelectorProcesses']:checked").val() === 'pids') {
            if (selStatsYgblstatsSelectorPids === null || selStatsYgblstatsSelectorPids === '' || (Array.isArray(selStatsYgblstatsSelectorPids) && selStatsYgblstatsSelectorPids.length === 0)) {
                app.ui.msgbox.show('You must select at least one process.', app.ui.msgbox.INPUT_ERROR)

                return
            }
        }

        // samples
        if ($('#optStatsYgblstatsSelectorSamples').val().length === 0) {
            app.ui.msgbox.show('You must select at least one sample.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        // region
        if ($('#optStatsYgblstatsSelectorRegions').val().length === 0) {
            app.ui.msgbox.show('You must select at least one region.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        // sample rate
        if (isNaN(inpStatsYgblstatsSelectorRateSecs)) {
            app.ui.msgbox.show('You must enter the sample rate in seconds.', app.ui.msgbox.INPUT_ERROR)

            return
        }
        if (inpStatsYgblstatsSelectorRateSecs === 0) {
            app.ui.msgbox.show('You must enter a sample rate greater than 0.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        const rate = inpStatsYgblstatsSelectorRateSecs.toString().split('.')
        if (rate[1] && rate[1].length > 1) {
            app.ui.msgbox.show('The decimal part of the sample rate must be 1 digit only, as the step is 100 milliseconds.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        // adjust the regions array if needed (* + more regions selected = * only)
        const selRegions = $('#optStatsYgblstatsSelectorRegions').val()
        if (selRegions[0] === '*' && selRegions.length > 1) selRegions.splice(1, selRegions.length - 1)

        let ygbl = {
            type: 'ygbl',
            sampleRate: parseFloat(inpStatsYgblstatsSelectorRateSecs),
            data: {
                enabled: true,
                samples: $('#optStatsYgblstatsSelectorSamples').val(),
                regions: selRegions,
                processes: {
                    type: $("input[name='nameStatsYgblstatsSelectorProcesses']:checked").val(),
                    count: isNaN(numStatsYgblstatsSelectorProcessesTop) ? null : numStatsYgblstatsSelectorProcessesTop,
                    sampleCount: optStatsYgblstatsSelectorTopSample,
                    pids: selStatsYgblstatsSelectorPids
                },
                views: [],
            },
        }

        $('#modalstatsYgblstatsSelector').modal('hide')

        if (this.editModeIx === -1) {
            ygbl.id = app.ui.GUID()
            app.ui.stats.sources.entry.data.push(ygbl)

            const lastIx = app.ui.stats.sources.entry.data.length - 1
            app.ui.stats.sources.entry.data[lastIx].ix = lastIx

            app.ui.stats.sources.refresh()

        } else {
            const source = app.ui.stats.sources.entry.data[this.editModeIx]
            const oldYgbl = app.ui.stats.sources.entry.data[this.editModeIx]

            // determine what has changed
            const samplesChanged = oldYgbl.data.samples.join(' ') !== ygbl.data.samples.join(' ')
            const regionsChanged = oldYgbl.data.regions.join(' ') !== ygbl.data.regions.join(' ')
            const processTypeChanged = oldYgbl.data.processes.type !== ygbl.data.processes.type
            const processSampleCountChanged = oldYgbl.data.processes.sampleCount !== ygbl.data.processes.sampleCount
            const processCountChanged = oldYgbl.data.processes.count !== ygbl.data.processes.count
            const processPidsChanged = oldYgbl.data.processes.pids && oldYgbl.data.processes.pids.join(' ') !== ygbl.data.processes.pids.join(' ')

            // sample rate only ?
            if (this.sampleRateChanged === true &&
                samplesChanged === false &&
                processSampleCountChanged === false &&
                processTypeChanged === false &&
                regionsChanged === false &&
                processCountChanged === false &&
                processPidsChanged === false) {

                app.ui.inputbox.show('Update the source?', '', res => {
                    if (res === 'YES') {
                        ygbl.id = source.id
                        app.ui.stats.sources.entry.data[this.editModeIx].sampleRate = ygbl.sampleRate

                        app.ui.stats.sources.refresh()
                    }
                })

                return
            }

            if (source.data.views.length > 0) {

                if (regionsChanged === true || processTypeChanged === true || processSampleCountChanged === true || processCountChanged === true) {
                    let message = ''

                    if (regionsChanged === true) {
                        message += 'The regions are changed.<br>'
                    }

                    if (processTypeChanged === true) {
                        message += 'The processes type has changed.<br>'
                    }

                    if (processSampleCountChanged === true) {
                        message += 'The TOP processes sample count has changed.<br>'
                    }

                    if (processCountChanged === true) {
                        message += 'The TOP processes count has changed.<br>'
                    }

                    if (samplesChanged === true && processPidsChanged === true) {
                        message += 'The processes PIDs are changed along with the samples.<br>'
                    }

                    message += 'This will require a deletion of the existing views.<br><br>Do you want to proceed?'

                    app.ui.inputbox.show(message, '', res => {
                        if (res === 'YES') {
                            ygbl.id = source.id
                            app.ui.stats.sources.entry.data[this.editModeIx] = ygbl

                            app.ui.stats.sparkChart.renderer.render()

                            app.ui.stats.sources.refresh()
                        }
                    })

                    return
                }

                const report = this.getReconInfo(ygbl, source)
                let message = ''

                if (report.text.length > 0) {
                    message = 'The following view(s) will be affected:<br>'
                    report.text.forEach(line => {
                        message += line + '<br>'
                    })
                }

                message += 'Update the source?'

                app.ui.inputbox.show(message, '', res => {
                    if (res === 'YES') {

                        // exec recon
                        this.execRecon(this.editModeIx, ygbl, source, report.indexes, processPidsChanged)

                        // refresh the screen to reflect changes
                        app.ui.stats.sparkChart.renderer.render()

                        // and refresh the sources screen, also to reflect the changes
                        app.ui.stats.sources.refresh()
                    }
                })

            } else {
                app.ui.inputbox.show('Update the source?', '', res => {
                    if (res === 'YES') {
                        ygbl.id = source.id
                        ygbl.data.views = []
                        app.ui.stats.sources.entry.data[this.editModeIx] = ygbl

                        app.ui.stats.sources.refresh()
                    }
                })
            }
        }
    },

    findSampleByMnemonic: function (mnemonic) {
        let retValue = undefined

        this.samples.forEach(group => {
            if (retValue !== undefined) return
            retValue = group.children.find(sample => mnemonic === sample.value)
        })

        return retValue
    },

    findSampleByFhead: function (fheadName) {
        let retValue = undefined

        this.samples.forEach(group => {
            if (retValue !== undefined) return
            retValue = group.children.find(sample => fheadName === sample.valueFhead)
        })

        return retValue
    },

    findSampleByPeekByName: function (peekByNameName) {
        let retValue = undefined

        this.samples.forEach(group => {
            if (retValue !== undefined) return
            retValue = group.children.find(sample => peekByNameName === sample.valuePeekByName)
        })

        return retValue
    },

    getReconInfo: function (ygbl, source) {
        const originalViews = source.data.views
        const report = {
            indexes: [],
            text: []
        }

        // return if no views
        if (originalViews.length === 0) return report

        // analyze each view and amend, as needed
        originalViews.forEach((view, viewIx) => {
            const removeIxs = []
            const graphRemoveIxs = []
            let addSamples = []
            let usedFlag = false

            view.mapping.forEach((map, mapIx) => {
                // check if sample exists in our new source and flag it for deletion
                if (!(ygbl.data.samples.find(sample => sample === map.sample))) {
                    if (this.isSampleUsed(map) === true) usedFlag = true

                    removeIxs.push({
                        mapIx: mapIx,
                        sample: map.sample,
                        used: this.isSampleUsed(map)
                    })
                }
            })
            addSamples = ygbl.data.samples.filter(sample => {
                return view.mapping.find(map => map.sample === sample) === undefined
            })

            // collect graphMapping indexes if needed
            if (view.type === 'graph') {
                view.graphMapping.forEach((map, mapIx) => {
                    // check if sample exists in our new source and flag it for deletion
                    if (!ygbl.data.samples.find(sample => sample === map.type.split('-')[1])) graphRemoveIxs.push({
                        mapIx: mapIx,
                        sample: map.type.split('-')[1]
                    })
                })
            }

            // push indexes in report
            if (removeIxs.length > 0 || addSamples.length > 0) {
                report.indexes[viewIx] = {
                    removeIxs: removeIxs,
                    graphRemoveIxs: graphRemoveIxs,
                    addSamples: addSamples
                }
            }

            // create display text
            let sampleList = []

            removeIxs.forEach(entry => {
                if (entry.used === true) sampleList.push(entry.sample)
            })

            if (usedFlag === true) {
                report.text.push(
                    'View: [' + viewIx + '] ' + sampleList.join(', ') + ' will be removed. '
                )
            }
        })

        return report
    },

    isSampleUsed: function (map) {
        return !(map.abs === false && map.delta === false && map.min === false && map.max === false && map.avg === false)
    },

    execRecon: function (editModeIx, ygbl, source, indexes, pidsChanged) {
        const originalViews = source.data.views || []

        // if no views, simply replace it
        if (originalViews.length === 0) {
            ygbl.id = source.id
            ygbl.data.views = []
            app.ui.stats.sources.entry.data[editModeIx] = ygbl
            app.ui.stats.sources.refresh()

            return
        }

        // analyze each view and amend, as needed
        originalViews.forEach((view, viewIx) => {
            // perform the deletion on mapping

            if (indexes[viewIx]) {
                const newMapping = []

                view.mapping.forEach((map, ix) => {
                    if (indexes[viewIx].removeIxs.find(removeIx => removeIx.mapIx === ix) === undefined) newMapping.push(view.mapping[ix])
                })
                view.mapping = newMapping
            }

            // perform the deletion on highlights
            if (view.type === 'reportLine' && indexes[viewIx]) {
                const newHighlights = []
                view.highlights.forEach((map, ix) => {
                    if (indexes[viewIx].removeIxs.find(removeIx => removeIx.mapIx === ix) === undefined) newHighlights.push(view.highlights[ix])
                })
                view.highlights = newHighlights
            }

            // perform the deletion on graphMapping
            if (view.type === 'graph' && indexes[viewIx]) {
                const newGraphMapping = []

                view.graphMapping.forEach((map, ix) => {
                    if (indexes[viewIx].graphRemoveIxs.find(removeIx => removeIx.mapIx === ix) === undefined) newGraphMapping.push(view.graphMapping[ix])
                })

                view.graphMapping = newGraphMapping
            }

            // if processes type = 'pids', update graph mapping
            if (pidsChanged === true && view.type === 'graph') {
                // delete and recreate graphMappings
                this.recreateGraphMappings(view, ygbl.data.processes.pids)
            }
        })

        // and add the new samples
        originalViews.forEach((view, viewIx) => {
            if (indexes[viewIx]) {
                indexes[viewIx].addSamples.forEach(sample => {
                    view.mapping.push({
                        sample: sample,
                        abs: false,
                        delta: false,
                        min: false,
                        max: false,
                        avg: false
                    })
                })
            }
        })

        source.sampleRate = ygbl.sampleRate
        source.data.enabled = ygbl.data.enabled
        source.data.samples = ygbl.data.samples
        source.data.regions = ygbl.data.regions
        source.data.processes = ygbl.data.processes
    },

    recreateGraphMappings: function (view, pids) {
        // clear the current mapping
        view.graphMapping = []

        // find which samples are set
        const selectedMaps = []
        view.mapping.forEach(map => {
            if (this.isSampleUsed(map) === true) {
                selectedMaps.push(map)
            }
        })

        // create new entries based on the new PIDs
        selectedMaps.forEach(map => {
            // find the sample types
            const sampleType = []
            if (map.abs === true) sampleType.push('abs')
            if (map.delta === true) sampleType.push('delta')
            if (map.min === true) sampleType.push('min')
            if (map.max === true) sampleType.push('max')
            if (map.avg === true) sampleType.push('avg')

            // create the entries for each sample type
            let mappingColorIx = 0

            pids.forEach((pid, pidIx) => {
                sampleType.forEach(sampleType => {
                    const graphMappingId = pidIx + '-' + map.sample + '-' + sampleType
                    const settings = JSON.parse(JSON.stringify(app.userSettings.stats.sparkLines.sections.graph.series))

                    // colors
                    settings.line.stroke.color = settings.area.stroke.color = app.userSettings.stats.sparkLines.sections.graph.series.colors[mappingColorIx]
                    mappingColorIx++
                    if (mappingColorIx === 12) mappingColorIx = 0
                    delete settings.colors

                    // y-axis
                    settings.yAxis = JSON.parse(JSON.stringify(app.userSettings.stats.sparkLines.sections.graph.chart.yAxis.defaultsSeries))

                    view.graphMapping.push({
                        type: graphMappingId,
                        settings: settings
                    })
                })
            })
        })
    },

    sampleRateChanged: false,
    othersChanged: false,
    editModeIx: -1,

    samples: [
        {
            name: 'Logical DB Operations',
            children: [
                {
                    value: 'SET',
                    valuePeekByName: 'node_local.gvstats_rec.n_set',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_set',
                    caption: '# of SET operations (TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'KIL',
                    valuePeekByName: 'node_local.gvstats_rec.n_kill',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_kill',
                    caption: '# of KILL operations (kill as well as zwithdraw, TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'GET',
                    valuePeekByName: 'node_local.gvstats_rec.n_get',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_get',
                    caption: '# of GET operations (TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'DTA',
                    caption: '# of DATA operations (TP and non-TP)',
                    ygbl: true,
                },
                {
                    value: 'ORD',
                    valuePeekByName: 'node_local.gvstats_rec.n_order',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_order',
                    caption: '# of $ORDer(,1) (forward) operations (TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'ZPR',
                    valuePeekByName: 'node_local.gvstats_rec.n_zprev',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_zprev',
                    caption: '# of $order(,-1) or $ZPRevious() (reverse order) operations (TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'QRY',
                    valuePeekByName: 'node_local.gvstats_rec.n_query',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_query',
                    caption: '# of $QueRY() operations (TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
            ]
        },
        {
            name: 'Users',
            peekByName: true,
            children: [
                {
                    value: 'USR',
                    valuePeekByName: 'node_local.ref_cnt',
                    caption: 'Number of processes accessing the database',
                    ygbl: false,
                    fhead: false,
                    peekByName: true,
                },
            ]
        },
        {
            name: 'M Locks Operations',
            children: [
                {
                    value: 'LKS',
                    valuePeekByName: 'node_local.gvstats_rec.n_lock_success',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_lock_success',
                    caption: '# of Lock calls (mapped to this db) that Succeeded',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'LKF',
                    valuePeekByName: 'node_local.gvstats_rec.n_lock_fail',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_lock_fail',
                    caption: '# of Lock calls (mapped to this db) that Failed',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
            ]
        },
        {
            name: 'Disk Operations',
            children: [
                {
                    value: 'DRD',
                    valuePeekByName: 'node_local.gvstats_rec.n_dsk_read',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_dsk_read',
                    caption: '# of Disk Reads from the database file (TP and non-TP, committed and rolled-back)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                    onlyBG: true,
                },
                {
                    value: 'DWT',
                    valuePeekByName: 'node_local.gvstats_rec.n_dsk_write',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_dsk_write',
                    caption: '# of Disk Writes to the database file (TP and non-TP, committed and rolled-back)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                    onlyBG: true,
                },
            ]
        },
        {
            name: 'Non-Transactional Operations',
            children: [
                {
                    value: 'NTW',
                    caption: '# of Non-tp committed Transactions that were read-Write',
                    ygbl: true,
                },
                {
                    value: 'NTR',
                    caption: '# of Non-tp committed Transactions that were Read-only',
                    ygbl: true,
                },
                {
                    value: 'NBR',
                    valuePeekByName: 'node_local.gvstats_rec.n_nontp_blkread',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_nontp_blkread',
                    caption: '# of Non-tp committed transaction induced Block Reads',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'NBW',
                    valuePeekByName: 'node_local.gvstats_rec.n_nontp_blkwrite',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_nontp_blkwrite',
                    caption: '# of Non-tp committed transaction induced Block Writes',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'NR0',
                    caption: '# of Non-tp transaction Restarts at try 0',
                    ygbl: true,
                },
                {
                    value: 'NR1',
                    caption: '# of Non-tp transaction Restarts at try 1',
                    ygbl: true,
                },
                {
                    value: 'NR2',
                    caption: '# of Non-tp transaction Restarts at try 2',
                    ygbl: true,
                },
                {
                    value: 'NR3',
                    caption: '# of Non-tp transaction Restarts at try 3',
                    ygbl: true,
                },
            ]
        },
        {
            name: 'Transactions',
            children: [
                {
                    value: 'CTN',
                    valuePeekByName: 'node_local.gvstats_rec.db_curr_tn',
                    valueFhead: 'sgmnt_data.gvstats_rec.db_curr_tn',
                    caption: 'Current Transaction Number of the database for the last committed read-write transaction (TP and non-TP)',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TBR',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_blkread',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_blkread',
                    caption: '# of Tp transaction induced Block Reads',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TRB',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_rolledback',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_rolledback',
                    caption: '# of Tp read-only or read-write transactions Rolled Back',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TBW',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_blkwrite',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_blkwrite',
                    caption: '# of Tp transaction induced Block Writes',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TTR',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_readonly',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_readonly',
                    caption: '# of Tp committed Transactions that were Read-onl',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TTW',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_readwrite',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_readwrite',
                    caption: '# of Tp committed Transactions that were read-Write',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TR0',
                    caption: '# of Tp transaction Restarts at try 0',
                    ygbl: true,
                },
                {
                    value: 'TR1',
                    caption: '# of Tp transaction Restarts at try 1',
                    ygbl: true,
                },
                {
                    value: 'TR2',
                    caption: '# of Tp transaction Restarts at try 2',
                    ygbl: true,
                },
                {
                    value: 'TR3',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_tot_retries_3',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_tot_retries_3',
                    caption: '# of Tp transaction Restarts at try 3',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TR4',
                    valuePeekByName: 'node_local.gvstats_rec.n_tp_tot_retries_4',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_tp_tot_retries_4',
                    caption: '# of Tp transaction Restarts at try 4 and above',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TC0',
                    caption: '# of Tp transaction Conflicts at try 0',
                    ygbl: true,
                },
                {
                    value: 'TC1',
                    caption: '# of Tp transaction Conflicts at try 1',
                    ygbl: true,
                },
                {
                    value: 'TC2',
                    caption: '# of Tp transaction Conflicts at try 2',
                    ygbl: true,
                },
                {
                    value: 'TC3',
                    caption: '# of Tp transaction Conflicts at try 3',
                    ygbl: true,
                },
                {
                    value: 'TC4',
                    caption: '# of Tp transaction Conflicts at try 4 and above',
                    ygbl: true,
                },
            ]
        },
        {
            name: 'Triggers',
            children: [
                {
                    value: 'ZTR',
                    valuePeekByName: 'node_local.gvstats_rec.n_ztrigger',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_ztrigger',
                    caption: '# of ZTRigger command operations',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
            ]
        },
        {
            name: 'Buffers flushing',
            children: [
                {
                    value: 'DFL',
                    valuePeekByName: 'node_local.gvstats_rec.n_db_flush',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_db_flush',
                    caption: '# of Database Flushes of the entire set of dirty global buffers in shared memory to disk',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                    onlyBG: true,
                },
            ]
        },
        {
            name: 'FSync',
            children: [
                {
                    value: 'DFS',
                    valuePeekByName: 'node_local.gvstats_rec.n_db_fsync',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_db_fsync',
                    caption: '# of times a process does an fsync of the database file.',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
            ]
        },
        {
            name: 'Journal',
            children: [
                {
                    value: 'JFL',
                    valuePeekByName: 'node_local.gvstats_rec.n_jnl_flush',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_jnl_flush',
                    caption: '# of Journal Flushes of all dirty journal buffers in shared memory to disk',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'JFS',
                    valuePeekByName: 'node_local.gvstats_rec.n_jnl_fsync',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_jnl_fsync',
                    caption: '# of Journal FSync operations on the journal file',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'JBB',
                    valuePeekByName: 'node_local.gvstats_rec.n_jbuff_bytes',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_jbuff_bytes',
                    caption: '# of Journal Buffer Bytes updated in shared memory',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'JFB',
                    caption: '# of Journal File Bytes written to the journal file on disk',
                    ygbl: true,
                },
                {
                    value: 'JFW',
                    valuePeekByName: 'node_local.gvstats_rec.n_jfile_writes',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_jfile_writes',
                    caption: '# of Journal File Write system calls',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'JRL',
                    caption: '# of Journal Records with a Logical record type (e.g. SET, KILL etc.) written to the journal file',
                    ygbl: true,
                },
                {
                    value: 'JRP',
                    caption: '# of Journal Records with a Physical record type',
                    ygbl: true,
                },
                {
                    value: 'JRE',
                    caption: '# of Journal Regular Epoch records written to the journal file',
                    ygbl: true,
                },
                {
                    value: 'JRI',
                    caption: '# of Journal Idle epoch journal records written to the journal file',
                    ygbl: true,
                },
                {
                    value: 'JRO',
                    caption: '# of Journal Records with a type Other than logical written to the journal file ',
                    ygbl: true,
                },
                {
                    value: 'JEX',
                    caption: '# of Journal file EXtentions',
                    ygbl: true,
                },
            ]
        },
        {
            name: 'DB extensions',
            children: [
                {
                    value: 'DEX',
                    valuePeekByName: 'node_local.gvstats_rec.n_db_extends',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_db_extends',
                    caption: '# of Database file EXtentions',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
            ]
        },
        {
            name: 'Critical section acquisition',
            children: [
                {
                    value: 'CAT',
                    valuePeekByName: 'node_local.gvstats_rec.n_crit_success',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_crit_success',
                    caption: 'Total Acquisitions successes',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'CFE',
                    valuePeekByName: 'node_local.gvstats_rec.n_crit_failed',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_crit_failed',
                    caption: 'Failed (blocked) acquisition total caused by Epochs.',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'CFS',
                    caption: 'Square of CFT',
                    ygbl: true,
                },
                {
                    value: 'CFT',
                    caption: 'Failed (blocked) acquisition Total',
                    ygbl: true,
                },
            ]
        },
        {
            name: 'Blocks',
            children: [
                {
                    value: 'BTD',
                    caption: '# of database Block Transitions to Dirty',
                    ygbl: true,
                },
                {
                    value: 'WFR',
                    valuePeekByName: 'node_local.gvstats_rec.n_wait_for_read',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_wait_for_read',
                    caption: '# of times a process slept while waiting for another process to read in a database block',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'BUS',
                    caption: '# of times db_csh_get could not determine whether a block was in cache or not',
                    ygbl: true,
                },
                {
                    value: 'BTS',
                    caption: '# of times a dirty buffer was flushed so a BT could be reused',
                    ygbl: true,
                },
            ]
        },
        {
            name: 'Waiters',
            children: [
                {
                    value: 'TRX',
                    caption: 'Waiting for transaction in progress',
                    ygbl: true,
                },
                {
                    value: 'ZAD',
                    caption: 'Waiting for region freeze off,',
                    ygbl: true,
                },
                {
                    value: 'JOPA',
                    caption: 'Waiting for journal open critical section',
                    ygbl: true,
                },
                {
                    value: 'AFRA',
                    valueFhead: 'sgmnt_data.freeze',
                    caption: 'Waiting for freeze critical section release',
                    ygbl: true,
                    fhead: true,
                    peekByName: false,
                },
                {
                    value: 'BREA',
                    caption: 'Waiting for block read & decryption',
                    ygbl: true,
                },
                {
                    value: 'MLBA',
                    caption: 'waiting for blocked LOCK',
                    ygbl: true,
                },
                {
                    value: 'MLK',
                    valuePeekByName: 'node_local.gvstats_rec.n_mlk_wait',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_mlk_wait',
                    caption: 'Waiting for LOCK access',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'TRGA',
                    caption: 'Waiting for mini-transaction completion',
                    ygbl: true,
                },
                {
                    value: 'PRC',
                    caption: 'Waiting on exit',
                    ygbl: true,
                },
                {
                    value: 'DEXA',
                    valuePeekByName: 'node_local.gvstats_rec.n_dbext_wait',
                    valueFhead: 'sgmnt_data.gvstats_rec.n_dbext_wait',
                    caption: 'Waiting for db extension',
                    ygbl: true,
                    fhead: true,
                    peekByName: true,
                },
                {
                    value: 'GLB',
                    caption: 'Waiting for BG access critical section',
                    ygbl: true,
                },
                {
                    value: 'JNL',
                    caption: 'Waiting for journal access critical section',
                    ygbl: true,
                },
            ]
        },
    ]
}
