/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// This file contains the code to manage the creation / edit of FHEAD / PEEKBYNAME entries

app.ui.stats.sources.fheadPeekByName = {
    init: function () {
        $('#inpStatsFheadPeekByNameSelectorRateSecs').on('change', () => this.sampleRateUpdated())
        $('#btnStatsFheadPeekByNameSelectorOk').on('click', () => this.okPressed())

        $('#btnStatsFheadPeekByNameSelectorHelp').on('click', () => this.helpPressed())

        app.ui.setupDialogForTest('modalStatsFheadPeekByNameSelector')
    },

    show: function (type, editMode = false) {
        // populate the dialog first
        const optStatsFheadPeekByNameSelectorSamples = $('#optStatsFheadPeekByNameSelectorSamples')
        const optStatsFheadPeekByNameSelectorRegions = $('#optStatsFheadPeekByNameSelectorRegions')
        const maxLength = 42
        let options = ''

        this.type = type

        optStatsFheadPeekByNameSelectorSamples.empty()

        app.ui.stats.sources.ygblstats.samples.forEach(group => {
            if (group.children.find(child => child[type] === true)) {
                options += '<optgroup label="' + group.name + '">'

                group.children.forEach(sample => {
                    if (sample[type] === true) {
                        options += '<option style="color: green;" value="' + (type === 'fhead' ? sample.valueFhead : sample.valuePeekByName) + '" title="' + sample.caption + '">'
                        const strLen = type === 'fhead' ? sample.valueFhead.length : sample.valuePeekByName.length

                        options += (type === 'fhead' ? sample.valueFhead : sample.valuePeekByName) + (maxLength - strLen > 0 ? '&nbsp;'.repeat(maxLength - strLen) : '&nbsp;') + sample.caption
                        options += '</option>'
                    }
                })

                options += '</optgroup>'
            }
        })

        optStatsFheadPeekByNameSelectorSamples.append(options)

        // regions list
        optStatsFheadPeekByNameSelectorRegions.empty()
        options = ''

        for (const region in app.system.regions) {
            options += '<option value="' + region + '">' + region + '</option>'
        }

        optStatsFheadPeekByNameSelectorRegions.append(options)

        // process add / edit mode
        if (editMode === false) {
            this.editModeIx = -1

            $('#modalstatsFheadPeekByNameSelectorTitle').text(type.toUpperCase() + ': Adding entry')


            // try to select DEFAULT
            $('#optStatsFheadPeekByNameSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected')

            // if not present, select first entry
            const sel = $('#optStatsFheadPeekByNameSelectorRegions option:first-child').val()

            if (sel && sel !== 'DEFAULT') $('#optStatsFheadPeekByNameSelectorRegions option:first-child').attr('selected', 'selected')

            $('#inpStatsFheadPeekByNameSelectorRateSecs').val(1)

        } else {
            this.editModeIx = editMode
            this.sampleRateChanged = false

            const source = app.ui.stats.sources.entry.data[editMode]
            this.editModeIx = editMode

            $('#modalstatsFheadPeekByNameSelectorTitle').text(type.toUpperCase() + ': Editing entry')

            // select the region
            $('#optStatsFheadPeekByNameSelectorRegions option[value="' + source.data.regions + '"]').attr('selected', 'selected')

            // and the samples
            source.data.samples.forEach(sample => {
                $('#optStatsFheadPeekByNameSelectorSamples option[value="' + sample + '"]').attr('selected', 'selected')
            })

            // sample rate
            $('#inpStatsFheadPeekByNameSelectorRateSecs').val(source.sampleRate)
        }

        // display the dialog
        $('#modalStatsFheadPeekByNameSelector')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    sampleRateUpdated: function () {
        this.sampleRateChanged = true
    },

    okPressed: function () {
        const optStatsFheadPeekByNameSelectorSamples = $('#optStatsFheadPeekByNameSelectorSamples').val()
        const inpStatsFheadPeekByNameSelectorRateSecs = parseFloat($('#inpStatsFheadPeekByNameSelectorRateSecs').val())

        // samples
        if (optStatsFheadPeekByNameSelectorSamples.length === 0) {
            app.ui.msgbox.show('You must select at least one sample.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        // sample rate
        if (isNaN(inpStatsFheadPeekByNameSelectorRateSecs)) {
            app.ui.msgbox.show('You must enter the sample rate in seconds.', app.ui.msgbox.INPUT_ERROR)

            return
        }
        if (inpStatsFheadPeekByNameSelectorRateSecs === 0) {
            app.ui.msgbox.show('You must enter a sample rate greater than 0.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        const rate = inpStatsFheadPeekByNameSelectorRateSecs.toString().split('.')
        if (rate[1] && rate[1].length > 1) {
            app.ui.msgbox.show('The decimal part of the sample rate must be 1 digit only, as the step is 100 milliseconds.', app.ui.msgbox.INPUT_ERROR)

            return
        }

        const region = $('#optStatsFheadPeekByNameSelectorRegions').val()
        let source = {
            type: this.type,
            sampleRate: parseFloat(inpStatsFheadPeekByNameSelectorRateSecs),
            data: {
                enabled: true,
                samples: optStatsFheadPeekByNameSelectorSamples,
                processes: {
                    type: 'agg'
                },
                regions: [region],
                file: this.type === 'fhead' ? app.system.regions[region].dbFile.flags.file : '',
                views: [],
            },
        }

        $('#modalStatsFheadPeekByNameSelector').modal('hide')

        if (this.editModeIx === -1) {
            source.id = app.ui.GUID()
            app.ui.stats.sources.entry.data.push(source)

            const lastIx = app.ui.stats.sources.entry.data.length - 1
            app.ui.stats.sources.entry.data[lastIx].ix = lastIx

            app.ui.stats.sources.refresh()

        } else {
            // EDIT mode
            const oldSource = app.ui.stats.sources.entry.data[this.editModeIx]

            // determine what has changed
            const samplesChanged = oldSource.data.samples.join(' ') !== source.data.samples.join(' ')
            const regionsChanged = oldSource.data.regions[0] !== source.data.regions[0]

            // sample rate only
            if (this.sampleRateChanged === true &&
                samplesChanged === false &&
                regionsChanged === false) {

                app.ui.inputbox.show('Update the source?', '', res => {
                    if (res === 'YES') {
                        source.id = oldSource.id
                        app.ui.stats.sources.entry.data[this.editModeIx].sampleRate = source.sampleRate

                        app.ui.stats.sources.refresh()
                    }
                })

                return
            }

            // region changed
            if (regionsChanged === true) {
                let message = 'The region has changed.<br>'

                if (this.sampleRateChanged === true) {
                    message += 'The sample rate has changed.<br>'
                }

                message += 'This will require a deletion of the existing views.<br><br>Do you want to proceed?'

                app.ui.inputbox.show(message, '', res => {
                    if (res === 'YES') {
                        source.id = oldSource.id

                        app.ui.stats.sources.entry.data[this.editModeIx] = source

                        app.ui.stats.sparkChart.renderer.render()

                        app.ui.stats.sources.refresh()
                    }
                })

                return
            }

            // samples change
            const report = app.ui.stats.sources.ygblstats.getReconInfo(this.editModeIx, source, oldSource)
            let message = ''

            if (report.text.length > 0) {
                message = 'The following view(s) will be affected:<br>'
                report.text.forEach(line => {
                    message += line + '<br>'
                })
            }

            message += 'Update the source?'

            app.ui.inputbox.show(message, '', res => {
                if (res === 'YES') {
                    // exec recon
                    console.log(report.indexes)
                    app.ui.stats.sources.ygblstats.execRecon(this.editModeIx, source, oldSource, report.indexes, false)

                    // refresh the screen to reflect changes
                    app.ui.stats.sparkChart.renderer.render()

                    // and refresh the sources screen, also to reflect the changes
                    app.ui.stats.sources.refresh()
                }
            })
        }
    },

    helpPressed: function () {
        app.ui.help.show('stats/' + this.type.toLowerCase() + '-manage')
    },

    editModeIx: -1,
    source: {},
    type: '',
    sampleRateChanged: false
}
