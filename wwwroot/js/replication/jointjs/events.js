/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.topology.joint.events = {
    mount: function () {
        app.ui.replication.topology.joint.paper
            .on('link:mouseenter', function (linkView) {
                linkView.addTools(new joint.dia.ToolsView({
                    name: 'onhover',
                    tools: [
                        new joint.linkTools.Vertices({stopPropagation: true}),
                        new joint.linkTools.Segments({stopPropagation: true}),
                        new joint.linkTools.Vertices({
                            snapRadius: 0,
                            redundancyRemoval: true
                        })
                    ]
                }))
            })

            .on('link:mouseleave', function (linkView) {
                if (!linkView.hasTools('onhover')) return;
                linkView.removeTools();
            })

            .on('blank:pointerdown', (evt, x, y) => {
                const origin = app.ui.replication.topology.joint.paper.translate()
                this.lastPos = {tx: evt.originalEvent.x - origin.tx, ty: evt.originalEvent.y - origin.ty}

                $('#divTopologyPaper').css('cursor', 'grabbing')
            })

            .on('blank:pointermove', (evt, x, y) => {
                $('#divTopologyPaper').css('cursor', 'grabbing')
                app.ui.replication.topology.joint.paper.translate(evt.originalEvent.x - this.lastPos.tx, evt.originalEvent.y - this.lastPos.ty)
            })

            .on('blank:pointerup', (evt, x, y) => {
                $('#divTopologyPaper').css('cursor', 'grab')

                app.ui.replication.topology.joint.undoRedo.saveEntry()
            })

            .on('blank:mouseenter', (evt, x, y) => {
                $('#divTopologyPaper').css('cursor', 'grab')
            })

            .on('blank:mouseenter', (evt, x, y) => {
                $('#divTopologyPaper').css('cursor', 'grab')
            })

            .on('blank:mousewheel', (evt, x, y, delta) => {
                const currentScale = app.ui.replication.topology.joint.paper.scale().sx

                if (delta === -1) {
                    app.ui.replication.topology.joint.paper.scale(currentScale - 0.005)

                } else if (delta === 1) {
                    app.ui.replication.topology.joint.paper.scale(currentScale + 0.005)
                }

                this.wheelTimer.start()
            })

            // *******************************************************
            // highlighters
            // *******************************************************
            .on('cell:mouseenter', function (elementView) {
                joint.highlighters.mask.add(elementView, {selector: 'root'}, 'highlight', {
                    deep: true,
                    attrs: {
                        'stroke': '#FF4365',
                        'stroke-width': 3
                    }
                });
            })

            .on('cell:mouseleave', function (elementView) {
                joint.dia.HighlighterView.remove(elementView);
            })

            // *******************************************************
            // context menus
            // *******************************************************
            .on('cell:pointerup', function (elementView, event) {
                event.stopPropagation();
                event.preventDefault();
            })

            .on('cell:contextmenu', function (cellView, event) {
                app.ui.replication.topology.joint.events.buildContextMenu(cellView, event)
            })

            .on('blank:mouseover', function (event) {
                $('#divContextMenuShapes').css('display', 'none')
                $('#divContextMenuShapesDead').css('display', 'none')
                $('#divContextMenuLinks').css('display', 'none')
            })

        app.ui.replication.topology.joint.undoRedo.mountHandlers()
    },

    buildContextMenu: function (cellView, event) {
        event.stopPropagation();
        event.preventDefault();

        switch (cellView.model.attributes.type) {
            case 'Primary':
            case 'Secondary':
            case 'Supplementary':
                if (cellView.model.dead === true) {
                    // display menu
                    $('#divContextMenuShapesDead')
                        .css('display', 'block')
                        .css('left', event.originalEvent.clientX + 'px')
                        .css('top', (event.originalEvent.clientY - 50) + 'px')

                    // mount events
                    $('#menuReplContextErrorDetails')
                        .off()
                        .on('click', function (event) {
                            $('#divContextMenuShapesDead').css('display', 'none')

                            let msg = 'The following error occurred:<br>'

                            let serverInfo = 'N/A'

                            if (cellView.model.attributes.net) {
                                serverInfo = cellView.model.attributes.net.host + ':' + cellView.model.attributes.net.port
                            }

                            switch (cellView.model.attributes.data.ERROR) {
                                case 'server_not_reachable':
                                    msg += 'Couldn\'t connect to REST server: ' + serverInfo

                                    break
                                case 'curl:-5':
                                    msg += 'CURL: Could not resolve proxy: ' + serverInfo

                                    break
                                case 'curl:-6':
                                    msg += 'CURL: Could not resolve host: ' + serverInfo

                                    break
                                case 'curl:-7':
                                    msg += 'CURL: Failed to connect to host or proxy: ' + serverInfo

                                    break
                                case 'curl:-8':
                                    msg += 'CURL: The server data sent to libcurl could not be parsed'

                                    break
                                case 'curl:-28':
                                    msg += 'CURL: Operation timeout. The specified time-out period was reached: ' + serverInfo

                                    break
                                case 'curl:-35':
                                case 'curl:-53':
                                case 'curl:-54':
                                case 'curl:-56':
                                case 'curl:-58':
                                case 'curl:-59':
                                case 'curl:-60':
                                case 'curl:-64':
                                case 'curl:-66':
                                case 'curl:-77':
                                case 'curl:-80':
                                case 'curl:-82':
                                case 'curl:-83':
                                case 'curl:-90':
                                case 'curl:-91':
                                case 'curl:-98':
                                    msg += 'CURL: SSL error: ' + +cellView.model.attributes.data.ERROR + ' on server: ' + serverInfo

                                    break
                                case 'tls_file_not_found':
                                    msg += 'The SSL file was not found on the server: ' + serverInfo

                                    break

                                case 'status_500':
                                    msg += 'An internal REST server error occurred: ' + serverInfo

                                    break
                                default:
                                    msg += 'Generic error: ' + cellView.model.attributes.data.ERROR + ' on server: ' + serverInfo
                            }

                            app.ui.msgbox.show(msg, 'ERROR')
                        })

                    return
                }

                const menuReplContextOpenGui = $('#menuReplContextOpenGui')
                const menuReplContextOpenInstanceFile = $('#menuReplContextOpenInstanceFile')

                // disable current machine Open GUI menu
                if (cellView.model.id === app.system.replication.instanceFile.flags.instanceName) menuReplContextOpenGui.addClass('disabled')
                else menuReplContextOpenGui.removeClass('disabled')

                // display menu
                $('#divContextMenuShapes')
                    .css('display', 'block')
                    .css('left', event.originalEvent.clientX + 'px')
                    .css('top', (event.originalEvent.clientY - 50) + 'px')

                // mount events
                menuReplContextOpenGui
                    .off()
                    .on('click', function (event) {
                        const net = cellView.model.attributes.net
                        const instanceName = cellView.model.attributes.id
                        let dockerFound = false
                        let baseAddress

                        // are we in docker?
                        app.system.systemInfo.envVars.forEach(envVar => {
                            if (envVar.name === 'ydbgui_docker') dockerFound = true
                        })

                        baseAddress = dockerFound === true ? window.location.origin.split(':')[0, 1] : net.protocol + '://' + net.host
                        window.open(baseAddress + ':' + net.port, instanceName)

                        $('#divContextMenuShapes').css('display', 'none')
                    })

                $('#menuReplContextOpenLogFiles')
                    .off()
                    .on('click', function (event) {
                        app.ui.replication.logs.show(cellView.model.attributes.data.logFiles, cellView.model.attributes.id)

                        $('#divContextMenuShapes').css('display', 'none')
                    })

                menuReplContextOpenInstanceFile
                    .off()
                    .on('click', function (event) {
                        app.ui.replication.instanceFile.show(cellView.model.attributes.data.instanceFile)

                        $('#divContextMenuShapes').css('display', 'none')
                    })

                break

            default  :
                if (cellView.path.segments.length < 3) return

                $('#divContextMenuLinks')
                    .css('display', 'block')
                    .css('left', event.originalEvent.clientX + 'px')
                    .css('top', (event.originalEvent.clientY - 50) + 'px')

                $('#menuReplContextLinkReset')
                    .off()
                    .on('click', function (event) {
                        event.stopPropagation()

                        for (let cnt = 0; cnt < cellView.path.segments.length + 2; cnt++) {
                            cellView.model.removeVertex(-1)
                        }

                        $('#divContextMenuLinks').css('display', 'none')
                    })
        }
    },

    wheelTimer: {
        start: function () {
            if (this.handle > 0) clearTimeout(this.handle)

            this.handle = setTimeout(() => {
                app.ui.replication.topology.joint.undoRedo.saveEntry()
            }, 500)
        },
        handle: 0
    },

    lastPos: {x: 1, y: 2}
}
