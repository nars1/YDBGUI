/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const webSocket = require('./webSockets')
const process = require('process')
const LOGGER = require("./logger");

const args = process.argv.slice(2)
let port = 8090
let ydb_dist = ''
let tls_info = ''
logging = false
logDir = ''

process.title = 'ydbgui-ws-server'

args.forEach(arg => {
    if (arg.indexOf('--port') > -1 && (parseInt(arg.split('=')[1])) > 0) {
        port = parseInt(arg.split('=')[1])
    }

    if (arg.indexOf('--ydb_dist') > -1) {
        ydb_dist = arg.split('=')[1]
        if (ydb_dist.slice(-1) !== '/') ydb_dist += '/'
    }

    if (arg.indexOf('--tls_info') > -1) {
        tls_info = arg.split('=')[1]
    }

    if (arg.indexOf('--logging') > -1) {
        const argSplitted = arg.split('=')

        logging = true
        logDir = argSplitted[1]
    }

    if (arg.indexOf('--help') > -1) {
        console.log('YottaDB GUI Socket Server \n\nParameters:')
        console.log('--port=nnnn\t\t\t(optional) If not set, it will default to 8090')
        console.log('--ydb_dist=/path/to/dist\t(optional) The path of the YDB installation, if not available through env vars')
        console.log('--logging=/path/to/log\t\t(optional) Turns logging on, writing to the specified path')
        console.log('--help\t\t\t\tThis dialog\n')

        process.exit()
    }
})

LOGGER.log('Starting Socket server using: ' + args.join(' '), LOGGER.INFO, true)

webSocket.createServer(port, ydb_dist, tls_info, process.env.ydbguiServerInfo)
