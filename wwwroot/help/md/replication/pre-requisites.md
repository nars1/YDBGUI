<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## Pre-requisites

---

In order for the topology to work, you must have the [YottaDB Web Server](https://gitlab.com/YottaDB/Util/YDB-Web-Server)  installed on each replicated instance.


<img src="md/replication/configuration/img/layout-config.png" width="900"> 

In this example we install the web server in each instance, using the same IP number as the address / host used for the replication, but on a different port.

If you are using a firewall, you will need to make the web server port available to its parent.

> E.g. tampa2 web port 9500 needs to be visible to tampa1 only, as the software will crawl from Melbourne to Chicago, then to tampa1 and finally to tampa2.
