<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## <a href="#" onclick="app.link('replication/modify-topology')">Modifying, saving and recalling the Topology interactive chart</a>

## Load dialog

---

<img src="md/replication/img/edit-load-dialog.png" width="600">

The Load dialog is used to load a previously saved topology.

By clicking on a name on the list, it will populate the description pane and the Autoload flag status.

#### Deleting existing layouts

By selecting an item and clicking on the Delete button, you will permanently delete the layout.
