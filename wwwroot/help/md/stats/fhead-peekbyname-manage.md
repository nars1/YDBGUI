<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/sources')">Sources</a> / Add - Edit

---

<img src="md/stats/img/add-entry-dialog.png" width="600"> 

The Add - Edit Sources dialog allows you to create or edit a Data Collection Source.

A Data Collection Source is made up of:

- The sample(s) you wish to collect
- The target database(s)
- The target process(es)
- The sample rate

### Samples

The samples are grouped by category. You can select as many samples as you want, by using the `CTRL` key to add/remove samples to the mouse selection or the `SHIFT` key to select a range.

For each sample set, a statistic dataset (delta/avg/min/max) will be created upon receiving each sample packet. Each sample set can be used in up to 2 graphs or in the Line Report.

### Target databases

You can select one or more regions to get the samples from. Additionally, you can select the *, meaning you will get samples from all the databases in your system.

### Target processes

You can choose which processes to target in your data acquisition process:

- By selecting `All processes (aggregate)` you will get TOTALS of all processes
- By selecting `Top` you can get individual processes information
  - The mandatory `processes` numeric field allows you to enter HOW MANY processes you want to return
  - The mandatory `Sample` field is the sample field that will be used to compute the processes to return
- By selecting `Pids` you can specified one or more running PIDs. By clicking on the `Refresh` button if will
  re-populate the PIDs list

### Sample rate

The `sample rate` is the interval we use to get the sample data.

The minimum sample rate is 100 milliseconds, and it has an unlimited maximum.

If you have multiple sources, each with different sample rate; the Statistics Server will take care of collecting and distributing the returned samples according.
