<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Faq

---

- Can I display custom text on the Table?
- Can I use one source with many samples against many Graphs and Tables?
- In which format are the exported Report files?
- Can I use different sample times with different sources?
- Can I run statistics from two browsers at the same time?
- Can I change the height of a graph?

---

##### Can I display custom text on the Table?

Yes, you can. Just click on the header, where the region text appear, and type the new text.

---

##### Can I use one source with many samples against many Graphs and Tables?

Yes, you can configure a source to have many samples and then route them using the mapping to as many Graphs and Tables
as you want.

---

#####  What is the format of the exported Report files?

The exported Report files are in JSON format.

---

##### Can I use different sample times with different sources?

Yes, the Web Socket server will take care of collecting the multiple data sets at different intervals.

---

##### Can I run statistics from two browsers at the same time?

This is not possible with the current version.

---

##### Can I change the height of a graph ?

Yes, you can. Just drag the bottom of the graph with the mouse. The new height will be stored in the graph and recalled when you open it again.

---
