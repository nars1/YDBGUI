<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Report definition

---

<img src="md/stats/spark-chart/img/spark-chart.png" width="600">

In the Report Definition dialog you configure the appearance of the report.

Each Source is displayed as line in the Sources list:

<img src="md/stats/spark-chart/img/sc-entry.png" width="400">

In the list you can see the overview of the Source parameters:

- Sample
- Processes
- Databases
- S / R (Sample rate)
- Views

A `View` is a graphical representation of (a portion of) the statistical data. It can be either a Graph or a Table.

While the entire report can have a maximum of 2 `Graphs views` (coming from either one or two different sources), the
number of `Tables` is unlimited.

> Any change made in this dialog will be lost if you press the `Cancel` button. Only when you press the `Ok` button changes will be applied to the current report.

---

#### Adding a View to the report

You can add a View by clicking the <img src="md/stats/spark-chart/img/sc-add-btn.png" width="26"> icon.

It will then display a choice:

<img src="md/stats/spark-chart/img/sc-add-menu.png" width="250">

Once you select the item you want, it will be added to the Views list.

<img src="md/stats/spark-chart/img/sc-view-report-line.png" width="350">

<img src="md/stats/spark-chart/img/sc-view-graph.png" width="350">

The View has:

- a `delete button` to remove the view from the report
- an indicator icon, to identify the view type (graph or table)
- a `mapping button` to choose the fields to use in the view
- a `properties button`, to choose the view layout

---

#### Deleting a View from the report

By clicking on the <img src="md/stats/spark-chart/img/sc-delete-btn.png" width="26"> button you remove the view from the report.

---

#### Change the mapping of a View

By clicking on the <img src="md/stats/spark-chart/img/sc-mapping-btn.png" width="120"> button you display the <a href="#" onclick="app.link('stats/spark-chart/mapping')">Mapping dialog</a>, used to select the fields used in the view.

---

#### Editing the View's properties

By clicking on the <img src="md/stats/spark-chart/img/sc-properties-btn.png" width="120"> button you display the Properties dialog, where you can change the appearance of the current view.

Depending on the View type, it will automatically display one of the following dialogs:

- <a href="#" onclick="app.link('stats/spark-chart/graph')">Graph dialog</a>

- <a href="#" onclick="app.link('stats/spark-chart/report-line')">Table dialog</a>

#### Changing the graph configuration

When you have more than one graph in your report, by clicking on the <img src="md/stats/spark-chart/img/sc-graphs-settings-btn.png" width="600"> button you can change the way the graphs are arranged.

It will display the <a href="#" onclick="app.link('stats/spark-chart/graph-config')">Graph configuration dialog</a>.
