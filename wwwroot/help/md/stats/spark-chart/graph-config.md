<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/spark-chart/index')">Report definition</a> / Graph config

---

<img src="md/stats/spark-chart/img/sc-graphs-configuration-dialog.png" width="600">

The Graph configuration dialog allows you to control the appearance of the two graph and it appears only when you have TWO graphs in your report.

If you have no graph or only one graph, it will display a warning message.

##### Horizontal

<img src="md/stats/spark-chart/img/graphs-h.png" width="500">

##### Vertical

<img src="md/stats/spark-chart/img/graphs-v.png" width="500">


> You can still change, at run time, the height of the graph by dragging its bottom.
