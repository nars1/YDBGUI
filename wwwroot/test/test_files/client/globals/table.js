/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("CLIENT: Globals > Table: general", async () => {
    execSync('wwwroot/test/globals/createglobals.sh');

    it("Test # 1200: Submit a query to ^ORD and verify that the record size is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1200`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(200);

        const $tbody = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody').find('tr'));
        expect($tbody.length >= 200).to.be.true
    });

    it("Test # 1201: Data view: regular: Submit a query to ^ORD and verify that data is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1201`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // switch to data view: normal
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'no'));

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        const $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').text());
        expect($data).to.have.string('"ORDER STATUS^100.01I^99^16"')
    });

    it("Test # 1202: Data view: pieces: Submit a query to ^ORD and verify that data is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1202`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        const $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').text());
        expect($data).to.have.string('"ORDER STATUS^100.01I^99^16"')
    });

    it("Test # 1204: Data view: pieces: Change piece char, submit a query to ^ORD and verify that data is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1204`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '.'));
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').text());
        expect($data).to.have.string('ORDER STATUS^100.01I^99^16')
    });

    it("Test # 1205: Data view: pills: Change piece char, submit a query to ^ORD and verify that data is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1205`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '.'));
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').text());
        expect($data).to.have.string('P1 ORDER STATUS^100 P2 01I')
    });

    it("Test # 1206: Data view: force the data size to 16 chars verify that data length gets trimmed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1206`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.delay(2500)

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.maxDataLength = 16);
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'no'));

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').text());
        expect($data).to.have.string('"ORDER STATUS^100"…')
    });

    it("Test # 1207: Data view: force the data size to 16 chars verify that a button is present", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1207`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // switch to 16 chars max length
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.maxDataLength = 16);
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'no'));

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').find('button'));
        expect($data.length === 1).to.be.true;
    });

    it("Test # 1208: switch to dark mode and verify that table changes appearance", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1208`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // switch to dark mode
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.appearance.theme = 'dark');

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        let $data = await page.evaluate(() => $('.table-dark'));
        expect($data.length === 1).to.be.true;
    });

    it("Test # 1209: Zoom in and verify that font is bigger", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1209`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        const oldFontSize = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(1)').css('font-size'));

        // zoom in
        await page.evaluate(() => app.ui.gViewer.table.zoom('-G-1', '+'));

        await libs.delay(250);

        const newFontSize = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(1)').css('font-size'));

        expect(newFontSize > oldFontSize).to.be.true;
    });

    it("Test # 1210: Zoom out and verify that font is smaller", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1210`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        const oldFontSize = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(1)').css('font-size'));

        // zoom out
        await page.evaluate(() => app.ui.gViewer.table.zoom('-G-1', '-'));

        await libs.delay(250);

        const newFontSize = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(1)').css('font-size'));

        expect(newFontSize < oldFontSize).to.be.true;
    });

    // COMMENTED OUT BECAUSE OF FAILURES IN SERVER, WILL BE FIXED ON NEXT SPRINT

    /*
    it("Test # 1211: Verify that the default bookmark color is loaded", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1211`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let defaultBgnd = await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.table.bookmarkColor);
        if (defaultBgnd === 'red') defaultBgnd = 'rgb(255, 0, 0)';

        const background = await page.evaluate(() => $('#lblGviewerBookmarkColor-G-1').css('background-color'));

        expect(background === defaultBgnd).to.be.true;
    });

    it("Test # 1212: Verify that the default data view is loaded", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1212`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let defaultDataMode = await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.table.rows.right.showPieces);
        if (defaultDataMode === 'pills') defaultDataMode = true;

        const hasPills = await page.evaluate(() => $('#lblGviewerToolbarPiecesPills-G-1').hasClass('active'));

        expect(defaultDataMode === hasPills).to.be.true;
    });

    it("Test # 1213: Verify that the default piece char is loaded", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1213`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.delay(250);

        let defaultPieceChar = await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.table.rows.right.pieceChar);

        const caption = await page.evaluate(() => $('#btnGviewerPieceChar-G-1').text());

        expect(caption).to.have.string(defaultPieceChar)
    });

    it("Test # 1214: Verify that the default theme is loaded", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1214`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.delay(250);

        let defaultTheme = await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.appearance.theme);

        let theme = await page.evaluate(() => $('#btnGviewerPieceChar-G-1').hasClass('active'));
        if (theme === false) theme = 'light';

        expect(defaultTheme).to.have.string(theme)
    });
     */

    it("Test # 1215: View: pieces. Display ^testglobal, verify that the first entry data is set to $h", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1215`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1216: View: pieces. Display ^testglobal, verify that the 2nd entry data is set to $zh", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1216`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(2) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1217: View: pieces. Display ^testglobal, verify that the 3rd entry data has piece 4 set to $H", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1217`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(3) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1218: View: pieces. Display ^testglobal, verify that the 4th entry data has piece 4 set to $ZH", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1218`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(4) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1219: View: pieces. Display ^testglobal, verify that the 5th entry subscript 2 is set to $H", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1219`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(5) td:nth-child(2)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1220: View: pieces. Display ^testglobal, verify that the 6th entry subscript 2 is set to $ZH", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1220`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(6) td:nth-child(2)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1221: View: pieces. Display ^testglobal, verify that the 7th entry subscript 4 is set to $ZH", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1221`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(7) td:nth-child(2)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1222: View: pills. Display ^testglobal, verify that the 1st entry data is set to $h", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1222`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1223: View: pills. Display ^testglobal, verify that the 2nd entry data is set to $zh", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1223`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(2) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1224: View: pills. Display ^testglobal, verify that the 3rd entry data has piece 4 set to $H", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1224`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(3) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1225: View: pills. Display ^testglobal, verify that the 4th entry data has piece 4 set to $ZH", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1225`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(4) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1226: View: pills. Display ^testglobal, verify that the 5th entry subscript 2 is set to $H", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1226`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(5) td:nth-child(2)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1227: View: pills. Display ^testglobal, verify that the 6th entry subscript 2 is set to $ZH", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1227`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(6) td:nth-child(2)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1228: View: pills. Display ^testglobal, verify that the 7th entry subscript 4 is set to $ZH", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1228`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(7) td:nth-child(2)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1229: View: pills. Display ^testglobal, verify that the 8th entry has a GTM+1", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1229`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(8) td:nth-child(3)').html());
        expect(htmlData).to.have.string('GMT+1')
    });

    it("Test # 1230: View: pills. Display ^testglobal, verify that the 9th entry has a GTM-1", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1230`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(9) td:nth-child(3)').html());
        expect(htmlData).to.have.string('GMT-1')
    });

    it("Test # 1231: View: pills. Display ^testglobal, verify that the 10th isNOT recognized as $h", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1231`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(10) td:nth-child(3)').html());
        expect(htmlData).to.not.have.string('global-horolog-viewer')
    });

    it("Test # 1232: View: plain. Display ^testglobal, verify that the 1st entry data is set to $h", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1232`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'no'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1) td:nth-child(3)').html());
        expect(htmlData).to.have.string('global-horolog-viewer')
    });

    it("Test # 1233: View: plain. Display ^testglobal, verify that the 10th is NOT set to $H", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1233`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'no'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(10) td:nth-child(3)').html());
        expect(htmlData).to.not.have.string('global-horolog-viewer')
    });

    it("Test # 1234: View: plain. Display ^testglobal, verify that the 11th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1234`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'no'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(11) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    it("Test # 1235: View: pieces. Display ^testglobal, verify that the 11th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1235`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(11) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    it("Test # 1236: View: pieces. Display ^testglobal, verify that the 12th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1236`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(12) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    it("Test # 1237: View: pieces. Display ^testglobal, verify that the 13th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1237`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'yes'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(13) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    it("Test # 1238: View: pills. Display ^testglobal, verify that the 11th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1238`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(11) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    it("Test # 1239: View: pills. Display ^testglobal, verify that the 12th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1239`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(12) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    it("Test # 1240: View: pills. Display ^testglobal, verify that the 13th is a json", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1240`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('testglobal');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // switch to data view: pieces
        await page.evaluate(() => app.ui.gViewer.table.changeRightView('-G-1', 'pills'));
        await page.evaluate(() => app.ui.gViewer.table.changePieceChar('-G-1', '^'));

        await libs.delay(250);

        const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(13) td:nth-child(3)').html());
        expect(htmlData).to.have.string('app.ui.gViewer.table.displayJSON')
    });

    /*
    it("Test # 1241: UTF8: try to read binary data in ^%ydboctoocto, should NOT return an error in both M and UTF8", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1241`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.delay(250);

        //type a new name
        await page.keyboard.type('%ydboctoocto');
        await page.keyboard.press('Enter');

        await libs.delay(250);


        await page.evaluate(() => app.ui.gViewer.table.globalValue.show('^%ydboctoocto(~functions~,~ABS~,~%ydboctoFCMDmr1RPkcS1QSpZq9WR11~,~binary~,0)'));

        await libs.delay(500)

        const htmlData = await page.evaluate(() => $('#txtGlobalNodeValue').html());

        expect(htmlData).to.have.string('abs')
    });

     */

    it("Test # 1242: UTF8: try to read utf8 data, should be ok", async () => {
        if (global.charMode === 'UTF-8') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1242`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            //type a new name
            await page.keyboard.type('UTF8test');
            await page.keyboard.press('Enter');

            await libs.delay(250);

            const htmlData = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1) td:nth-child(3)').html());
            expect(htmlData).to.have.string('こにちわ')
        }
    });
});

describe("CLIENT: Globals > Table: reverse fetches", async () => {
    it("Test # 1245: use ^PSNDF, jump to end with CTRL-END, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(250)

        //type a new name
        await page.keyboard.type('PSNDF');
        await page.keyboard.press('Enter');

        await libs.delay(2000)

        let $data2 = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // last row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","pH TEST",2265)')

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","TRIBULUS",4429)')
    })

    it("Test # 1246: use ^PSNDF, jump to end with CTRL-END, then CTRL-HOME, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('PSNDF');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // first row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","TRIBULUS",4429)')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","pH TEST",2265)')

        // jump to home
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('Home');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('PSNDF(50.6,0')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('PSNDF(50.6,40,"TERMSTATUS","B",3050310,1)')
    })

    it("Test # 1247: use ^PSNDF, jump to end with CTRL-END, then PG-UP until it scrolls, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('PSNDF');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // scroll
        for (let cnt = 0; cnt < 40; cnt++) {
            await page.keyboard.press('PageUp');

            await libs.delay(250)
        }

        // first row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","PYRIDOSTIGMINE",154)')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","SENNA",2074)')
    })

    it("Test # 1248: use ^PSNDF, jump to end with CTRL-END, then PG-UP until it scrolls, verify, then UP to scroll again, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('PSNDF');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // scroll
        for (let cnt = 0; cnt < 40; cnt++) {
            await page.keyboard.press('PageUp');

            await libs.delay(250)
        }

        // first row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","PYRIDOSTIGMINE",154)')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","SENNA",2074)')

        // scroll again
        for (let cnt = 0; cnt < 10; cnt++) {
            await page.keyboard.press('PageUp');

            await libs.delay(250)
        }

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","PLASMA PROTEIN FRACTION",853)')
    })

    it("Test # 1249: use ^PSNDF, jump to end with CTRL-END, then PG-UP until it scrolls, verify, then DOWN to scroll down , verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('PSNDF');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // scroll
        for (let cnt = 0; cnt < 40; cnt++) {
            await page.keyboard.press('PageUp');

            await libs.delay(250)
        }

        // first row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","PYRIDOSTIGMINE",154)')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","SENNA",2074)')

        // scroll again
        for (let cnt = 0; cnt < 30; cnt++) {
            await page.keyboard.press('PageUp');

            await libs.delay(250)
        }

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","NESIRITIDE",3720)')

        // scroll again, but down
        for (let cnt = 0; cnt < 30; cnt++) {
            await page.keyboard.press('PageDown');

            await libs.delay(250)
        }

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","OLMESARTAN",3806)')
    })

    it("Test # 1250: use ^PSNDF, jump to end with CTRL-END, then CTRL-HOME, verify, then PAGE-DOWN until it scrolls, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('PSNDF');
        await page.keyboard.press('Enter');

        await libs.delay(2000)

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // scroll
        for (let cnt = 0; cnt < 40; cnt++) {
            await page.keyboard.press('PageUp');

            await libs.delay(250)
        }

        // first row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","PYRIDOSTIGMINE",154)')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","SENNA",2074)')

        // jump to home
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('Home');
        await page.keyboard.up('ControlRight');

        await libs.delay(1000)

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,0)')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,40,"TERMSTATUS","B",3050310,1)')

        // scroll down
        for (let cnt = 0; cnt < 80; cnt++) {
            await page.keyboard.press('PageDown');

            await libs.delay(250)
        }

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('PSNDF(50.6,120,"VUID")')

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('PSNDF(50.6,159,"VUID")')
    })

    it("Test # 1251: use ^PSNDF(50.68,*), jump to end with CTRL-END, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1251`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(250)

        //type a new name
        await page.keyboard.type('PSNDF(50.6,*)');
        await page.keyboard.press('Enter');

        await libs.delay(2000)

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // last row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","pH TEST",2265)')

        // first row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^PSNDF(50.6,"B","TRIBULUS",4429)')
    })

    it("Test # 1252: create a global with negative subscripts, verify, jump to end with CTRL-END, verify", async () => {
        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'for x=-1E6:1:1E6 s ^NegTest(x)=x \'');

        await page.goto(`https://localhost:${MDevPort}//index.html?test=1252`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(250)

        //type a new name
        await page.keyboard.type('NegTest');
        await page.keyboard.press('Enter');

        await libs.delay(2000)

        // first row
        let $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1)').text());
        expect($data).to.have.string('^NegTest(-1000000)')

        // jump to end
        await page.keyboard.down('ControlRight');
        await page.keyboard.press('End');
        await page.keyboard.up('ControlRight');

        await libs.delay(2000)

        // last row
        $data = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(200)').text());
        expect($data).to.have.string('^NegTest(1000000)')

        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'kill ^NegTest h\'');
    })
})

describe("CLIENT: Globals > Table: fetches", async () => {
    it("Test # 1295: Set the max buffer to 100, initial fetch to 50: verify that data size = 50", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1250`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.maxSize = 250);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.initialFetchSize = 50);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.lazyFetchSize = 25);

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(100);

        const $tbody = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody').find('tr'));
        expect($tbody.length === 50).to.be.true
    });

    it("Test # 1296: Set the max buffer to 100, initial fetch to 50 and lazy to 25: verify that, scrolling down, it is fetching", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1251`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.maxSize = 100);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.initialFetchSize = 50);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.lazyFetchSize = 25);

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(10000));

        await libs.delay(500);

        const $tbody = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody').find('tr'));
        expect($tbody.length === 75).to.be.true
    });

    it("Test # 1297: Set the max buffer to 100, initial fetch to 50 and lazy to 25: verify that, scrolling down after buffer is full , it is fetching and beheading the buffer", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1252`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.maxSize = 100);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.initialFetchSize = 50);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.lazyFetchSize = 25);

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(10000));
        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(30000));
        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(30000));
        await libs.delay(250);

        const firstRowId = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:first-child').attr('id'));
        expect(firstRowId === 'rowTableGviewer-G-1-25').to.be.true
    });

    it("Test # 1298: Set the max buffer to 100, initial fetch to 50 and lazy to 25: verify that, scrolling down after buffer is full and up again, it is fetching and betailing the buffer", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1253`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.maxSize = 100);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.initialFetchSize = 50);
        await page.evaluate(() => app.ui.gViewer.instance['-G-1'].viewer.buffer.defaults.buffering.lazyFetchSize = 25);

        //type a new name
        await page.keyboard.type('ORD');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(10000));
        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(30000));
        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(30000));
        await libs.delay(250);

        await page.evaluate(() => $('#divGviewerTableScroll-G-1').scrollTop(-200000));
        await libs.delay(500);

        //await page.keyboard.down('Control');
        await page.keyboard.press('Home');

        await libs.delay(500);

        const $tbody = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody').find('tr'));
        expect($tbody.length === 100).to.be.true
    });

});
