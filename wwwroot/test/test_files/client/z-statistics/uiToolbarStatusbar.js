/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require('./utils')
const {exec} = require('child_process');

describe("Statistics: Stats: Toolbar", async () => {
    it("Test # 2550: check all buttons status at startup", async () => {
        await utils.initStats()

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2551: check all buttons status after loading report", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2552: check all buttons status after creating a source", async () => {
        await utils.initStats()

        await utils.openSource()

        let list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        const oldTotal = list.length

        await utils.openSourceAdd()

        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))
        await page.evaluate(() => app.ui.stats.sources.ygblstats.samplesChange())

        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        await page.evaluate(() => $('#statsYgblstatsSelectorProcessesTop').attr('checked', 'checked'))

        await page.evaluate(() => $('#optStatsYgblstatsSelectorTopSample option[value="SET"]').attr('selected', 'selected'))

        await page.evaluate(() => $('#numStatsYgblstatsSelectorProcessesTop').val(3))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        list = await page.evaluate(() => $('#tblStatsSources >tbody').children())
        expect(list.length > oldTotal).to.be.true

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2553: check all buttons status after creating a source and a report line w mapping", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(4))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(4))

        await utils.openSparkchart()

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '4c2d443f-a6bf-4f43-87dc-4e8ad669c96c'))

        elem = await page.$('#options_4c2d443f-a6bf-4f43-87dc-4e8ad669c96c > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2554: check all buttons status after creating a source and a graph w mapping", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(4))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(4))

        await utils.openSparkchart()

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '4c2d443f-a6bf-4f43-87dc-4e8ad669c96c'))

        elem = await page.$('#options_4c2d443f-a6bf-4f43-87dc-4e8ad669c96c > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2555: check all buttons status after creating a source and a report line w mapping and then disabling the source", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(4))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(4))

        await utils.openSparkchart()

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '4c2d443f-a6bf-4f43-87dc-4e8ad669c96c'))

        elem = await page.$('#options_4c2d443f-a6bf-4f43-87dc-4e8ad669c96c > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await utils.openSource()

        let btnClick = await page.$("#sourcesEntryEnabled-0");
        await btnClick.click();

        elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2556: check all buttons status after loading a report, then select new to clear it", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(4))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(4))

        await utils.openSparkchart()

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '4c2d443f-a6bf-4f43-87dc-4e8ad669c96c'))

        elem = await page.$('#options_4c2d443f-a6bf-4f43-87dc-4e8ad669c96c > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false

        // clear buffer
        page.evaluate(() => app.ui.stats.tab.toolbar.newStat())

        await libs.waitForDialog('modalInputbox');

        let val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('The report file has been modified.')

        btnClick = await page.$("#btnInputboxNo");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('modalInputbox');

        val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('This will clear all the sources')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2557: load report, hit start, check other buttons enabled status", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2558: load report, hit start, then pause, check other buttons enabled status", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsPause");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2559: load report, hit start, then stop, check other buttons enabled status", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })

    it("Test # 2560: load report, hit start, then start again, it should be paused", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsPause').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.false

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // check toolbar status
        expect(await page.evaluate(() => $('#btnStatsRecord').prop('disabled'))).to.be.false
        expect(await page.evaluate(() => $('#btnStatsStop').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsSources').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsDef').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsTheme').prop('disabled'))).to.be.false

        expect(await page.evaluate(() => $('#btnStatsNew').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsLoad').prop('disabled'))).to.be.true
        expect(await page.evaluate(() => $('#btnStatsSave').prop('disabled'))).to.be.true

        expect(await page.evaluate(() => $('#btnStatsHelp').prop('disabled'))).to.be.false
    })
})

describe("Statistics: Stats: Status bar for PIDs process mode", async () => {
    it("2561: Load report 7, run, msgbox should display saying nothing is running", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(7))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(7))

        // hit start
        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // should display the msgbox
        await libs.waitForDialog('#modalMsgbox');
    })

    /*
    it("2562: Load report 7, run test program, dialog should appear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(7))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(7))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');

        // hit start
        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // should display the pids dialog
        await libs.waitForDialog('#statsYgblPids');
    })

     */
})

describe("Statistics: Stats: Status bar", async () => {
    it("Test # 2570: at startup, check Status field", async () => {
        await utils.initStats()

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerStatus').text())
        expect(val).to.have.string('Stopped')
    })

    it("Test # 2571: at startup, check Sample rate field", async () => {
        await utils.initStats()

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsStatusBarSampleRateValue').text())
        expect(val).to.have.string('N/A')
    })

    it("Test # 2572: at startup, check Timestamp field", async () => {
        await utils.initStats()

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerTimestamp').text())
        expect(val).to.have.string('00:00:00')
    })

    it("Test # 2573: at startup, check # of samples field", async () => {
        await utils.initStats()

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerSamples').text())
        expect(val).to.have.string('0')
    })

    it("Test # 2574: load a report, run it, check Status field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerStatus').text())
        expect(val).to.have.string('Running')
    })

    it("Test # 2575: load a report, run it, check Sample rate field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsStatusBarSampleRateValue').text())
        expect(val).to.have.string('0.5 secs')
    })

    it("Test # 2576: load a report, run it, check Timestamp field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerTimestamp').text())
        expect(val !== '00:00:00').to.be.true
    })

    it("Test # 2577: load a report, run it, check # of samples field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerSamples').text())
        expect(val !== '0').to.be.true
    })

    it("Test # 2578: load a report, run it, pause it, check Status field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsPause");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerStatus').text())
        expect(val).to.have.string('Paused')
    })

    it("Test # 2579: load a report, run it, pause it, check Sample rate field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsPause");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsStatusBarSampleRateValue').text())
        expect(val).to.have.string('0.5 secs')
    })

    it("Test # 2580: load a report, run it, stop it, check Status field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerStatus').text())
        expect(val).to.have.string('Stopped')
    })

    it("Test # 2581: load a report, run it, stop it, check Sample rate field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsStatusBarSampleRateValue').text())
        expect(val).to.have.string('0.5 secs')
    })

    it("Test # 2582: load a report, run it, stop it, check Timestamp field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerTimestamp').text())
        expect(val !== '00:00:00').to.be.true
    })

    it("Test # 2583: load a report, run it, stop it, check # of samples field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsControllerSamples').text())
        expect(val !== '0').to.be.true
    })

    it("Test # 2584: load a report with multiple source, multiple sample rates, verify Sample rate field", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(5))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(5))

        btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        await libs.delay(2000)

        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // verify status bar fields
        let val = await page.evaluate(() => $('#lblStatsStatusBarSampleRateValue').text())
        expect(val).to.have.string('1 / 2.5 secs')
    })

    it("Test # 2585: load a report, clear it, verify all fields", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(5))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(5))

        // clear buffer
        page.evaluate(() => app.ui.stats.tab.toolbar.newStat())

        await libs.waitForDialog('modalInputbox');

        let val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('This will clear all the sources')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // verify status bar fields
        val = await page.evaluate(() => $('#lblStatsControllerStatus').text())
        expect(val).to.have.string('Stopped')

        val = await page.evaluate(() => $('#lblStatsStatusBarSampleRateValue').text())
        expect(val).to.have.string('N/A')

        val = await page.evaluate(() => $('#lblStatsControllerTimestamp').text())
        expect(val).to.have.string('00:00:00')

        val = await page.evaluate(() => $('#lblStatsControllerSamples').text())
        expect(val).to.have.string('0')
    })
})
