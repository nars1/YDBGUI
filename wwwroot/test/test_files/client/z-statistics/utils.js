/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const fs = require('fs');

const loadReport = name => {
    const path = '/YDBGUI/wwwroot/test/statsTestFiles/' + name + '.ysr'

    try {
        return JSON.parse(fs.readFileSync(path).toString())

    } catch (err) {
        console.log(err)
        return {}
    }
}

const initStats = async () => {
    return new Promise(async function (resolve) {
        await page.goto(`https://localhost:${MDevPort}//index.html?testStats`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(500)

        // init stats
        await page.evaluate(() => app.userSettings.stats.displayHelpAtTabOpen = false)
        await page.evaluate(() => app.ui.stats.showTab())

        await libs.waitForDialog('#tab-SDiv');

        resolve()
    })
}

const openSource = async () => {
    await page.evaluate(() => app.ui.stats.sources.show())

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalStatsSources');
}

const openSourceAdd = async () => {
    await page.evaluate(() => app.ui.stats.sources.ygblstats.show())

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalstatsYgblstatsSelector');
}

const openSourceFheadAdd = async () => {
    await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.show('fhead'))

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');
}

const openSourcePeekByNameAdd = async () => {
    await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.show('peekByName'))

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');
}

const openSave = async () => {
    await page.evaluate(() => app.ui.stats.storage.save.show())

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalStatsSourcesSave');
}

const createSource = async (region = 'DEFAULT') => {
    await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))
    await page.evaluate(() => app.ui.stats.sources.ygblstats.samplesChange())

    switch (region) {
        case 'DEFAULT':
            await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

            break
        case 'YDBAIM':
            await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))
    }

    await page.evaluate(() => $('#statsYgblstatsSelectorProcessesTop').attr('checked', 'checked'))
    await page.evaluate(() => $('#optStatsYgblstatsSelectorTopSample option[value="SET"]').attr('selected', 'selected'))
    await page.evaluate(() => $('#numStatsYgblstatsSelectorProcessesTop').val(3))

    const elem = await page.$('#btnStatsYgblstatsSelectorOk');
    await libs.clickOnElement(elem)

    await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');
}

const createSourceFheadPeekByName = async (type, region = 'DEFAULT') => {
    if (type === 'fhead') await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_set"]').attr('selected', 'selected'))
    else await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_set"]').attr('selected', 'selected'))

    switch (region) {
        case 'DEFAULT':
            await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

            break
        case 'YDBAIM':
            await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))
    }

    const elem = await page.$('#btnStatsFheadPeekByNameSelectorOk');
    await libs.clickOnElement(elem)

    await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');
}

const createMapping = async () => {
    await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
    await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked', true))
    await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked', true))

    elem = await page.$('#btnStatsSparkChartMappingOk');
    await libs.clickOnElement(elem)

    await libs.waitForDialog('#modalStatsSparkChartMapping', 'close')
}

const openOpen = async () => {
    await page.evaluate(() => app.ui.stats.storage.load.show())

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalStatsSourcesLoad');
}

const openSparkchart = async () => {
    await page.evaluate(() => app.ui.stats.sparkChart.show())

    // wait for dashboard to be set by the async call
    await libs.waitForDialog('#modalStatsSparkChart');
}

module.exports.loadReport = loadReport;
module.exports.initStats = initStats;
module.exports.openSource = openSource;
module.exports.openSourceFheadAdd = openSourceFheadAdd;
module.exports.openSourcePeekByNameAdd = openSourcePeekByNameAdd;
module.exports.openSourceAdd = openSourceAdd;
module.exports.openSave = openSave;
module.exports.openOpen = openOpen;
module.exports.createSource = createSource;
module.exports.createSourceFheadPeekByName = createSourceFheadPeekByName;
module.exports.openSparkchart = openSparkchart;
module.exports.createMapping = createMapping;
