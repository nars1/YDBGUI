/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Stats: Sources: FHEAD Add", async () => {
    it("Test # 2730: Click ok, should display msgbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        let btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2731: select 1 sample,click ok, should create source, verify in table", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_set"]').attr('selected', 'selected'))

        let btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let rowData = await page.evaluate(() => $('#tblStatsSources > tbody > tr ').text())
        expect(rowData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_setDEFAULT1.000')
    })

    it("Test # 2732: select 3 samples, change region, change sample rate, click ok, verify in table", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_set"]').attr('selected', 'selected'))
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_get"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))

        let btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let rowData = await page.evaluate(() => $('#tblStatsSources > tbody > tr ').text())
        expect(rowData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_set, sgmnt_data.gvstats_rec.n_getYDBAIM3.000')
    })
})

describe("Statistics: Stats: Sources: FHEAD Edit", async () => {
    it("Test # 2740: Create source, enter edit mode, change sample rate, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2741: Create source, enter edit mode, change sample rate, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_setDEFAULT3.000')
    })

    it("Test # 2742: Create source, enter edit mode, change region, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2743: Create source, enter edit mode, change region, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');


        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_setYDBAIM1.000')
    })

    it("Test # 2744: Create source, enter edit mode, change region and sample rate, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2745: Create source, enter edit mode, change region and sample rate, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_setYDBAIM3.000')
    })

    it("Test # 2746: Create source, enter edit mode, change samples, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2747: Create source, enter edit mode, change samples, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_set, sgmnt_data.gvstats_rec.n_lock_failDEFAULT1.000')
    })

    it("Test # 2748: Create source, enter edit mode, change samples and sample rate, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2749: Create source, enter edit mode, change samples and sample rate, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_set, sgmnt_data.gvstats_rec.n_lock_failDEFAULT3.000')
    })

    it("Test # 2750: Create source, enter edit mode, change samples and region, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test #  2751: Create source, enter edit mode, change samples and region, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_set, sgmnt_data.gvstats_rec.n_lock_failYDBAIM1.000')
    })

    it("Test # 2752: Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test #  2753: Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceFheadAdd()

        await utils.createSourceFheadPeekByName('fhead')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="sgmnt_data.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('FHEADsgmnt_data.gvstats_rec.n_set, sgmnt_data.gvstats_rec.n_lock_failYDBAIM3.000')
    })
})
