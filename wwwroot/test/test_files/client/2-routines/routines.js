/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("Routine viewer: Menus", async () => {
    it("Test # 1500: Select menu and verify that routines selector is displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1500`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // click menu
        await page.evaluate(() => app.ui.rViewer.select.show());

        await libs.delay(200)

        // make sure dialog is visible
        const isVisible = await libs.getCssDisplay('#modalRviewerSelect') !== 'none';
        expect(isVisible).to.be.true;

    });
});

describe("Routine viewer: Routines select", async () => {
    it("Test # 1530: Verify that query submit button is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1530`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        const state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));

        expect(state).to.be.true;
    });

    it("Test # 1531: Verify that open button is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1531`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        const state = await page.evaluate(() => $('#btnRviewerSelectShow').hasClass('disabled'));

        expect(state).to.be.true;
    });

    it("Test # 1532: Type: a and verify that submit is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1532`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('a');

        const state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));

        expect(state).to.be.true;
    });

    it("Test # 1533: Type: % and verify that submit is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1533`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%');

        const state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));

        expect(state).to.be.true;
    });

    it("Test # 1534: Type: * and verify that submit is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1534`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('*');

        const state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));

        expect(state).to.be.true;
    });

    it("Test # 1535: Type: a* and verify that submit is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1535`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('a*');

        const state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));

        expect(state).to.be.true;
    });


    it("Test # 1536: Type: %* and verify that submit is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1536`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%*');

        const state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));

        expect(state).to.be.true;
    });

    it("Test # 1537: Type: %y* and verify that submit is enabled, but open is disabled", async () => {
        // create routine
        execSync('cd /YDBGUI/routines && echo " new a\n" > _test.m');
        execSync('cd /YDBGUI/routines && echo " new a\n" > _test2.m');

        await libs.delay(200);

        await page.goto(`https://localhost:${MDevPort}//index.html?test=1537`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%t*');
        await page.keyboard.press('Enter');

        await libs.delay(1000);

        let state = await page.evaluate(() => $('#btnRviewerMaskSubmit').hasClass('disabled'));
        expect(state).to.be.false;


        state = await page.evaluate(() => $('#btnRviewerSelectShow').hasClass('disabled'));
        expect(state).to.be.true;

    });

    it("Test # 1538: Type: %y* and verify that list is populated correctly", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1538`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%t*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let cell = await page.$('#tblRviewerSelect >tbody >tr >td:nth-child(1) ');
        let text = await page.evaluate(el => el.textContent, cell);
        expect(text === '%t').to.be.true;
    });

    it("Test # 1539: Type: norou* and verify that list returns: No routines found...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1539`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('norou*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let cell = await page.$('#tblRviewerSelect >tbody >tr >td:nth-child(1)');
        let text = await page.evaluate(el => el.textContent, cell);
        expect(text === 'No routines found...').to.be.true;
    });

    it("Test # 1540: Type: %y*  and select one item, submit and verify that open is enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1540`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%t*');
        await page.keyboard.press('Enter');

        await libs.delay(1000);

        let row = await page.$('#tblRviewerSelect >tbody >tr ');
        row.click();

        await libs.delay(100)

        state = await page.evaluate(() => $('#btnRviewerSelectShow').hasClass('disabled'));
        expect(state).to.be.false;
    });

    it("Test # 1541: Type: %y* and select two items, submit and verify that open is enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1541`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%t*');
        await page.keyboard.press('Enter');

        await libs.delay(1000);

        let row = await page.$('#tblRviewerSelect >tbody >tr');
        row.click();

        row = await page.$('#tblRviewerSelect >tbody >tr:nth-child(1)');
        row.click();

        await libs.delay(1000);

        state = await page.evaluate(() => $('#btnRviewerSelectShow').hasClass('disabled'));
        expect(state).to.be.false;
    });

    it("Test # 1542: Type: %y*  and select one item, submit and verify that one tab gets created", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1542`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%t*');
        await page.keyboard.press('Enter');

        await libs.delay(1000);

        let row = await page.$('#tblRviewerSelect >tbody >tr ');
        row.click();

        await libs.delay(100);

        let button = await page.$('#btnRviewerSelectShow');
        button.click();

        await libs.delay(500);

        await page.$('#tabRviewer-R-1Div');
    });

    it("Test # 1543: Type: %y* and select two items, submit and verify that two tabs gets created", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1543`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('%t*');
        await page.keyboard.press('Enter');

        await libs.delay(1000);

        let row = await page.$('#tblRviewerSelect >tbody >tr ');
        row.click();

        row = await page.$('#tblRviewerSelect >tbody >tr:nth-child(2) ');
        row.click();

        await libs.delay(100);

        let button = await page.$('#btnRviewerSelectShow');
        button.click();

        await page.$('#tabRviewer-R-1Div');
        await page.$('#tabRviewer-R-2Div');

        // delete routine
        execSync('rm /YDBGUI/routines/_test.m');
    });
});
