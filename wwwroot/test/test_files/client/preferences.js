/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("CLIENT: Preferences: general", async () => {
    it("Test # 2950: Open the dialog, click on Backup, should display two PATH entries in the table", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        btnClick = await page.$("#Backup_anchor");
        await btnClick.click();

        await libs.delay(200)

        let row = await page.evaluate(() => $('#tblPrefsItems > tbody tr:nth-child(1)').text())
        expect(row).to.have.string('Target path:/...')

        row = await page.evaluate(() => $('#tblPrefsItems > tbody tr:nth-child(2)').text())
        expect(row).to.have.string('Replication target path:/...')
    })

    it("Test # 2951: Open the dialog, double-click REST, click on Timeout, should display two numeric entries in the table", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#REST_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#REST-Timeout_anchor");
        await btnClick.click();

        await libs.delay(200)

        let row = await page.evaluate(() => $('#tblPrefsItems > tbody tr:nth-child(1)').text())
        expect(row).to.have.string('Short')

        row = await page.evaluate(() => $('#tblPrefsItems > tbody tr:nth-child(2)').text())
        expect(row).to.have.string('Long')
    })

    it("Test # 2952: Open the dialog, double-click Octo, click on DragAndDrop, should display one boolean entry in the table", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Octo_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#Octo-DragAndDrop_anchor");
        await btnClick.click();

        await libs.delay(200)

        let row = await page.evaluate(() => $('#tblPrefsItems > tbody tr:nth-child(1)').text())
        expect(row).to.have.string('Display help on drop')
    })

    it("Test # 2955: Open the dialog, select Storage / Disk space alerts, it should display the Range Percent pane with correct title", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Storage_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#Storage-Diskspacealert_anchor");
        await btnClick.click();

        await libs.delay(200)

        let row = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(row).to.have.string('Disk space alert for File Storage')
    })

    it("Test # 2956: Open the dialog, select Region / Journal, it should display the Range Percent pane with correct title", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        let row = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(row).to.have.string('Disk space alert for Journal file')
    })

    it("Test # 2957: Open the dialog, select Region / Database file / Disk space alert, it should expand to \"Default\" and a list of regions (upper case)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(200)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(200)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        const children = await page.evaluate(() => $('#Regions-Databasefile-Diskspacealert > ul').children().text())

        expect(children).to.have.string('DefaultRegion DEFAULTRegion YDBAIMRegion YDBJNLFRegion YDBOCTO')
    })

    it("Test # 2958: Open the dialog, double-click REST, click on Timeout, change the Short value, click OK to save it, refresh the browser, value should be changed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#REST_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#REST-Timeout_anchor");
        await btnClick.click();

        await libs.delay(200)

        // set the value to 20
        await page.evaluate(() => $('#inp-short').val(20))
        await page.evaluate(() => app.ui.prefs.tableUtils.numChange('inp-short'))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH BROWSER
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#REST_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#REST-Timeout_anchor");
        await btnClick.click();

        await libs.delay(200)

        // read the value
        const newValue = await page.evaluate(() => $('#inp-short').val())

        expect(newValue === '20').to.be.true
    })

    it("Test # 2961: Open the dialog, double-click Octo, click on DragAndDrop, change the option from Yes to No, save it, refresh the browser and verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Octo_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#Octo-DragAndDrop_anchor");
        await btnClick.click();

        await libs.delay(200)

        // set the value to NO
        await page.evaluate(() => $('#bool-displayHelp option[value="false"]').attr('selected', 'selected'))
        await page.evaluate(() => app.ui.prefs.tableUtils.boolChange('bool-displayHelp'))

        await libs.delay(100)

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH BROWSER
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Octo_anchor'))

        await libs.delay(200)

        btnClick = await page.$("#Octo-DragAndDrop_anchor");
        await btnClick.click();

        await libs.delay(200)

        const sel = await page.evaluate(() => $('#bool-displayHelp').find(":selected").val())

        expect(sel).to.have.string('false')
    })

    it("Test # 2962: Display dialog, click ok, msgbox should appear with message: No changes", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(text).to.have.string('Nothing has changed')
    })
})

describe("CLIENT: Preferences: ranges", async () => {
    /*
    it("Test # 2970: Storage: change 0 - 70 % down to 0 - 60 %. Save, refresh and test", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.prefs.show())

            // wait for preferences to be set by the async call
            await libs.waitForDialog('#modalPrefs');

            await libs.dblClickOnElement(await page.$('#Storage_anchor'))

            let btnClick = await page.$("#Storage-Diskspacealert_anchor");
            await btnClick.click();

            await libs.delay(200)

            const text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
            expect(text).to.have.string('Disk space alert for File Storage')

            // change value to 60
            await page.evaluate(() => $('#inpPrefsRangePercent1').val(60))
            await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(1))

            await libs.delay(500)

            // submit
            btnClick = await page.$("#btnPrefsOk");
            await btnClick.click();

            // wait for preferences to be closed
            await libs.delay(500)

            // REFRESH
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.prefs.show())

            // wait for preferences to be set by the async call
            await libs.waitForDialog('#modalPrefs');

            await libs.dblClickOnElement(await page.$('#Storage_anchor'))

            btnClick = await page.$("#Storage-Diskspacealert_anchor");
            await btnClick.click();

            await libs.delay(500)

            // reads value
            const val = await page.evaluate(() => $('#inpPrefsRangePercent1').val())
            expect(val).to.have.string('60')
        }
    })

    it("Test # 2971: Storage: change 71 - 90 % down to 71 - 80 %. Save, refresh and test", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.prefs.show())

            // wait for preferences to be set by the async call
            await libs.waitForDialog('#modalPrefs');

            await libs.dblClickOnElement(await page.$('#Storage_anchor'))

            let btnClick = await page.$("#Storage-Diskspacealert_anchor");
            await btnClick.click();

            await libs.delay(500)

            const text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
            expect(text).to.have.string('Disk space alert for File Storage')

            // change value to 80
            await page.evaluate(() => $('#inpPrefsRangePercent2').val(80))
            await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(2))

            await libs.delay(500)

            // submit
            btnClick = await page.$("#btnPrefsOk");
            await btnClick.click();

            // wait for preferences to be closed
            await libs.delay(500)

            // REFRESH
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.prefs.show())

            // wait for preferences to be set by the async call
            await libs.waitForDialog('#modalPrefs');

            await libs.dblClickOnElement(await page.$('#Storage_anchor'))

            btnClick = await page.$("#Storage-Diskspacealert_anchor");
            await btnClick.click();

            await libs.delay(500)

            // reads value
            const val = await page.evaluate(() => $('#inpPrefsRangePercent2').val())
            expect(val).to.have.string('80')
        }
    })

    it("Test # 2972: Storage: change 91 - 97 % down to 91 - 95 %. Save, refresh and test", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.prefs.show())

            // wait for preferences to be set by the async call
            await libs.waitForDialog('#modalPrefs');

            await libs.dblClickOnElement(await page.$('#Storage_anchor'))

            let btnClick = await page.$("#Storage-Diskspacealert_anchor");
            await btnClick.click();

            await libs.delay(500)

            const text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
            expect(text).to.have.string('Disk space alert for File Storage')

            // change value to 80
            await page.evaluate(() => $('#inpPrefsRangePercent3').val(95))
            await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(3))

            await libs.delay(500)

            // submit
            btnClick = await page.$("#btnPrefsOk");
            await btnClick.click();

            // wait for preferences to be closed
            await libs.delay(500)

            // REFRESH
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.prefs.show())

            // wait for preferences to be set by the async call
            await libs.waitForDialog('#modalPrefs');

            await libs.dblClickOnElement(await page.$('#Storage_anchor'))

            btnClick = await page.$("#Storage-Diskspacealert_anchor");
            await btnClick.click();

            await libs.delay(500)

            // reads value
            const val = await page.evaluate(() => $('#inpPrefsRangePercent3').val())
            expect(val).to.have.string('95')
        }
    })

     */

    it("Test # 2973: Regions / Journal: change 0 - 70 % down to 0 - 60 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        let btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(text).to.have.string('Disk space alert for Journal file')

        // change value to 60
        await page.evaluate(() => $('#inpPrefsRangePercent1').val(60))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(1))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(text).to.have.string('Disk space alert for Journal file')

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent1').val())
        expect(val).to.have.string('60')
    })

    it("Test # 2974: Regions / Journal: change 71 - 90 % down to 71 - 80 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        let btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(text).to.have.string('Disk space alert for Journal file')

        // change value to 80
        await page.evaluate(() => $('#inpPrefsRangePercent2').val(80))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(2))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent2').val())
        expect(val).to.have.string('80')
    })

    it("Test # 2975: Regions / Journal: change 91 - 97 % down to 91 - 95 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        let btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(text).to.have.string('Disk space alert for Journal file')

        // change value to 80
        await page.evaluate(() => $('#inpPrefsRangePercent3').val(95))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(3))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        btnClick = await page.$("#Regions-Journal_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent3').val())
        expect(val).to.have.string('95')
    })

    it("Test # 2976: Regions /Disk alert / Default: change 0 - 70 % down to 0 - 60 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        let btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(text).to.have.string('Disk space alert for Manual Extend mode')

        // change value to 60
        await page.evaluate(() => $('#inpPrefsRangePercent1').val(60))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(1))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent1').val())
        expect(val).to.have.string('60')
    })

    it("Test # 2977: Regions /Disk alert / Default: change 71 - 90 % down to 71 - 80 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 80
        await page.evaluate(() => $('#inpPrefsRangePercent2').val(80))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(2))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent2').val())
        expect(val).to.have.string('80')
    })

    it("Test # 2978: Regions /Disk alert / Default: change 91 - 97 % down to 91 - 95 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 80
        await page.evaluate(() => $('#inpPrefsRangePercent3').val(95))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(3))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent3').val())
        expect(val).to.have.string('95')
    })

    it("Test # 2979: Regions /Disk alert / Default: unl. to 11 ext. to 14 to 9 ext. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        let btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-AutoExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 36
        await page.evaluate(() => $('#inpPrefsRangeExt1').val(36))
        await page.evaluate(() => app.ui.prefs.rangeExt.extChanged(1))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-AutoExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangeExt1').val())
        expect(val).to.have.string('36')
    })

    it("Test # 2980: Regions /Disk alert / Default: 10 to 6 ext. to 10 to 8 ext. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        let btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-AutoExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 36
        await page.evaluate(() => $('#inpPrefsRangeExt2').val(8))
        await page.evaluate(() => app.ui.prefs.rangeExt.extChanged(2))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-AutoExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangeExt2').val())
        expect(val).to.have.string('8')
    })

    it("Test # 2981: Regions /Disk alert / Default: 5 to 3 ext. to 5 to 4 ext. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        let btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-AutoExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 36
        await page.evaluate(() => $('#inpPrefsRangeExt3').val(5))
        await page.evaluate(() => app.ui.prefs.rangeExt.extChanged(3))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-Default_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-Default-AutoExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangeExt3').val())
        expect(val).to.have.string('5')
    })

    it("Test # 2982: Regions /Disk alert / Region: Default: change 0 - 70 % down to 0 - 60 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-RegionDEFAULT_anchor'))

        await libs.delay(100)

        let btnClick = await page.$("#Regions-Databasefile-Diskspacealert-RegionDEFAULT-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        let text = await page.evaluate(() => $('#h5PrefsRangePercentHeader').text())
        expect(text).to.have.string('Disk space alert for Manual Extend mode')

        // change value to 60
        await page.evaluate(() => $('#inpPrefsRangePercent1').val(60))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(1))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-RegionDEFAULT_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-RegionDEFAULT-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent1').val())
        expect(val).to.have.string('60')
    })

    it("Test # 2983: Regions /Disk alert / Region: Default: change 71 - 90 % down to 71 - 80 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-RegionDEFAULT_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-RegionDEFAULT-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 80
        await page.evaluate(() => $('#inpPrefsRangePercent2').val(80))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(2))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-RegionDEFAULT_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-RegionDEFAULT-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent2').val())
        expect(val).to.have.string('80')
    })

    it("Test # 2984: Regions /Disk alert / Region: Default: change 91 - 97 % down to 91 - 95 %. Save, refresh and test", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-RegionDEFAULT_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-RegionDEFAULT-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // change value to 80
        await page.evaluate(() => $('#inpPrefsRangePercent3').val(95))
        await page.evaluate(() => app.ui.prefs.rangePercent.percentChanged(3))

        // submit
        btnClick = await page.$("#btnPrefsOk");
        await btnClick.click();

        // wait for preferences to be closed
        await libs.delay(500)

        // REFRESH
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.prefs.show())

        // wait for preferences to be set by the async call
        await libs.waitForDialog('#modalPrefs');

        await libs.dblClickOnElement(await page.$('#Regions_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert_anchor'))

        await libs.delay(100)

        await libs.dblClickOnElement(await page.$('#Regions-Databasefile-Diskspacealert-RegionDEFAULT_anchor'))

        await libs.delay(100)

        btnClick = await page.$("#Regions-Databasefile-Diskspacealert-RegionDEFAULT-ManualExtendmode_anchor");
        await btnClick.click();

        await libs.delay(200)

        // reads value
        const val = await page.evaluate(() => $('#inpPrefsRangePercent3').val())
        expect(val).to.have.string('95')
    })

})
