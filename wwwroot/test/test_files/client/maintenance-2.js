/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const libs = require('../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("MAINTENANCE: Reorg", async () => {
    it("Test # 1100: display dialog, change settings, close and reload again, verify default values are used", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // change settings
            await page.evaluate(() => $('#rngCompactFillFactor').val(80));
            await page.evaluate(() => $('#rngCompactIndexFillFactor').val(60));
            await page.evaluate(() => $('#rngCompactReclamation').val(77));
            let elem = await page.$('#YDBAIM_anchor');
            await libs.clickOnElement(elem)
            await page.evaluate(() => $('#swCompactRecover').prop('checked', true));
            await libs.delay(250)

            // close dialog
            elem = await page.$('#btnCompactCancel');
            await libs.clickOnElement(elem)
            await libs.waitForDialog('#modalDefrag', 'close');

            // open again
            // display dialog
            await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // check and ensure they are the default
            let ret = await page.evaluate(() => $('#rngCompactFillFactor').val());
            expect(ret === '100').to.be.true
            ret = await page.evaluate(() => $('#rngCompactIndexFillFactor').val());
            expect(ret === '100').to.be.true
            ret = await page.evaluate(() => $('#rngCompactReclamation').val());
            expect(ret === '0').to.be.true
        }
    })

    /*
    it("Test # 1101: Select the DEFAULT region, switch to Reclaim space and select the MM region, should display a message and set the switch off", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await page.evaluate(() => $('#swCompactRecover').prop('checked', true));
            await libs.delay(250)

            elem = await page.$('#YDBAIM_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(250)

            visible = await page.evaluate(() => $('#modalMsgbox').css('display'));
            expect(visible === 'block').to.be.true
        }
    })

    it("Test # 1102: Select all regions and switch to Reclaim space, should display a message and set the switch off", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select all regions
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)
            elem = await page.$('#YDBOCTO_anchor');
            await libs.clickOnElement(elem)
            elem = await page.$('#YDBAIM_anchor');
            await libs.clickOnElement(elem)

            elem = await page.$('#swCompactRecover');
            await libs.clickOnElement(elem)
            await libs.delay(1000)

            visible = await page.evaluate(() => $('#modalMsgbox').css('display'));
            expect(visible === 'block').to.be.true
        }
    })

     */

    it("Test # 1104: Deselect all regions, OK button should be disabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            await libs.delay(1000)

            const disabled = await page.evaluate(() => $('#btnCompactOk').prop('disabled'));
            expect(disabled).to.be.true
        }
    })

    it("Test # 1005: Change the range in the fillFactor, label should change", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            await page.evaluate(() => $('#rngCompactFillFactor').val(80));
            await page.evaluate(() => app.ui.defrag.fillFactorChange());
            let value = await page.evaluate(() => $('#lblCompactFillFactor').text());
            expect(value === '80').to.be.true
        }
    })

    it("Test # 1006: Change the range in the indexFillFactor, label should change", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            await page.evaluate(() => $('#rngCompactIndexFillFactor').val(80));
            await page.evaluate(() => app.ui.defrag.indexFillFactorChange());
            let value = await page.evaluate(() => $('#lblCompactIndexFillFactor').text());
            expect(value === '80').to.be.true
        }
    })

    it("Test # 1007: Select reclaim space, verify that range is visible, change the range in the Minimum amount for the reclamation, label should change", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT and YDBOCTO
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)
            elem = await page.$('#YDBOCTO_anchor');
            await libs.clickOnElement(elem)

            // click on the Recover switch
            elem = await page.$('#swCompactRecover');
            await libs.clickOnElement(elem)
            await libs.delay(1000)

            //the range should be visible
            visible = await page.evaluate(() => $('#collapseReclaim').css('display'));
            expect(visible === 'flex').to.be.true

            await page.evaluate(() => $('#rngCompactReclamation').val(50));
            await page.evaluate(() => app.ui.defrag.reclamationFactorChange());
            let value = await page.evaluate(() => $('#lblCompactReclamation').text());
            expect(value === '50').to.be.true
        }
    })

    it("Test # 1108: Submit an operation. It should display an Inputpbox. Clicking cancel it should close it", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select all regions
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)
            elem = await page.$('#YDBOCTO_anchor');
            await libs.clickOnElement(elem)
            elem = await page.$('#YDBAIM_anchor');
            await libs.clickOnElement(elem)

            // submit dialog
            await libs.delay(250)
            elem = await page.$('#btnCompactOk');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox')

            // close dialog
            await libs.delay(250)
            elem = await page.$('#btnInputboxNo');
            await libs.clickOnElement(elem)

            await libs.delay(250)

            visible = await page.evaluate(() => $('#modalInputbox').css('display'));
            expect(visible === 'none').to.be.true
        }
    })

    it("Test # 1109: Reports on YDBJNLF and YDBAIM: at first it should display \"No globals\" on all regions", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select YDBOCTO and YDBAIM
            let elem = await page.$('#YDBAIM_anchor');
            await libs.clickOnElement(elem)
            elem = await page.$('#YDBJNLF_anchor');
            await libs.clickOnElement(elem)

            // submit dialog
            await libs.delay(500)
            elem = await page.$('#btnCompactOk');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            await libs.delay(250)

            // submit
            await libs.delay(500)
            elem = await page.$('#btnInputboxYes');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefragResult');

            // and be correct
            await libs.delay(150)
            let text = await page.evaluate(() => $('#tblCompactResult >tbody >tr:nth-child(2) >td:nth-child(1)').text());
            expect(text === 'No globals found').to.be.true
            text = await page.evaluate(() => $('#tblCompactResult >tbody >tr:nth-child(5) >td:nth-child(1)').text());
            expect(text === 'No globals found').to.be.true
        }
    })

    it("Test # 1110: Generate a report on DEFAULT, it should display values", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            // submit dialog
            await libs.delay(500)
            elem = await page.$('#btnCompactOk');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            await libs.delay(1000)

            // submit
            elem = await page.$('#btnInputboxYes');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            //await libs.delay(3500)
            await libs.waitForDialog('#modalDefragResult');

            // and be correct
            let text = await page.evaluate(() => $('#tblCompactResult >tbody >tr:nth-child(2) >td:nth-child(1)').text());
            expect(text === 'Globals in region:').to.be.true
        }
    })

    it("Test # 1111: Generate a report on DEFAULT, generate a report, click on expand button and analyze the content", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)
            await libs.delay(250)

            // submit dialog
            await libs.delay(250)
            elem = await page.$('#btnCompactOk');
            await libs.clickOnElement(elem)

            await libs.delay(1000)

            visible = await page.evaluate(() => $('#modalInputbox').css('display'));
            expect(visible === 'block').to.be.true

            // submit
            await libs.delay(250)
            elem = await page.$('#btnInputboxYes');
            await libs.clickOnElement(elem)

            await libs.delay(3500)

            // expect report to display
            await libs.waitForDialog('#modalDefragResult');

            // expand the report to get details
            elem = await page.$('#btnDefragDetailsDEFAULT');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            // expect details report to display
            visible = await page.evaluate(() => $('#modalDefragDetailsResult').css('display'));
            expect(visible === 'block').to.be.true

            let text = await page.evaluate(() => $('#tblCompactDetailResult >tbody >tr:nth-child(2) >td').text());
            expect(text !== '').to.be.true
        }
    })

    it("Test # 1112: set pendingReorg flag to true on DEFAULT, it should display the Pending dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1112`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');
        }
    })

    it("Test # 1113: set pendingReorg flag to true on DEFAULT, the dialog should display a list with ONLY the faulty region", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1113`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            let text = await page.evaluate(() => $('#txtInputboxText').text());
            expect(text).to.have.string('DEFAULT')
            expect(text.indexOf('YDBOCTO') === -1).to.be.true
            expect(text.indexOf('YDBAIM') === -1).to.be.true
        }
    })

    it("Test # 1114: set pendingReorg flag to true on DEFAULT, select NO, it should display a standard REORG dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1114`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.delay(250)

            elem = await page.$('#btnInputboxNo');
            await libs.clickOnElement(elem)

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDefrag');
            const display = await libs.getCssDisplay(('#divDefragIndexFillFactor'))
            expect(display === 'flex').to.be.true
        }
    })

    it("Test # 1115: set pendingReorg flag to true on DEFAULT, select 'YES', it should display a reduced form with only the selected region, the resume switch and the background switch", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1115`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.delay(250)

            elem = await page.$('#btnInputboxYes');
            await libs.clickOnElement(elem)

            await libs.delay(1500)
            const display = await libs.getCssDisplay(('#divDefragIndexFillFactor'))
            expect(display === 'none').to.be.true
            await libs.delay(4000)
        }
    })

    it("Test # 1116: Generate a report on `DEFAULT`with recover space, it should display values", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            //  wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)
            await libs.delay(500)

            // check reclaim deleted space
            await page.evaluate(() => $('#swCompactRecover').prop('checked', true));

            // submit dialog
            await libs.delay(500)
            elem = await page.$('#btnCompactOk');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            await libs.delay(250)
            //await libs.waitForDialog('modalInputbox')

            // submit
            await libs.delay(250)
            elem = await page.$('#btnInputboxYes');
            await libs.clickOnElement(elem)

            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefragResult');

            // and be correct
            await libs.delay(500)
            let text = await page.evaluate(() => $('#tblCompactResult >tbody >tr:nth-child(10) >td:nth-child(1)').text());
            expect(text === 'Reduced total blocks:' || text === 'Reduced free blocks:').to.be.true
        }
    })

    it("Test # 1117: Ensure \"Run background\" is reset after dialog close / open", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // change settings
            await page.evaluate(() => $('#swCompactRunBackground').prop('checked', true));
            await libs.delay(250)

            // close dialog
            elem = await page.$('#btnCompactCancel');
            await libs.clickOnElement(elem)
            await libs.waitForDialog('#modalDefrag', 'close');

            // open again
            // display dialog
            await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // check and ensure they are the default
            const val = await page.evaluate(() => $('#swCompactRunBackground').prop('checked'));
            expect(val === false).to.be.true
        }
    })

    it("Test # 1118: Submit using \"Run background\": message box should display extra text", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await page.evaluate(() => $('#swCompactRunBackground').prop('checked', true));

            await libs.delay(250)

            // submit dialog
            await libs.delay(250)
            elem = await page.$('#btnCompactOk');
            await libs.clickOnElement(elem)

            await libs.waitForDialog('modalInputbox');

            const text = await page.evaluate(() => $('#txtInputboxText').text());
            expect(text).to.have.string('You have chosen to run the defragment')
        }
    })

    it("Test # 1119: Run using \"Run background\": toaster should appear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await page.evaluate(() => $('#swCompactRunBackground').prop('checked', true));

            await libs.delay(250)

            // submit dialog
            await libs.delay(250)
            await libs.clickOnElement(await page.$('#btnCompactOk'))

            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.delay(3000)

            const text = await page.evaluate(() => $('#toastTitle-0').text());
            expect(text).to.have.string('Success: Reorg')
        }
    })

    it("Test # 1120: Run using \"Run background\": click on toast, should display result dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await page.evaluate(() => $('#swCompactRunBackground').prop('checked', true));

            await libs.delay(250)

            // submit dialog
            await libs.delay(250)
            await libs.clickOnElement(await page.$('#btnCompactOk'))

            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.delay(3000)

            await libs.clickOnElement(await page.$('#btnToastBody-0'))

            await libs.waitForDialog('#modalDefragResult');
        }
    })

    it("Test # 1130: RO mode: Menu entry should be disabled", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let disabled = await page.evaluate(() => $('#menuSystemAdministrationDefrag').hasClass('disabled'));
            expect(disabled).to.be.true
        }
    })
})

