#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '************************************'
echo '---Topology > tls > bc7'
echo '************************************'
echo

$PWD/replication/repl run bc7
while
	! docker logs --tail 1 melbourne | grep -q "Starting Server at port" ||
	! docker logs --tail 1 paris | grep -q "Starting Server at port" ||
	! docker logs --tail 1 santiago | grep -q "Starting Server at port" ||
	! docker logs --tail 1 rome | grep -q "Starting Server at port" ||
	! docker logs --tail 1 amsterdam | grep -q "Starting Server at port" ||
	! docker logs --tail 1 london | grep -q "Starting Server at port" ||
	! docker logs --tail 1 madrid  | grep -q "Starting Server at port" ||
	! docker logs --tail 1 tokio | grep -q "Starting Server at port";
do sleep 1
done

docker exec -e ydb_in_repl=melbourne melbourne npm test -- "wwwroot/test/test_files/replication/topology-tls/bc7/melbourne.js"
script1=$?

$PWD/replication/repl down
exit $script1
