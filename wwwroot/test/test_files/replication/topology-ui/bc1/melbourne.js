/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Topology UI > bc1 > Save dialog", async () => {
    it("Test # 3450: open layout, click save, expect dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');
    })

    it("Test # 3451: open layout, click save, click ok, expect msgbox", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 3452: open layout, click save, enter name bc1_test_1, click ok, expect dialog to close", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_1')

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        await libs.waitForDialog('#modalReplSave', 'close');
    })

    it("Test # 3453: save layout, close, save it again with name bc2_test_1, enter description, expect inputbox, say no, dialog should still be open", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_1')

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        await libs.waitForDialog('#modalReplSave', 'close');

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_1')

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        // wait for dialog to be set by the async call
        await libs.waitForDialog('modalInputbox')

        let btnClick = await page.$("#btnInputboxNo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('modalInputbox', 'close')
    })

    it("Test # 3454: save layout, close, save it again with name bc2_test_1, expect inputbox, say yes, dialog should close", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_1')

        await page.evaluate(() => $('#inpReplSaveDescription').focus())

        await page.keyboard.type('This is a description')

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        // wait for dialog to be set by the async call
        await libs.waitForDialog('modalInputbox')

        let btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalReplSave', 'close')
    })

    it("Test # 3455: modify layout, save it with name bc1_test_2, mark it as autoload, close layout, reopen, verify", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // modify layout
        await page.evaluate(() => {
            const shape = app.ui.replication.topology.joint.findShapeById('paris')
            shape.position(500, 500)
        })

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // SAVE IT

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify dialog opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_2')

        await page.evaluate(() => $('#inpReplSaveDescription').focus())

        await page.keyboard.type('This is a description')

        await page.evaluate(() => $('#swReplSaveAutoload').prop('checked', true))

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalReplSave', 'close')

        // NOW REFRESH THE BROWSER TO RESET EVERYTHING

        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(3000)


        // verify that autoload worked
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph.forEach(model => {
            if (model.id === 'paris') {
                expect(model.position.x === 500).to.be.true
                expect(model.position.y === 500).to.be.true
            }
        })
    })

    it("Test # 3456: modify layout, save it with name bc1_test_3, mark it as autoload, close layout, reopen, verify toolbar got switched", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // modify layout
        await page.evaluate(() => {
            const shape = app.ui.replication.topology.joint.findShapeById('paris')
            shape.position(500, 500)
        })

        // SAVE IT

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify dialog opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_3')

        await page.evaluate(() => $('#inpReplSaveDescription').focus())

        await page.keyboard.type('This is a description')

        await page.evaluate(() => $('#swReplSaveAutoload').prop('checked', true))

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalReplSave', 'close')

        // NOW REFRESH THE BROWSER TO RESET EVERYTHING

        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(3000)

        // verify that autoload worked
        const graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())
        graph.forEach(model => {
            if (model.id === 'paris') {
                expect(model.position.x === 500).to.be.true
                expect(model.position.y === 500).to.be.true
            }
        })

        // and toolbar got switched automatically
        const display = await page.evaluate(() => $('#divTopologyToolbarRun').css("display"))
        expect(display === 'block')
    })
})

describe("CLIENT: REPL > Topology UI > bc1 > Load dialog", async () => {
    it("Test # 3460: open layout, click load, expect dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // click icon
        cell = await page.$('#btnTopologyToolbarLoad')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLoad');
    })

    it("Test # 3461: open layout, save an entry, click load, verify that list has one item", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // SAVE DIALOG

        // click icon
        cell = await page.$('#btnTopologyToolbarSave')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplSave');

        await page.evaluate(() => $('#inpReplSaveName').focus())

        await page.keyboard.type('bc1_test_1')

        // click ok
        cell = await page.$('#btnReplSaveOk')
        await cell.click()

        await libs.waitForDialog('#modalReplSave', 'close');

        // LOAD DIALOG

        // click icon
        cell = await page.$('#btnTopologyToolbarLoad')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLoad');

        const items = await page.evaluate(() => $('#selReplLoadNames').children().length)
        expect(items === 5).to.be.true
    })

    it("Test # 3465: open layout, create an entry, click load, click delete, expect inputbox", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)


        // LOAD DIALOG

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarLoad')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLoad');

        // click delete
        cell = await page.$('#btnReplLoadDelete')
        await cell.click()

        // wait for dialog to be set by the async call
        await libs.waitForDialog('modalInputbox')
    })

    it("Test # 3466: open layout, click load, click delete, expect inputbox, choose Yes, close dialog, reopen, verify total entries", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // LOAD DIALOG

        // switch toolbar
        let cell = await page.$('#btnTopologyToolbarSwitchToRun')
        await cell.click()

        // click icon
        cell = await page.$('#btnTopologyToolbarLoad')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLoad');

        const items2 = await page.evaluate(() => $('#selReplLoadNames').children().length)
        expect(items2 === 5).to.be.true

        // click delete
        cell = await page.$('#btnReplLoadDelete')
        await cell.click()

        // wait for dialog to be set by the async call
        await libs.waitForDialog('modalInputbox')

        let btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('modalInputbox', 'close')

        // close the dialog
        btnClick = await page.$("#btnReplLoadCancel");
        await btnClick.click();

        // verify it closes correctly
        await libs.waitForDialog('#modalReplLoad', 'close');

        // LOAD DIALOG

        // click icon
        cell = await page.$('#btnTopologyToolbarLoad')
        await cell.click()

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLoad');

        const items = await page.evaluate(() => $('#selReplLoadNames').children().length)
        expect(items === (items2 - 1)).to.be.true
    })
})
