/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
const fs = require("fs");

const browserPorts = {
    MELBOURNE: 8089,
    PARIS: 9089,
    SANTIAGO: 10089,
    ROME: 11089,
    AMSTERDAM: 12089,
    LONDON: 13089,
    TOKIO: 14089,
    MADRID: 15089
}

const configTls = async function () {
    await page.evaluate(() => {
        const serverSantiago = app.userSettings.defaults.replication.discoveryService.servers[1]
        serverSantiago.tlsFileLocation = '/repl/backups/santiago.ydbgui.pem'

        const serverParis = app.userSettings.defaults.replication.discoveryService.servers[2]
        serverParis.tlsFileLocation = '/repl/backups/paris.ydbgui.pem'

        const serverRome = app.userSettings.defaults.replication.discoveryService.servers[3]
        serverRome.tlsFileLocation = '/repl/backups/rome.ydbgui.pem'

        const serverAmsterdam = app.userSettings.defaults.replication.discoveryService.servers[4]
        serverAmsterdam.tlsFileLocation = '/repl/backups/amsterdam.ydbgui.pem'

        const serverLondon = app.userSettings.defaults.replication.discoveryService.servers[5]
        serverLondon.tlsFileLocation = '/repl/backups/london.ydbgui.pem'

        const serverTokio = app.userSettings.defaults.replication.discoveryService.servers[6]
        serverTokio.tlsFileLocation = '/repl/backups/tokio.ydbgui.pem'

        const serverMadrid = app.userSettings.defaults.replication.discoveryService.servers[7]
        serverMadrid.tlsFileLocation = '/repl/backups/madrid.ydbgui.pem'
    })
}

const forFileStatus = function (path, status) {
    return new Promise(function (resolve) {
        let hTimer = setInterval(() => {
            const newStatus = fs.readFileSync(path).toString().replace('\n', '')

            if (status === newStatus) {
                clearInterval(hTimer)
                resolve()
            }
        }, 200)
    })
}

module.exports.browserPorts = browserPorts;
module.exports.configTls = configTls;
module.exports.forFileStatus = forFileStatus;
