/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("SERVER: Octo / Rocto detection", async () => {
    it("Test # 6200: Check Octo detected, Rocto stopped", async () => {
        // execute the call
        const res = await libs._REST('dashboard/get-all').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        // Check the result
        let isNode = res.result !== undefined;
        expect(isNode).to.be.true;

        expect(res.result).to.have.string('OK');

        // Check if node Octo exists
        isNode = res.data.octo !== undefined;
        expect(isNode).to.be.true;

        // and all of its descendents
        isNode = res.data.octo.status !== undefined;
        expect(isNode).to.be.true;

        expect(res.data.octo.status).to.have.string('ok');

        isNode = res.data.octo.rocto !== undefined;
        expect(isNode).to.be.true;

        expect(res.data.octo.rocto.status).to.have.string('stopped');
    })

    it("Test # 6201: Run Rocto, check Octo detected, Rocto running, stop Rocto", async () => {
        // this will run and stay in the background
        exec('rocto')

        await libs.delay(250)

        const pid = execSync('pgrep rocto').toString()
        await libs.delay(250)

        // execute the call
        const res = await libs._REST('dashboard/get-all').catch(() => {});

        expect(res.result).to.have.string('OK');

        isNode = res.data.octo.rocto !== undefined;
        expect(isNode).to.be.true;

        expect(res.data.octo.rocto.status).to.have.string('running');
        expect(res.data.octo.rocto.ip === '0.0.0.0' || res.data.octo.rocto.ip === '127.0.0.1').to.be.true

        execSync('kill ' + pid)
    })

    it("Test # 6202: Run Rocto with params, check Octo detected, Rocto running, stop Rocto", async () => {
        // this will run and stay in the background
        exec('rocto -w')

        await libs.delay(250)

        const pid = execSync('pgrep rocto').toString()

        // execute the call
        const res = await libs._REST('dashboard/get-all').catch(() => {});

        expect(res.result).to.have.string('OK');

        isNode = res.data.octo.rocto !== undefined;
        expect(isNode).to.be.true;

        expect(res.data.octo.rocto.status).to.have.string('running');
        expect(res.data.octo.rocto.params).to.have.string('-w');

        execSync('kill ' + pid)

        await libs.delay(250)
    })

    it("Test # 6203: Run Rocto on another port, with params, check Octo detected, Rocto running, stop Rocto", async () => {
        // this will run and stay in the background
        exec('rocto -w --port=1338')

        await libs.delay(250)

        const pid = execSync('pgrep rocto').toString()

        // execute the call
        const res = await libs._REST('dashboard/get-all').catch(() => {});

        expect(res.result).to.have.string('OK');

        isNode = res.data.octo.rocto !== undefined;
        expect(isNode).to.be.true;
        expect(res.data.octo.rocto.status).to.have.string('running');
        expect(res.data.octo.rocto.params).to.have.string('-w--port=1338');

        execSync('kill ' + pid)
        await libs.delay(250)
    })

    it("Test # 6204: Move .../plugin/octo directory, refresh, check Octo missing, move back", async () => {
        // move the octo directory
        execSync('mv $ydb_dist/plugin/octo $ydb_dist/plugin/octoOld')

        // execute the call
        const res = await libs._REST('dashboard/get-all').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        // Check the result
        let isNode = res.result !== undefined;
        expect(isNode).to.be.true;

        expect(res.result).to.have.string('OK');

        // Check if node Octo exists
        isNode = res.data.octo !== undefined;
        expect(isNode).to.be.true;

        // and all of its descendents
        isNode = res.data.octo.status !== undefined;
        expect(isNode).to.be.true;

        expect(res.data.octo.status).to.have.string('error');

        // move the octo directory back
        execSync('mv $ydb_dist/plugin/octoOld $ydb_dist/plugin/octo')
    })

    it("Test # 6205: Move %ydbocto.dat as *.old, detect should fail, move *.old file back", async () => {
        // rename the octo db
        execSync('mv $ydb_dir/$ydb_rel/g/%ydbocto.dat $ydb_dir/$ydb_rel/g/%ydbocto.old')

        // execute the call
        const res = await libs._REST('dashboard/get-all').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        // Check the result
        let isNode = res.result !== undefined;
        expect(isNode).to.be.true;

        expect(res.result).to.have.string('OK');

        // Check if node Octo exists
        isNode = res.data.octo !== undefined;
        expect(isNode).to.be.true;

        // and all of its descendents
        isNode = res.data.octo.status !== undefined;
        expect(isNode).to.be.true;

        expect(res.data.octo.status).to.have.string('error');

        // rename the octo db back
        execSync('mv $ydb_dir/$ydb_rel/g/%ydbocto.old $ydb_dir/$ydb_rel/g/%ydbocto.dat')
    })
})

describe("SERVER: execute Query", async () => {
    it("Test # 6220: submit with no query specified", async () => {
        // execute the call
        const obj = {}
        const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        // Check the result
        let isNode = res.result !== undefined;
        expect(isNode).to.be.true;

        expect(res.result).to.have.string('ERROR');
        expect(res.error.description).to.have.string('No query has been specified')
    })

    it("Test # 6221: submit with no timeout specified", async () => {
        // execute the call
        const obj = {
            query: '\\d;'
        }
        const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        // Check the result
        let isNode = res.result !== undefined;
        expect(isNode).to.be.true;

        expect(res.result).to.have.string('ERROR');
        expect(res.error.description).to.have.string('No timeout has been specified')
    })

    it("Test # 6222: submit with \\d;, verify response", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const obj = {
                query: '\\d;',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            let isNode = res.result !== undefined;
            expect(isNode).to.be.true;

            expect(res.result).to.have.string('OK');
            expect(res.parseError).to.be.false
            expect(res.data.length > 0).to.be.true
        }
    })

    it("Test # 6223: submit with \\d, timeout 1, verify timeout gets triggered", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const obj = {
                query: '\\d;',
                timeout: 1
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            let isNode = res.result !== undefined;
            expect(isNode).to.be.true;

            expect(res.result).to.have.string('ERROR');
            expect(res.error.description).to.have.string('Timeout while executing')
        }
    })

    it("Test # 6224: submit query with double-quotes, verify it gets processed alright", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const obj = {
                query: 'select * from suppliers where suppliername = \'I have ""quotes"";\'',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            let isNode = res.result !== undefined;
            expect(isNode).to.be.true;

            expect(res.result).to.have.string('OK');
            expect(res.parseError).to.be.false
            expect(res.data.length === 2).to.be.true
        }
    })

    it("Test # 6225: submit query without ;, verify that it gets executed", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const obj = {
                query: 'select * from suppliers where suppliername = \'I have ""quotes""\'',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            let isNode = res.result !== undefined;
            expect(isNode).to.be.true;

            expect(res.result).to.have.string('OK');
            expect(res.parseError).to.be.false
            expect(res.data.length === 2).to.be.true
        }
    })

    it("Test # 6226: RO: try to execute an SELECT, verify error is returned", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            const obj = {
                query: 'select * from suppliers',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            expect(res.error.code === 403).to.be.true
        }
    })

    it("Test # 6227: RO: try to execute an INSERT, verify error is returned", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            const obj = {
                query: 'INSERT INTO suppliers',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            expect(res.error.code === 403).to.be.true
        }
    })

    it("Test # 6228: RO: try to execute an UPDATE, verify error is returned", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            const obj = {
                query: 'UPDATE suppliers',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            expect(res.error.code === 403).to.be.true
        }
    })

    it("Test # 6229: RO: try to execute an DROP, verify error is returned", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            const obj = {
                query: 'DROP suppliers',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            expect(res.error.code === 403).to.be.true
        }
    })

    it("Test # 6230: RO: try to execute a \\d, verify it doesn't executed", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            const obj = {
                query: '\\d',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            expect(res.error.code === 403).to.be.true
        }
    })

    it("Test # 6231: RO: try to execute a \\q, verify it doesn't executed", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            const obj = {
                query: '\\q',
                timeout: 3000
            }
            const res = await libs._RESTpost('octo/execute', obj).catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            // Check the result
            expect(res.error.code === 403).to.be.true
        }
    })
})

describe("SERVER: get-objects", async () => {
    it("Test # 6250: submit, verify response", async () => {
        // execute the call
        const res = await libs._REST('octo/get-objects').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        expect(res.result).to.have.string('OK');
        expect(res.data.functions.length > 0).to.be.true
        expect(res.data.tables.length > 0).to.be.true
    })
})

describe("SERVER: get-objects", async () => {
    it("Test # 6251: submit table with injection (suppliers;drop), verify response", async () => {
        // execute the call
        const res = await libs._REST('octo/tables/suppliers;drop/get-struct').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        expect(res.result).to.have.string('ERROR');
        expect(res.error.description).to.have.string('The table name can not contain the ; character')
    })
})


describe("SERVER: get-objects", async () => {
    it("Test # 6252: submit, verify response includes the view `v1`", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const res = await libs._REST('octo/get-objects').catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            expect(res.result).to.have.string('OK');
            expect(res.data.functions.length > 0).to.be.true
            expect(res.data.tables.length > 0).to.be.true

            expect(res.data.views[0].name).to.have.string('v1')
        }
    })
})

describe("SERVER: get-struct", async () => {
    it("Test # 6275: submit with bad table, verify error", async () => {
        // execute the call
        const res = await libs._REST('octo/tables/suppliersbad/get-struct').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        expect(res.result).to.have.string('OK');
        expect(res.data.global).to.have.string('ERR_UNKNOWN_TABLE')
    })

    it("Test # 6276: submit with valid table, verify response", async () => {
        // execute the call
        const res = await libs._REST('octo/tables/suppliers/get-struct').catch(() => {});

        // Check if it is an object
        const isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        expect(res.result).to.have.string('OK');
        expect(res.data.columns.length > 0).to.be.true
        expect(res.data.indexes.length > 0).to.be.true
        expect(res.data.name !== '').to.be.true
        expect(res.data.type !== '').to.be.true
        expect(res.data.global !== '').to.be.true
    })

    it("Test # 6277: submit with bad view, verify error", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const res = await libs._REST('octo/views/suppliersbad/get-struct').catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;

            expect(res.result).to.have.string('OK');
            expect(res.data.def === '').to.be.true
        }
    })

    it("Test # 6278: submit with valid view, verify response", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            const res = await libs._REST('octo/views/v1/get-struct').catch(() => {});

            // Check if it is an object
            const isObject = typeof res === 'object';
            expect(isObject).to.be.true;
            expect(res.result).to.have.string('OK');
            expect(res.data.columns.length > 0).to.be.true
            expect(res.data.def).to.have.string('create view v1 as select * from categories')
        }
    })

})

