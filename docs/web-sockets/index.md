<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# Statistics

#### Index

- Introduction
- The Web Sockets server
- Starting / Enquiring Web Socket server status
- The scheduler
- The MUMPS Statistics server
- The graphical interface
- Commands and data structures

### Introduction

The Statistics module allows the user to collect database and system samples data and visualize it using several
techniques to be used for further analysis and decision-making.

In the first stage of the implementation we supply only the statistical data available from the ^%YGBLSTATS routine, but
the software is designed (and partially coded) to allow easy integration of
cpu, memory, journal, global buffers, etc.
This was important to avoid future limitations or refactoring (which usually leads to: takes too long, we don't do it).

So, data structures, variable and module naming are ready for the future...

The (streaming) statistical data has to be collected by software that leaves the smallest footprint in both memory and
cpu.
To achieve that we have a Web Socket server and a MUMPS Statistics Server running in the background, collecting and
sending data to the Web browser.

The MUMPS Statistics Server gets launched by the Web Socket server and stays suspended in mempry, waiting for commands
to execute (sent from the Web Socket Server) .
It then collects the data, format the response and send it back to the Web Socket Server, ready to be dispatched to the
Web browser client.

Since the intrinsic nature of data collection is "collecting data at specific, regular intervals", by having a suspended
M server, waiting for commands and execute them, instead of "shelling out an M program and get the stats each interval
tick",
will avoid the very expensive work of shelling out (creating a process, initializing the M environment, etc.) that could
affect server performances.

To allow flexibility, the software allows also "multiple data sets" to be collected, each at different timing intervals.
In practise, it allows us to collect, p.e., every 500 ms. SET and KIL, every 1000 ms. ORD and every 5 seconds KIL.

### The Web Sockets server

The Web Socket server is node.js, which gets installed using apt-get and does NOT use the npm to manage
Javascript modules, as we don't have any of those, all the calls are done using the built-in C code.

We chose node.js because of its intrinsic even driven programming model, which suits perfectly for this task.

The server gets started when the client opens the "Statistics" tab, which in turn starts up the MUMPS Stats Server.
A REST call takes care of starting the server and checking the server status.

Once the server is started, the web client connects to the web socket.

A custom protocol has been created to communicate between the server and the client. It has been designed in a way that
he can be
easily expanded in the future.

The protocol is based on opCodes and associated data structures.
Responses will always contain a reference to the "originator" (which is the request opCode), the status (ACK, NAK, etc)
and a data
section.

Usually, messages from the M server are "transformed" before to be sent to the client, appending extra information
useful to the interface.

Protocols and data structures are fully described later in this document.

The streaming data gets received in the client and buffered.

A strong flow and error control is securing the communication and automatically perform restarts, retries, termination
and reporting when needed.
This extends to the client, which must notify the server when the tab or the browser gets closed,
for us to terminate all running programs from the server.
An eventual client (web browser) unexpected crash will also be detected, which will trigger a full system termination on
the server.

> Note that, to follow standards, all REST calls get answered with `status = 'OK' | 'ERROR' | 'WARNING'`,
> while all socket and IPC communication get answered with `status = 'ACK' | 'NAK'`


It is safe to say that more than 30 % of the code are flow and error handlers.

<!--TODO-->
At this point of the implementation it is not clear yet is we will use a limited buffer that would flush to disk when
full or just keep all the data in RAM, further investigation and measurements are needed in order to make a final
decision.
But this is just a detail...

### Starting / Enquiring Web Socket server status

<!--TODO-->
For the web client to start the Web Socket server, there is a REST call available.

### The Scheduler

Seen that we allow multiple operations to be scheduled at different times, by using an array of timers, each at the
item's scheduled rate, we will end up having
two or more timers triggering at "about the same time", leaving us no choice but to handle collisions on the Web
Sockets <--> MUMPS Server
IPC.
To prevent that, we have a simple timer, ticking at the minimum sample rate frequency (100 ms.) and a counter.
At each tick, we go through each entry and try to divide the ticks / (sample rate * 10). If the quotient is an INT
value() we push it into an array, to be scheduled for MUMPS perf.
The MUMPS Server, of course, supports array requests and will perform all of them, sequentially.

<!--TODO-->

