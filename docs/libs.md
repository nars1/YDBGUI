<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# Libraries used

| Name                        | Version | Reference                                                   | License |
|:----------------------------|:--------|:------------------------------------------------------------|:--------|
| Bootstrap                   | 4.6.2   | https://getbootstrap.com/docs/4.6/getting-started/download/ | MIT     | 
| Bootstrap icons             | 1.11.3  | https://github.com/twbs/icons/releases/tag/v1.11.1          | MIT     | 
| jquery                      | 3.7.1   | https://jquery.com/download/                                | MIT     |
| jquery-ui                   | 1.13.2  | https://jqueryui.com/download/                              | MIT     |
| jstree                      | 3.3.15  | https://www.jstree.com/                                     | MIT     |
| codemirror                  | 5.65.9  | https://www.codemirror.com/                                 | MIT     |
| web sockets                 | 7.4.6   | https://www.npmjs.com/package/ws                            | MIT     |
| Apex Chart                  | 3.41.0  | https://apexcharts.com/                                     | MIT     |
| Showdown                    | 2.1.0   | https://github.com/showdownjs/showdown                      | MIT     | 
| chartjs                     | 4.4.1   | https://chartjs.org                                         | MIT     | 
| luxon                       | 3.4.4   | https://moment.github.io/luxon/#/                           | MIT     | 
| chartjs-adapter-luxon       | 1.3.1   | https://github.com/chartjs/chartjs-adapter-luxon            | MIT     | 
| joint.js                    | 4.0.1   | https://www.mozilla.org/en-US/MPL/2.0/                      | MPL 2.0 | 
| jointjs.layoutDirectedGraph | 4.0.3   | https://www.mozilla.org/en-US/MPL/2.0/                      | MPL 2.0 | 
| dagrejs                     | 1.0.4   | https://github.com/dagrejs/dagre                            | MIT     | 
| graphlib                    | 2.1.13  | https://github.com/dagrejs/graphlib                         | MIT     | 
