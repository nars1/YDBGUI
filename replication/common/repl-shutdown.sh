#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

$ydb_dist/mupip replicate -receive -shutdown -timeout=0
$ydb_dist/mupip replicate -source -shutdown -timeout=0

 # Look for M processes and terminate them nicely
processes=$(pgrep yottadb)
if [ ! -z "${processes}" ] ; then
		echo "Stopping any remaining M processes nicely"
		for i in ${processes}
		do
				mupip stop ${i}
		done

		# Wait for process to react to mupip stop instead of force kill later
		sleep 5
fi
$ydb_dist/mupip rundown -region "*"
$ydb_dist/mupip rundown -relinkctl
